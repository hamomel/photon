package com.hamom.photon.data.network.adapters;

import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.sources.PhotoCardTestSourse;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by hamom on 30.05.17.
 */
public class PhotoCardAdapterTest {

  JsonAdapter<PhotoCard> mMoshi;

  @Before
  public void setUp() throws Exception {
    mMoshi = new Moshi.Builder().add(new PhotoCardAdapter()).build().adapter(PhotoCard.class);

  }

  @Test
  public void fromJson() throws Exception {
    PhotoCard photoCard = mMoshi.fromJson(PhotoCardTestSourse.PHOTO_JSON);
    System.out.println(photoCard);
    assertEquals("58dbbd0d172720ac551c7251", photoCard.getId());
    assertEquals("искусственный", photoCard.getTags().get(0));
  }
}