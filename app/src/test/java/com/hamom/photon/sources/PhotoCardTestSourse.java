package com.hamom.photon.sources;

import com.hamom.photon.data.storage.realm.Filters;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.utils.ConstantManager;

/**
 * Created by hamom on 30.05.17.
 */

public class PhotoCardTestSourse {
  public static final String PHOTO_JSON = "{\n"
      + "\"id\": \"58dbbd0d172720ac551c7251\",\n"
      + "\"owner\": \"58dbbd0d172720ac551c7252\",\n"
      + "\"title\": \"Гамбургер\",\n"
      + "\"photo\": \"http://mtvoyage.ru/wp-content/gallery/bajjkal/1.jpg\",\n"
      + "\"views\": 7,\n"
      + "\"favorits\": 7,\n"
      + "\"tags\": [\n"
      + "\"искусственный\",\n"
      + "\"напитки\"\n"
      + "],\n"
      + "\"filters\": {\n"
      + "\"dish\": \"meat\",\n"
      + "\"nuances\": \"red\",\n"
      + "\"decor\": \"holiday\",\n"
      + "\"temperature\": \"hot\",\n"
      + "\"light\": \" natural \",\n"
      + "\"lightDirection\": \"direct\",\n"
      + "\"lightSource\": \"one\"\n"
      + "}\n"
      + "}";

  public static final PhotoCard PHOTO_CARD = new PhotoCard("58dbbd0d172720ac551c7251",
      "58dbbd0d172720ac551c7252",
      "http://mtvoyage.ru/wp-content/gallery/bajjkal/1.jpg",
      7,
      getFilters(),
      "Гамбургер",
      7,
      getTags(),
      true);

  private static String getTags() {
    return "искусственный" + ConstantManager.PHOTO_TAGS_SEPARATOR + "напитки" + ConstantManager.PHOTO_TAGS_SEPARATOR;
  }

  private static Filters getFilters() {
    return new Filters("red", "direct", "one", "meat", "holiday", "natural", "hot");
  }
}
