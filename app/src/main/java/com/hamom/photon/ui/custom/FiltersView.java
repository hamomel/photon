package com.hamom.photon.ui.custom;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.dto.FiltersDto;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hamom on 29.06.17.
 */

public class FiltersView extends FrameLayout {

  private static String TAG = ConstantManager.TAG_PREFIX + "FiltersView ";

  private static final String VIEW_ID_DIVIDER = "_";
  @BindView(R.id.dish_group)
  RadioGroup dishGroup;
  @BindView(R.id.decor_group)
  RadioGroup decorGroup;
  @BindView(R.id.temperature_group)
  RadioGroup temperatureGroup;
  @BindView(R.id.light_group)
  RadioGroup lightGroup;
  @BindView(R.id.direction_group)
  RadioGroup directionGroup;
  @BindView(R.id.source_group)
  RadioGroup sourceGroup;

  @BindViews({R.id.red, R.id.orange, R.id.yellow, R.id.green, R.id.lightBlue,
      R.id.blue, R.id.violet, R.id.brown, R.id.black, R.id.white })
  List<FilterItemView> tintViews;


  public FiltersView(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    View view = LayoutInflater.from(context).inflate(R.layout.layout_view_filters, this, true);
    if (!isInEditMode()) ButterKnife.bind(this, view);
  }

  public void showCurrentFilters(FiltersDto filters) {
    setItemChecked(dishGroup, filters.getDish());
    setItemChecked(decorGroup, filters.getDecor());
    setItemChecked(temperatureGroup, filters.getTemperature());
    setItemChecked(lightGroup, filters.getLight());
    setItemChecked(directionGroup, filters.getLightDirection());
    setItemChecked(sourceGroup, filters.getLightSource());
    setColorItemsSelected(filters);
    setViewsNotClickable();
  }

  private void setViewsNotClickable() {
    setChildrenNotClickable(directionGroup);
    setChildrenNotClickable(dishGroup);
    setChildrenNotClickable(decorGroup);
    setChildrenNotClickable(temperatureGroup);
    setChildrenNotClickable(sourceGroup);
    setChildrenNotClickable(lightGroup);


    for (FilterItemView tintView : tintViews) {
      tintView.setClickable(false);
    }
  }

  private void setChildrenNotClickable(ViewGroup viewGroup) {
    int childCount = viewGroup.getChildCount();
    for (int i = 0; i < childCount; i++){
      viewGroup.getChildAt(i).setClickable(false);
    }

  }

  private void setItemChecked(RadioGroup group, String filter) {
    for (int i = 0; i < group.getChildCount(); i++){
      RadioButton button = ((RadioButton) group.getChildAt(i));
      if (filter.equals(getTextFromId(button.getId()))){
        button.setChecked(true);
      }
    }
  }

  private void setColorItemsSelected(FiltersDto filters) {
    List<String> nuances = filters.getNuances();
    for (FilterItemView tintView : tintViews) {
      String idName = getTextFromId(tintView.getId());
      if (nuances.contains(idName)) tintView.setChecked(true);
    }
  }

  public FiltersDto getDto() {
    String dish = getTextFromId(dishGroup.getCheckedRadioButtonId());
    String appearance = getTextFromId(decorGroup.getCheckedRadioButtonId());
    String temperature = getTextFromId(temperatureGroup.getCheckedRadioButtonId());
    String light = getTextFromId(lightGroup.getCheckedRadioButtonId());
    String lightDirection = getTextFromId(directionGroup.getCheckedRadioButtonId());
    String lightSource = getTextFromId(sourceGroup.getCheckedRadioButtonId());
    List<String> tints = getColors();

    return new FiltersDto(dish, tints, appearance, temperature, light, lightDirection, lightSource);
  }

  private List<String> getColors() {
    List<String> tints = new ArrayList<>();
    for (FilterItemView tintView : tintViews) {
      if (tintView.isChecked()){
        tints.add(getTextFromId(tintView.getId()));
      }
    }
    return tints;
  }

  private String getTextFromId(@IdRes int id) {
    String text = "";
    if (AppConfig.DEBUG) Log.d(TAG, "getTextFromId: " + id);

    if (id != -1){
      text = getContext().getResources().getResourceEntryName(id);
      text = text.split(VIEW_ID_DIVIDER)[0];
    }
    return text;
  }
}
