package com.hamom.photon.ui.activities.root;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.hamom.photon.R;
import com.hamom.photon.data.network.errors.SignInError;
import com.hamom.photon.data.network.errors.SignUpError;
import com.hamom.photon.di.DaggerComponentFactory;
import com.hamom.photon.di.components.AppComponent;
import com.hamom.photon.di.scopes.DaggerScope;
import com.hamom.photon.eventBus.EventBus;
import com.hamom.photon.eventBus.events.ErrorEvent;
import com.hamom.photon.eventBus.events.ToastEvent;
import com.hamom.photon.flow.MyKeyParceler;
import com.hamom.photon.flow.TreeKeyDispatcher;
import com.hamom.photon.mortar.ScreenScoper;
import com.hamom.photon.ui.custom.MenuItemHolder;
import com.hamom.photon.ui.screens.base.IBackView;
import com.hamom.photon.ui.screens.home.HomeScreen;
import com.hamom.photon.ui.screens.profile.ProfileScreen;
import com.hamom.photon.ui.screens.upload.UploadScreen;
import com.hamom.photon.utils.App;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import dagger.Provides;
import flow.Direction;
import flow.Flow;
import flow.History;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import java.util.List;
import javax.inject.Inject;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class RootActivity extends AppCompatActivity
    implements IRootView, BottomNavigationView.OnNavigationItemSelectedListener {
  private static String TAG = ConstantManager.TAG_PREFIX + "RootActivity: ";

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.main_frame)
  FrameLayout mainFrame;
  @BindView(R.id.bottom_navigation)
  BottomNavigationView bottomNavigation;
  @BindView(R.id.coordinator)
  CoordinatorLayout coordinator;
  @BindView(R.id.appbar_layout)
  AppBarLayout appbarLayout;

  @Inject
  RootPresenter mPresenter;
  @Inject
  RootNavigator mNavigator;

  private List<MenuItemHolder> mMenuItems;
  private CompositeDisposable mCompositeDisposable;

  @Override
  protected void attachBaseContext(Context newBase) {
    newBase = Flow.configure(newBase, this)
        .defaultKey(new HomeScreen())
        .keyParceler(new MyKeyParceler())
        .dispatcher(new TreeKeyDispatcher(this))
        .install();
    super.attachBaseContext(newBase);
  }

  @Override
  public Object getSystemService(@NonNull String name) {
    MortarScope scope = MortarScope.findChild(getApplicationContext(), getScopeName());
    if (scope == null) {
      scope = buildScope();
    }
    return scope.hasService(name) ? scope.getService(name) : super.getSystemService(name);
  }

  private MortarScope buildScope() {
    MortarScope scope = MortarScope.buildChild(getApplicationContext())
        .withService(ConstantManager.DAGGER_SERVICE_NAME, createDaggerComponent())
        .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
        .build(getScopeName());
    ScreenScoper.registerScope(scope);
    return scope;
  }

  private String getScopeName() {
    return this.getClass().getName();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    if (AppConfig.DEBUG) Log.d(TAG, "onCreate: ");

    super.onCreate(savedInstanceState);
    BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
    initToolbar();
    //noinspection WrongConstant
    ((Component) getSystemService(ConstantManager.DAGGER_SERVICE_NAME)).inject(this);
    mPresenter.takeView(this);
    mNavigator.takeActivity(this);
    if (mCompositeDisposable == null) mCompositeDisposable = new CompositeDisposable();
    subscribeOnErrors();
    subscribeOnToasts();
  }

  private void subscribeOnToasts() {
    if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnToasts: ");

    mCompositeDisposable.add(EventBus.INSTANCE.listen(ToastEvent.class)
        .subscribeOn(AndroidSchedulers.mainThread())
        .doOnNext(toastEvent -> {
          if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnToasts: " + Thread.currentThread().getName());
        })
        .subscribe(toastEvent -> showMessage(toastEvent.getMessage())));
  }

  private void subscribeOnErrors() {
    if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnErrors: ");

    mCompositeDisposable.add(EventBus.INSTANCE.listen(ErrorEvent.class)
        .subscribeOn(AndroidSchedulers.mainThread())
        .subscribe(errorEvent -> showError(errorEvent.getThrowable())));
  }

  private Component createDaggerComponent() {
    return DaggerComponentFactory.createComponent(App.getAppComponent(), Component.class,
        new Module());
  }

  @Override
  protected void onStart() {
    super.onStart();
    bottomNavigation.setOnNavigationItemSelectedListener(this);
  }

  @Override
  protected void onStop() {
    super.onStop();
    bottomNavigation.setOnNavigationItemSelectedListener(null);
  }

  @Override
  protected void onDestroy() {
    if (AppConfig.DEBUG) Log.d(TAG, "onDestroy: ");

    if (isFinishing()) {
      mCompositeDisposable.dispose();
      mPresenter.dropView(this);
      mNavigator.dropActivity();
    }
    super.onDestroy();
  }

  //region===================== Toolbar ==========================
  public void initToolbar() {
    setSupportActionBar(toolbar);
  }

  @Override
  public void setOverFlowButtonIcon(@DrawableRes int iconRes) {
    if (iconRes == 0) {
      toolbar.setOverflowIcon(
          ContextCompat.getDrawable(this, R.drawable.ic_custom_menu_black_24dp));
    } else {
      toolbar.setOverflowIcon(ContextCompat.getDrawable(this, iconRes));
    }
  }

  public void setToolbarVisible(boolean isVisible) {
    if (AppConfig.DEBUG) Log.d(TAG, "setToolbarVisible: " + isVisible);

    if (isVisible) {
      toolbar.setVisibility(View.VISIBLE);
    } else {
      toolbar.setVisibility(View.GONE);
    }
  }

  public void setToolbarTitle(String title) {
    toolbar.setTitle(title);
  }

  public void setShowBackButton(boolean show) {
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(show);
  }

  public void setMenuItems(List<MenuItemHolder> menuItems) {
    mMenuItems = menuItems;
    supportInvalidateOptionsMenu();
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    if (AppConfig.DEBUG) Log.d(TAG, "onPrepareOptionsMenu: ");

    menu.clear();
    if (mMenuItems != null) {
      for (MenuItemHolder menuItem : mMenuItems) {
        MenuItem item = menu.add(menuItem.getTitle());
        item.setShowAsAction(menuItem.getShowAsAction());
        item.setOnMenuItemClickListener(menuItem.getListener());
        if (menuItem.getItemResId() != 0) {
          if (menuItem.getShowAsAction() != MenuItem.SHOW_AS_ACTION_NEVER) {
            item.setIcon(menuItem.getItemResId());
          } else {
            SpannableStringBuilder sb = new SpannableStringBuilder("*   " + item.getTitle());
            sb.setSpan(new ImageSpan(this, menuItem.getItemResId()), 0, 1,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            item.setTitle(sb);
          }
        }
      }
    }
    return super.onPrepareOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      onBackPressed();
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void setTabs(ViewPager pager) {

    TabLayout tabLayout = new TabLayout(this);
    tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.color_text_dark));
    tabLayout.setupWithViewPager(pager);
    appbarLayout.removeAllViews();
    appbarLayout.addView(tabLayout);
  }

  @Override
  public void removeTabs() {
    View tabLayout = appbarLayout.getChildAt(0);
    if (tabLayout != null && tabLayout instanceof TabLayout) {
      appbarLayout.removeView(tabLayout);
      appbarLayout.addView(toolbar);
    }
  }

  //endregion

  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    switch (item.getItemId()) {
      case R.id.navigation_home:
       mNavigator.setHistory(RootNavigator.Histories.HOME);
        break;
      case R.id.navigation_profile:
       mNavigator.setHistory(RootNavigator.Histories.PROFILE);
        break;
      case R.id.navigation_upload:
       mNavigator.setHistory(RootNavigator.Histories.UPLOAD);
        break;
    }
    return true;
  }

  @Override
  public void showMessage(String message) {
    //runOnUiThread because method call thru event bus from different threads
    runOnUiThread(() -> Toast.makeText(this, message, Toast.LENGTH_SHORT).show());
  }

  @Override
  public void showError(Throwable e) {
    e.printStackTrace();
    if (e instanceof SignInError || e instanceof SignUpError) {
      showSnackBar(e.getMessage(), null, 0, null, R.color.color_red);
    }
  }

  @Override
  public void showSnackBar(String message, String actionText, @ColorRes int actionTextColor,
      View.OnClickListener listener, @ColorRes int backGroundColor) {
    //runOnUiThread because method call thru event bus from different threads
    runOnUiThread(() -> {
      // FIXME: 01.06.17 snackbar shown over bottom navigation view on pre lolipop devices
      Snackbar snackbar = Snackbar.make(coordinator, message, Snackbar.LENGTH_LONG);
      if (actionText != null && listener != null) snackbar.setAction(actionText, listener);
      if (actionTextColor != 0) {
        snackbar.setActionTextColor(getResources().getColor(actionTextColor));
      }

      CoordinatorLayout.LayoutParams params =
          ((CoordinatorLayout.LayoutParams) snackbar.getView().getLayoutParams());
      params.setMargins(0, 0, 0, bottomNavigation.getHeight());
      snackbar.getView().setLayoutParams(params);

      if (backGroundColor != 0) {
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, backGroundColor));
      }
      snackbar.show();
    });
  }

  @Override
  public void onBackPressed() {
    IBackView view = ((IBackView) mainFrame.getChildAt(0));

    if (view != null && !view.onBackPressed()) {
      showExitDialog();
    }
  }

  private void showExitDialog() {
   new  AlertDialog.Builder(this)
       .setMessage(R.string.quit_app)
       .setPositiveButton(R.string.quit, (dialog, which) -> super.onBackPressed())
       .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
       .create()
       .show();
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    mPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    mPresenter.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  public void openAppSettings() {
    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    Uri uri = Uri.fromParts("package", getPackageName(), null);
    intent.setData(uri);
    startActivity(intent);
  }

  //region===================== DI ==========================
  @dagger.Component(dependencies = AppComponent.class, modules = Module.class)
  @DaggerScope(RootActivity.class)
  public interface Component {
    void inject(RootActivity rootActivity);

    RootPresenter getRootPresenter();
  }

  @dagger.Module
  public class Module {
    @Provides
    @DaggerScope(RootActivity.class)
    RootPresenter provideMainPresenter() {
      return new RootPresenter();
    }
  }
  //endregion
}
