package com.hamom.photon.ui.custom;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import com.hamom.photon.R;
import com.hamom.photon.data.network.requests.SignInReq;
import com.hamom.photon.data.network.requests.SignUpReq;
import com.hamom.photon.utils.ConstantManager;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hamom on 04.06.17.
 */

public class LoginDialog extends AlertDialog {

  @BindView(R.id.title_tv)
  TextView titleTv;
  @BindView(R.id.login_layout)
  TextInputLayout loginLayout;
  @BindView(R.id.login_et)
  TextInputEditText loginEt;
  @BindView(R.id.email_layout)
  TextInputLayout emailLayout;
  @BindView(R.id.email_et)
  EditText emailEt;
  @BindView(R.id.name_layout)
  TextInputLayout nameLayout;
  @BindView(R.id.name_et)
  TextInputEditText nameEt;
  @BindView(R.id.password_layout)
  TextInputLayout passwordLayout;
  @BindView(R.id.password_et)
  TextInputEditText passwordEt;

  private Context mContext;
  private boolean mIsRegisterMode;
  private RegisterCallback mRegisterCallback;
  private LoginCallBack mLoginCallBack;

  protected LoginDialog(Context context, ViewGroup parentView) {
    super(context);
    mContext = context;
    View view = LayoutInflater.from(context).inflate(R.layout.dialog_login, parentView, false);
    ButterKnife.bind(this, view);
    setView(view);
    loginEt.setHint(mContext.getString(R.string.login));
    loginEt.clearFocus();
    emailEt.setHint(mContext.getString(R.string.email));
    loginEt.clearFocus();
    nameEt.setHint(mContext.getString(R.string.name));
    nameEt.clearFocus();
    passwordEt.setHint(mContext.getString(R.string.password));
    passwordEt.clearFocus();

  }

  @Override
  public void show() {
    super.show();
    getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
  }

  public void setRegisterMode(boolean registerMode) {
    mIsRegisterMode = registerMode;
    if (!registerMode){
      loginLayout.setVisibility(View.GONE);
      nameLayout.setVisibility(View.GONE);
      titleTv.setText(R.string.enter_account);
    }
  }

  public void setRegisterCallback(RegisterCallback registerCallback) {
    mRegisterCallback = registerCallback;
  }

  public void setLoginCallBack(LoginCallBack loginCallBack) {
    mLoginCallBack = loginCallBack;
  }



  @OnClick({ R.id.ok_btn, R.id.cancel_btn })
  void onButtonClick(Button button) {
    switch (button.getId()) {
      case R.id.ok_btn:
        checkInputAndSend();
        break;
      case R.id.cancel_btn:
        dismiss();
        break;
    }
  }

  private void checkInputAndSend() {
    if (mIsRegisterMode && checkRegisterDataValid()){
      mRegisterCallback.onRegisterClick(createRegisterReq());
      dismiss();
    } else {
      if (checkLoginDataValid()){
        mLoginCallBack.onLoginClick(createLoginReq());
        dismiss();
      }
    }

  }

  private SignUpReq createRegisterReq() {
    return new SignUpReq(passwordEt.getText().toString(),
        nameEt.getText().toString(),
        loginEt.getText().toString(),
        emailEt.getText().toString());
  }

  private SignInReq createLoginReq() {
    return new SignInReq(passwordEt.getText().toString(), emailEt.getText().toString());
  }

  private boolean checkLoginDataValid() {
    return verifyPassword(passwordEt.getText().toString()) &&
        verifyEmail(emailEt.getText().toString());
  }

  private boolean checkRegisterDataValid() {
    return verifyEmail(emailEt.getText().toString()) &&
        verifyLogin(loginEt.getText().toString()) &&
        verifyName(nameEt.getText().toString()) &&
        verifyPassword(passwordEt.getText().toString());
  }

  @OnTextChanged(R.id.login_et)
  void onLoginTextChanged(CharSequence text){
    verifyLogin(text.toString());
  }

  @OnTextChanged(R.id.email_et)
  void onEmailTextChanged(CharSequence text){
    verifyEmail(text.toString());
  }

  @OnTextChanged(R.id.name_et)
  void onNameTextChanged(CharSequence text){
    verifyName(text.toString());
  }

  @OnTextChanged(R.id.password_et)
  void onPasswordTextChanged(CharSequence text){
    verifyPassword(text.toString());
  }

  private boolean verifyLogin(String text) {
    Pattern pattern = Pattern.compile(ConstantManager.LOGIN_REGEXP);
    Matcher matcher = pattern.matcher(text);
    if (text.length() > 0 && !matcher.matches()){
      loginLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.text_input_background_red));
      loginLayout.setHint(mContext.getString(R.string.wrong_format));
      loginLayout.setHintTextAppearance(R.style.DialogTextInputErrorStyle);
      return false;
    } else {
      loginLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.text_input_background));
      loginLayout.setHint("");
      loginLayout.setHintTextAppearance(R.style.DialogTextInputLayoutStyle);
      return true;
    }
  }

  private boolean verifyName(String text) {
    Pattern pattern = Pattern.compile(ConstantManager.NAME_REGEXP);
    Matcher matcher = pattern.matcher(text);
    if (text.length() > 0 && !matcher.matches()){
      nameLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.text_input_background_red));
      nameLayout.setHint(mContext.getString(R.string.length_no_less_3_char));
      nameLayout.setHintTextAppearance(R.style.DialogTextInputErrorStyle);
      return false;
    } else {
      nameLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.text_input_background));
      nameLayout.setHint("");
      nameLayout.setHintTextAppearance(R.style.DialogTextInputLayoutStyle);
      return true;
    }
  }

  private boolean verifyEmail(String text) {
    Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(text);
    if (text.length() > 0 && !matcher.matches()){
      emailLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.text_input_background_red));
      emailLayout.setHint(mContext.getString(R.string.wrong_format));
      emailLayout.setHintTextAppearance(R.style.DialogTextInputErrorStyle);
      return false;
    } else {
      emailLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.text_input_background));
      emailLayout.setHint("");
      emailLayout.setHintTextAppearance(R.style.DialogTextInputLayoutStyle);
    }
    return true;
  }

  private boolean verifyPassword(String text) {
    Pattern pattern = Pattern.compile(ConstantManager.PASSWORD_REGEXP);
    Matcher matcher = pattern.matcher(text);
    if (text.length() > 0 && !matcher.matches()){
      passwordLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.text_input_background_red));
      passwordLayout.setHint(mContext.getString(R.string.wrong_format));
      passwordLayout.setHintTextAppearance(R.style.DialogTextInputErrorStyle);
      return false;
    } else {
      passwordLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.text_input_background));
      passwordLayout.setHint("");
      passwordLayout.setHintTextAppearance(R.style.DialogTextInputLayoutStyle);
      return true;
    }
  }

  public interface LoginCallBack{
    void onLoginClick(SignInReq signInReq);
  }

  public interface RegisterCallback {
    void onRegisterClick(SignUpReq signUpReq);
  }

  public static class Builder {
    private ViewGroup mParentView;
    private Context mContext;
    private boolean mIsRegisterMode;
    private RegisterCallback mRegisterCallback;
    private LoginCallBack mLoginCallBack;

    public Builder(Context context, ViewGroup parentView) {
      mParentView = parentView;
      mContext = context;
    }

    public Builder setRegisterMode(boolean registerMode) {
      mIsRegisterMode = registerMode;
      return this;
    }

    public Builder setRegisterCallback(RegisterCallback registerCallback) {
      mRegisterCallback = registerCallback;
      return this;
    }

    public Builder setSignInCallBack(LoginCallBack loginCallBack) {
      mLoginCallBack = loginCallBack;
      return this;
    }

    public LoginDialog build(){
      LoginDialog loginDialog = new LoginDialog(mContext, mParentView);
      if (mIsRegisterMode){
        setRegisterModeToDialog(loginDialog);
      } else {
        setLoginModeToDialog(loginDialog);
      }
      return loginDialog;
    }

    private void setRegisterModeToDialog(LoginDialog loginDialog) {
      if (mRegisterCallback == null){
        throw new IllegalStateException("RegisterCallback missing in register mode");
      } else {
        loginDialog.setRegisterMode(true);
        loginDialog.setRegisterCallback(mRegisterCallback);
      }
    }

    private void setLoginModeToDialog(LoginDialog loginDialog) {
      if (mLoginCallBack == null){
        throw new IllegalStateException("LoginCallback missing in login mode");
      } else {
        loginDialog.setRegisterMode(false);
        loginDialog.setLoginCallBack(mLoginCallBack);
      }
    }
  }
}
