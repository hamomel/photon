package com.hamom.photon.ui.screens.detail_photo;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcel;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.hamom.photon.R;
import com.hamom.photon.data.network.requests.SignInReq;
import com.hamom.photon.data.storage.realm.Album;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.di.DaggerComponentFactory;
import com.hamom.photon.di.scopes.DaggerScope;
import com.hamom.photon.eventBus.EventBus;
import com.hamom.photon.eventBus.events.FirebaseAnaliticsEvent;
import com.hamom.photon.ui.activities.root.RootActivity;
import com.hamom.photon.ui.screens.author.AuthorScreen;
import com.hamom.photon.ui.screens.base.AbstractScreen;
import com.hamom.photon.flow.Screen;
import com.hamom.photon.ui.screens.base.AbstractPresenter;
import com.hamom.photon.utils.App;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import dagger.Provides;
import flow.Flow;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import mortar.MortarScope;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by hamom on 03.06.17.
 */

@Screen(R.layout.screen_photo_detail)
public class PhotoCardDetailScreen extends AbstractScreen<RootActivity.Component> {
  private static String TAG = ConstantManager.TAG_PREFIX + "PhotoScreen: ";
  private final AbstractScreen mParentScreen;
  private String mPhotoId;

  //parentScreen is necessary for distinguish same screens in different histories
  public PhotoCardDetailScreen(String photoId, AbstractScreen parentScreen) {
    mParentScreen = parentScreen;
    mPhotoId = photoId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;
    return false;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + mParentScreen.hashCode();
    result = 31 * result + mPhotoId.hashCode();
    return result;
  }

  @Override
  public Object createScreenComponent(RootActivity.Component parentComponent) {
    return DaggerComponentFactory.createComponent(parentComponent, Component.class, new Module());
  }

  //region===================== DI ==========================
  @DaggerScope(PhotoCardDetailScreen.class)
  @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
  public interface Component {
    void inject(Presenter presenter);

    void inject(PhotoCardDetailView view);
  }

  @dagger.Module
  class Module {
    @Provides
    @DaggerScope(PhotoCardDetailScreen.class)
    Presenter providePhotoCardDetailPresenter() {
      return new Presenter();
    }

    @Provides
    @DaggerScope(PhotoCardDetailScreen.class)
    PhotoCardDetailModel providePhotoCardDetailModel() {
      return new PhotoCardDetailModel();
    }
  }
  //endregion

  //region===================== Parcelable ==========================
  public static final Creator<PhotoCardDetailScreen> CREATOR =
      new Creator<PhotoCardDetailScreen>() {
        @Override
        public PhotoCardDetailScreen createFromParcel(Parcel source) {
          return new PhotoCardDetailScreen(source.readString(),
              source.readParcelable(AbstractScreen.class.getClassLoader()));
        }

        @Override
        public PhotoCardDetailScreen[] newArray(int size) {
          return new PhotoCardDetailScreen[size];
        }
      };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(mParentScreen, flags);
    dest.writeString(mPhotoId);
  }
  //endregion

  //region===================== Presenter ==========================
  class Presenter extends AbstractPresenter<PhotoCardDetailView, PhotoCardDetailModel> {
    private String TAG = ConstantManager.TAG_PREFIX + "DetailPres: ";
    private PhotoCard mPhotoCard;
    private Bitmap mPhotoBitmap;

    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(ConstantManager.DAGGER_SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initToolbar() {
      if (AppConfig.DEBUG) Log.d(TAG, "initToolbar hasView: " + hasView());

      if (hasView()) {
        getView().initToolbar(mRootPresenter.toolbarBuilder(), isFavoritePhoto(), isOwner());
      }
    }

    private boolean isOwner() {
      String currentUserId = mModel.getCurrentUserId();
      PhotoCard photoCard = mModel.getPhotoCardById(mPhotoId);
      return photoCard.getOwner().equals(currentUserId);
    }

    private boolean isFavoritePhoto() {
      User currentUser = mModel.getCurrentUser();
      if (currentUser == null) return false;
      for (PhotoCard photo : getFavoriteAlbum(currentUser).getPhotocards()) {
        if (mPhotoId.equals(photo.getId())) return true;
      }
      return false;
    }

    private Album getFavoriteAlbum(User currentUser) {
      Album favoriteAlbum = currentUser.getAlbums().get(0);
      for (Album album : currentUser.getAlbums()) {
        if (album.isFavorite()) favoriteAlbum = album;
      }
      return favoriteAlbum;
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
      super.onEnterScope(scope);
      addPhotoCardView();
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
      super.onLoad(savedInstanceState);
      subscribeOnPhotoCardObs();
    }

    private void addPhotoCardView() {
      mModel.addPhotoCardView(mPhotoId);
    }

    private void subscribeOnPhotoCardObs() {
      if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnPhotoCardObs: ");

      mCompositeDisposable.add(mModel.getPhotoCardByIdObs(mPhotoId).subscribe(photoCard -> {
        mPhotoCard = photoCard;
        if (hasView()) {
          getView().initView(photoCard);
        }
        if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnPhotoCardObs: " + photoCard.getOwner());

        subscribeOnUserObs(photoCard.getOwner());
      }, throwable -> getRootView().showError(throwable)));
    }

    private void subscribeOnUserObs(String userId) {

      if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnUserObs: " + userId);

      mCompositeDisposable.add(mModel.getUserObs(userId).subscribe(user -> {
        if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnUserObs: " + user);
        if (hasView()) {
          getView().setUser(user);
        }
      }, throwable -> getRootView().showError(throwable)));
    }

    public void onShareClick(Drawable drawable) {
      mPhotoBitmap = getBitmapFromDrawable(drawable);

      if (checkWritePermissions(ConstantManager.SHARE_PERMISSIONS_REQUEST)) {
        sharePhoto();
      } else {
        subscribeOnPermissionResult();
      }
    }

    private Bitmap getBitmapFromDrawable(Drawable drawable) {
      Bitmap bmp;
      if (drawable instanceof GlideBitmapDrawable) {
        bmp = ((GlideBitmapDrawable) drawable).getBitmap();
      } else {
        return null;
      }
      return bmp;
    }

    private void sharePhoto() {
      sendFirebaseEvent();
      Uri bmpUri = saveBitmapToFileAndGetUri(mPhotoBitmap);
      if (bmpUri != null) {
        if (hasView()) {
          getView().sendShareIntent(bmpUri);
        } else {
          // TODO: 15.06.17 show error to user
        }
      }
    }

    private void sendFirebaseEvent() {
      if (AppConfig.DEBUG) Log.d(TAG, "sendFirebaseEvent: ");

      Bundle bundle = new Bundle();
      bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mPhotoId);
      String eventType = FirebaseAnalytics.Event.SHARE;
      EventBus.INSTANCE.publish(new FirebaseAnaliticsEvent(eventType, bundle));
    }

    private Uri saveBitmapToFileAndGetUri(Bitmap bmp) {

      Uri bmbUri = null;
      try {
        String fileName = mPhotoCard.getTitle() + "_" + mPhotoCard.getId().hashCode() + ".png";
        File directory = App.getAppContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File file = new File(directory, fileName);
        FileOutputStream outputStream = new FileOutputStream(file);
        bmp.compress(Bitmap.CompressFormat.PNG, 90, outputStream);
        outputStream.close();
        bmbUri =
            FileProvider.getUriForFile(App.getAppContext(), ConstantManager.FILE_PROVIDER_AUTHORITY,
                file);
      } catch (IOException e) {
        getRootView().showError(e);
      }
      return bmbUri;
    }

    private void subscribeOnPermissionResult() {
      mCompositeDisposable.add(mRootPresenter.getPermissionResultObs().subscribe(integer -> {
        switch (integer) {
          case ConstantManager.DOWNLOAD_PERMISSIONS_REQUEST:
            savePhotoCardToDownloadDir(mPhotoBitmap);
            break;
          case ConstantManager.SHARE_PERMISSIONS_REQUEST:
            sharePhoto();
        }
      }));
    }

    void onDownloadClick(Drawable drawable) {
      mPhotoBitmap = getBitmapFromDrawable(drawable);
      if (checkWritePermissions(ConstantManager.DOWNLOAD_PERMISSIONS_REQUEST)) {
        savePhotoCardToDownloadDir(mPhotoBitmap);
      } else {
        subscribeOnPermissionResult();
      }
    }

    private void savePhotoCardToDownloadDir(Bitmap bitmap) {
      try {
        String fileName = mPhotoCard.getTitle() + "_" + mPhotoId.hashCode() + ".jpg";
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(dir, fileName);
        FileOutputStream outputStream = new FileOutputStream(file);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
        outputStream.close();
        getRootView().showMessage(App.getAppContext().getString(R.string.photo_saved));
      } catch (IOException e) {
        getRootView().showError(e);
      }
    }

    private boolean checkWritePermissions(int requestCode) {
      String[] permissions = new String[] { WRITE_EXTERNAL_STORAGE };
      return mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions, requestCode);
    }

    void onAddFavoriteClick() {
      if (mModel.checkAuthUser()) {
        mModel.addToFavorite(mPhotoId);
        initToolbar();
      } else {
        if (hasView()) {
          getView().showLoginDialog();
        }
      }
    }

    void onLogin(SignInReq signInReq) {
      mCompositeDisposable.add(mModel.signIn(signInReq).subscribe(user -> {
        if (!TextUtils.isEmpty(user.getId())) onAddFavoriteClick();
      }, throwable -> getRootView().showError(throwable)));
    }

    void showNoAppFitsMessage(String message) {
      getRootView().showMessage(message);
    }

    void onUserInfoClick() {
      Flow.get(getView()).set(new AuthorScreen(mPhotoCard.getOwner()));
    }

    void onRemoveFavoriteClick() {
      mModel.removeFromFavorite(mPhotoId);
      initToolbar();
    }

    public void onDeleteClick() {
      mModel.deletePhotoCard(mPhotoId);
      //noinspection CheckResult
      Flow.get(getView()).goBack();
    }
  }
  //endregion
}
