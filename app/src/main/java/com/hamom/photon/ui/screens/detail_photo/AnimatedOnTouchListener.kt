package com.hamom.photon.ui.screens.detail_photo

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.support.constraint.ConstraintLayout
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnticipateInterpolator
import android.view.animation.AnticipateOvershootInterpolator
import android.view.animation.CycleInterpolator
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager

/**
 * Created by hamom on 13.07.17.
 */
class AnimatedOnTouchListener(val view: View,
    val parentView: PhotoCardDetailView) : View.OnTouchListener {
  val TAG = ConstantManager.TAG_PREFIX + "AnimOnTouchL: "
  val initialHeight: Int = view.layoutParams.height

  override fun onTouch(v: View?, event: MotionEvent?): Boolean {
    if (AppConfig.DEBUG) Log.d(TAG, "onTouch: " + event?.action)
    val layoutParams: ConstraintLayout.LayoutParams = v?.layoutParams as ConstraintLayout.LayoutParams

    when (event?.action) {

      MotionEvent.ACTION_MOVE -> {
        if (event.historySize > 0) {
          val deltaY: Float = event.y - event.getHistoricalY(0)
          if ((deltaY > 0 || layoutParams.height > initialHeight) && layoutParams.height < parentView.height) {
            val newHeight: Float = v.height + deltaY
            layoutParams.height = newHeight.toInt()
            v.layoutParams = layoutParams
          }
        }
      }

      MotionEvent.ACTION_UP -> {
        restoreHeightWithAnim(v, layoutParams)
      }
    }
    return true
  }

  fun restoreHeightWithAnim(v: View, params: ConstraintLayout.LayoutParams){
    if (AppConfig.DEBUG) Log.d(TAG, "restoreHeightWithAnim: ")
    val animation: ValueAnimator = ValueAnimator.ofInt(params.height, initialHeight)
    animation.interpolator = AccelerateDecelerateInterpolator()
    animation.addUpdateListener { params.height = it.animatedValue as Int; v.layoutParams = params }
    animation.start()

  }
}