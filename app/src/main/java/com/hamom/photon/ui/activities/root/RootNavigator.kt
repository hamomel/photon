package com.hamom.photon.ui.activities.root

import android.app.Activity
import com.hamom.photon.di.scopes.DaggerScope
import com.hamom.photon.ui.screens.home.HomeScreen
import com.hamom.photon.ui.screens.profile.ProfileScreen
import com.hamom.photon.ui.screens.upload_empty.EmptyUploadScreen
import com.hamom.photon.utils.ConstantManager
import flow.Direction
import flow.Flow
import flow.History
import javax.inject.Inject

/**
 * Created by hamom on 20.07.17.
 */
@DaggerScope(RootActivity::class)
class RootNavigator @Inject constructor() {
  val TAG = ConstantManager.TAG_PREFIX + "RootNavigator: "
  enum class Histories {
    HOME, PROFILE, UPLOAD
  }

  private var activity: Activity? = null
  private var homeHistory: History? = null
  private var profileHistory: History? = null
  private var uploadHistory: History? = null

  fun takeActivity(activity: Activity) {
    this.activity = activity
  }

  fun dropActivity() {
    activity = null
  }

  fun setHistory(history: Histories) {
    saveCurrentHistory()
    when (history) {
      Histories.HOME -> {
        Flow.get(activity!!).setHistory(homeHistory!!, Direction.REPLACE)
      }

      Histories.PROFILE -> {
        if (profileHistory == null) profileHistory = homeHistory!!.buildUpon().clear().push(
            ProfileScreen()).build()
        Flow.get(activity!!).setHistory(profileHistory!!, Direction.REPLACE)
      }

      Histories.UPLOAD -> {
        if (uploadHistory == null) uploadHistory = homeHistory!!.buildUpon().clear().push(
            EmptyUploadScreen()).build()
        Flow.get(activity!!).setHistory(uploadHistory!!, Direction.REPLACE)
      }
    }
  }

  private fun saveCurrentHistory() {
    val current: History = Flow.get(activity!!).history
    val builder: History.Builder = current.buildUpon()
    var firstScreen: Any = builder.pop()

    while (!builder.isEmpty) firstScreen = builder.pop()

    when (firstScreen) {
      is HomeScreen -> homeHistory = current
      is ProfileScreen -> profileHistory = current
      is EmptyUploadScreen -> uploadHistory = current
      else -> throw IllegalStateException("Wrong first screen in history")
    }
  }
}