package com.hamom.photon.ui.screens.search;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.dto.SearchDto;
import com.hamom.photon.ui.custom.TagsView;
import com.hamom.photon.ui.screens.home.HomeScreen;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hamom on 20.06.17.
 */

class SearchPageViewHolder {
  private static String TAG = ConstantManager.TAG_PREFIX + "SearchHolder: ";


  @BindView(R.id.search_tv)
  TextView searchTv;
  @BindView(R.id.suggestion_layout)
  LinearLayout suggestionLayout;
  @BindView(R.id.tags_search)
  TagsView tagsView;
  @BindView(R.id.action_ib)
  ImageButton actionIb;

  private View mView;
  private List<String> mSuggestions = new ArrayList<>();
  private SearchCallback mCallback;
  private int mSelectedTagsCount;

  SearchPageViewHolder(View view, SearchCallback callback) {
    mView = view;
    mCallback = callback;
    ButterKnife.bind(this, view);
    initOnEditorChangeListener();
    initOnSelectionChangeListener();
    setPreviewsState();
  }

  private void setPreviewsState() {
    SearchDto search = HomeScreen.getSearch();
    if (search.isEmpty()){
      changeActionButtonState();
    } else {
      searchTv.setText(search.getSearchPhrase());
      selectTags(search.getTags());
      changeActionButtonState();
    }
  }

  private void selectTags(List<String> tags) {
    for (String tag : tags) {
      tagsView.selectTag(tag);
    }
  }

  private void initOnEditorChangeListener() {
    searchTv.setOnEditorActionListener((v, actionId, event) -> {
      if (actionId == EditorInfo.IME_ACTION_SEARCH) performSearch();
      return true;
    });
  }

  private void initOnSelectionChangeListener() {
    tagsView.setOnSelectionChangeListener(selectedCount -> {
      mSelectedTagsCount = selectedCount;
      changeActionButtonState();
    });
  }

  private void changeActionButtonState() {
    if (mSelectedTagsCount > 0 || !searchTv.getText().toString().trim().isEmpty()){
      actionIb.setImageDrawable(ContextCompat.getDrawable(mView.getContext(),R.drawable.ic_done_black_24dp));
      actionIb.setOnClickListener(v -> performSearch());
    } else {
      actionIb.setImageDrawable(ContextCompat.getDrawable(mView.getContext(),R.drawable.ic_arrow_back_black_24dp));
      actionIb.setOnClickListener(v -> goBack());
    }
  }

  private void performSearch() {
    hideKeyboard();
    mCallback.apply(new SearchDto(searchTv.getText().toString(), tagsView.getSelectedTags()));
  }

  private void goBack() {
    hideKeyboard();
    mCallback.apply(new SearchDto());
  }


  private void hideKeyboard() {
    if (searchTv.hasFocus()){
      InputMethodManager imm = ((InputMethodManager) searchTv.getContext()
          .getSystemService(Context.INPUT_METHOD_SERVICE));
      imm.hideSoftInputFromWindow(searchTv.getWindowToken(), 0);
    }
  }

  @OnClick(R.id.clear_ib)
  void onButtonClick(View v) {
    searchTv.setText("");
    tagsView.clearSelections();
  }

  @OnTextChanged(R.id.search_tv)
  void onTextChanged(CharSequence text) {
    changeActionButtonState();
    setSuggestions(text.toString());
  }

  void addSuggestion(String s) {
    if (AppConfig.DEBUG) Log.d(TAG, "addSuggestion: " + s);

    if (!mSuggestions.contains(s)){
      mSuggestions.add(s);
      setSuggestions(searchTv.getText().toString());
    }
  }

  private void setSuggestions(String s) {
    suggestionLayout.removeAllViews();
    for (String suggestion : mSuggestions) {
      String text = "";
      if (s.isEmpty()) {
        text = suggestion;
      } else {
        if (suggestion.contains(s)) {
          text = suggestion;
        }
      }
      addSuggestionTextView(text);
    }
  }

  private void addSuggestionTextView(String text) {
    if (!text.isEmpty()) {
      TextView textView = getTextView(text);
      suggestionLayout.addView(textView);
    }
  }

  @NonNull
  private TextView getTextView(String text) {
    TextView textView = new TextView(suggestionLayout.getContext());
    LinearLayout.LayoutParams params =
        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
    textView.setLayoutParams(params);
    textView.setText(text);
    textView.setTextColor(ContextCompat.getColor(textView.getContext(), R.color.color_text_dark));
    textView.setOnClickListener(v -> searchTv.setText(((TextView) v).getText()));
    return textView;
  }

  void addTag(String s) {
    if (AppConfig.DEBUG) Log.d(TAG, "addTag: " + s);

    tagsView.addTag(s);
  }

  interface SearchCallback {
    void apply(SearchDto dto);
  }
}
