package com.hamom.photon.ui.custom;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.utils.ConstantManager;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by hamom on 05.06.17.
 */

public class UserInfoView extends ConstraintLayout {
  private static String TAG = ConstantManager.TAG_PREFIX + "UserInfoView: ";
  @BindView(R.id.avatar_iv)
  CircleImageView avatarIv;
  @BindView(R.id.name_first_part_tv)
  TextView nameFirstPartTv;
  @BindView(R.id.name_second_part_tv)
  TextView nameSecondPartTv;
  @BindView(R.id.user_albums_tv)
  TextView userAlbumsTv;
  @BindView(R.id.user_cards_tv)
  TextView userCardsTv;

  private boolean showLogin = false;

  public UserInfoView(Context context, AttributeSet attrs) {
    super(context, attrs);
    resolveAttrs(attrs);
    setSaveEnabled(true);
  }

  private void resolveAttrs(AttributeSet attrs) {
    showLogin = attrs.getAttributeBooleanValue("http://schemas.android.com/apk/res-auto", "showLogin", false);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    ButterKnife.bind(this);
  }

  public void setUser(User user){
    Glide.with(getContext()).load(user.getAvatar()).into(avatarIv);
    resolveNameFields(user);
    userAlbumsTv.setText(String.valueOf(user.getAlbumCount()));
    userCardsTv.setText(String.valueOf(user.getPhotocardCount()));
  }

  private void resolveNameFields(User user) {
    if (showLogin){
      nameFirstPartTv.setText(user.getLogin());
      String name =getContext().getString(R.string.name_divider, user.getName());
      nameSecondPartTv.setText(name);
    } else {
      nameFirstPartTv.setText(user.getName());
    }
  }

  public void setShowLogin(boolean showLogin){
    this.showLogin = showLogin;
  }

}
