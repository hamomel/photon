package com.hamom.photon.ui.screens.upload_empty

import android.content.Context
import android.util.AttributeSet
import butterknife.OnClick
import com.hamom.photon.R
import com.hamom.photon.ui.screens.base.AbstractView
import com.hamom.photon.utils.ConstantManager
import mortar.MortarScope

/**
 * Created by hamom on 27.07.17.
 */
class EmptyUploadView(context: Context,
    attrs: AttributeSet) : AbstractView<EmptyUploadScreen.Presenter>(context, attrs) {
  val TAG = ConstantManager.TAG_PREFIX + "EmptyUpView: "
  override fun onBackPressed() = mPresenter.onBackPressed()

  override fun initDagger() {
    MortarScope.getScope(context).getService<EmptyUploadScreen.Component>(
        ConstantManager.DAGGER_SERVICE_NAME).inject(this)
  }

  @OnClick(R.id.choose_btn)
  fun onButtonClock(){
    mPresenter.onChooseClick()
  }
}