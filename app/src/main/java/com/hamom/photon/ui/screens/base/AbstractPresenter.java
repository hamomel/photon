package com.hamom.photon.ui.screens.base;

import android.os.Bundle;
import com.hamom.photon.ui.activities.root.IRootView;
import com.hamom.photon.ui.activities.root.RootPresenter;
import io.reactivex.disposables.CompositeDisposable;
import io.realm.annotations.PrimaryKey;
import javax.inject.Inject;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * Created by hamom on 01.06.17.
 */

public abstract class AbstractPresenter<V extends AbstractView, M extends AbstractModel>
    extends ViewPresenter<V> {

  @Inject
  protected M mModel;
  @Inject
  protected RootPresenter mRootPresenter;

  protected CompositeDisposable mCompositeDisposable;

  protected abstract void initDagger(MortarScope scope);

  protected IRootView getRootView(){
    return mRootPresenter.getRootView();
  }

  @Override
  protected void onEnterScope(MortarScope scope) {
    super.onEnterScope(scope);
    initDagger(scope);
    mCompositeDisposable = new CompositeDisposable();
  }

  @Override
  protected void onExitScope() {
    super.onExitScope();
    mCompositeDisposable.dispose();
  }

  @Override
  protected void onLoad(Bundle savedInstanceState) {
    super.onLoad(savedInstanceState);
    initToolbar();
  }

  protected abstract void initToolbar();

}
