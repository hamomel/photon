package com.hamom.photon.ui.screens.auth;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import butterknife.OnClick;
import com.hamom.photon.R;
import com.hamom.photon.ui.activities.root.RootPresenter;
import com.hamom.photon.ui.custom.LoginDialog;
import com.hamom.photon.ui.screens.base.AbstractView;
import com.hamom.photon.utils.ConstantManager;
import mortar.MortarScope;

/**
 * Created by hamom on 04.06.17.
 */

class AuthView extends AbstractView<AuthScreen.AuthPresenter> {

  @OnClick({R.id.register_btn, R.id.enter_btn})
  void onButtonClick(Button button){
    switch (button.getId()){
      case R.id.register_btn:
        showRegisterDialog();
        break;
      case R.id.enter_btn:
        showEnterDialog();
        break;
    }
  }

  private void showEnterDialog() {
    new LoginDialog.Builder(getContext(), this).setRegisterMode(false)
        .setSignInCallBack(loginReq -> mPresenter.onLoginClick(loginReq))
        .build().show();
  }

  private void showRegisterDialog() {
    new LoginDialog.Builder(getContext(), this).setRegisterMode(true)
        .setRegisterCallback(registerReq -> mPresenter.onRegisterClock(registerReq))
        .build().show();
  }

  public AuthView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  public boolean onBackPressed() {
    return false;
  }

  @Override
  protected void initDagger() {
    ((AuthScreen.Component) MortarScope.getScope(getContext())
        .getService(ConstantManager.DAGGER_SERVICE_NAME)).inject(this);
  }
}
