package com.hamom.photon.ui.custom

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.design.widget.TextInputLayout
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.OnTextChanged
import com.bumptech.glide.Glide
import com.hamom.photon.R
import com.hamom.photon.data.network.requests.EditUserReq
import com.hamom.photon.data.storage.dto.EditUserDto
import com.hamom.photon.data.storage.realm.User
import com.hamom.photon.ui.screens.profile.ProfileScreen
import com.hamom.photon.utils.App
import com.hamom.photon.utils.ConstantManager
import de.hdodenhof.circleimageview.CircleImageView
import io.reactivex.Observable
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created by hamom on 01.07.17.
 */
class UserEditDialog(context: Context, parentView: ViewGroup, val presenter: ProfileScreen.Presenter) : AlertDialog(context) {

  @BindView(R.id.avatar_civ) lateinit var avatarCiv: CircleImageView
  @BindView(R.id.edit_name_et) lateinit var nameEt: EditText
  @BindView(R.id.edit_name_til) lateinit var nameTil: TextInputLayout
  @BindView(R.id.edit_login_et) lateinit var loginEt: EditText
  @BindView(R.id.edit_login_til) lateinit var loginTil: TextInputLayout

  private val user: User = presenter.user
  private lateinit var avatar: String

  init {
    window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_edit_user, parentView, false)
    ButterKnife.bind(this, view)
    setView(view)
    setUser()
  }

  @OnTextChanged(R.id.edit_name_et)
  fun onOnNameTextChanged() {
    isValidName()
  }

  @OnTextChanged(R.id.edit_login_et)
  fun onLoginTextChanged() {
    isValidLogin()
  }

  private fun setUser() {
    avatar = user.avatar
    loadPhoto()
    nameEt.setText(user.name)
    loginEt.setText(user.login)
  }

  private fun loadPhoto() {
    Glide.with(App.getAppContext()).load(avatar).into(avatarCiv)
  }

  @OnClick(R.id.ok_btn, R.id.cancel_btn)
  fun onButtonClick(button: Button){
    when(button.id){
      R.id.cancel_btn -> dismiss()
      R.id.ok_btn -> sendCallback()
    }
  }

  fun setAvatarMode(){
    nameTil.visibility = View.GONE
    loginTil.visibility = View.GONE
  }

  private fun sendCallback() {
    if (isValidName() && isValidLogin()){
      presenter.editUser(EditUserDto(nameEt.text.toString(), loginEt.text.toString(), avatar))
      dismiss()
    }
  }

  private fun isValidLogin(): Boolean {
    val pattern: Pattern = Pattern.compile(ConstantManager.LOGIN_REGEXP)
    val matcher: Matcher = pattern.matcher(loginEt.text)
    if(!matcher.matches()){
      loginTil.background = ContextCompat.getDrawable(context, R.drawable.text_input_background_red)
      loginTil.hint = context.getString(R.string.wrong_format)
      loginTil.setHintTextAppearance(R.style.DialogTextInputErrorStyle)
      return false
    } else {
      loginTil.background = ContextCompat.getDrawable(context, R.drawable.text_input_background)
      loginTil.hint = ""
      loginTil.setHintTextAppearance(R.style.DialogTextInputLayoutStyle)
      return true
    }
  }

  private fun isValidName(): Boolean {
    val pattern: Pattern = Pattern.compile(ConstantManager.NAME_REGEXP)
    val matcher: Matcher = pattern.matcher(nameEt.text)
    if(!matcher.matches()){
      nameTil.background = ContextCompat.getDrawable(context, R.drawable.text_input_background_red)
      nameTil.hint = context.getString(R.string.wrong_format)
      nameTil.setHintTextAppearance(R.style.DialogTextInputErrorStyle)
      return false
    } else {
      nameTil.background = ContextCompat.getDrawable(context, R.drawable.text_input_background)
      nameTil.hint = ""
      nameTil.setHintTextAppearance(R.style.DialogTextInputLayoutStyle)
      return true
    }
  }

  @OnClick(R.id.avatar_civ)
  fun onAvatarClick(){
    presenter.getNewAvatar(object : AvatarCallback {
      override fun onPhotoRequest(photoObs: Observable<String>) {
        subscribeOnPhotoObs(photoObs)
      }
    })
  }

  private fun subscribeOnPhotoObs(photoObs: Observable<String>) {
    photoObs.subscribe { photo -> avatar = photo; loadPhoto()}
  }

  interface AvatarCallback {
    fun onPhotoRequest(photoObs: Observable<String>): Unit
  }
}