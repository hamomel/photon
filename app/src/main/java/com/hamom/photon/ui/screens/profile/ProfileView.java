package com.hamom.photon.ui.screens.profile;

import android.content.Context;
import android.os.Parcelable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MenuItem;
import butterknife.BindView;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.ui.activities.root.RootPresenter;
import com.hamom.photon.ui.custom.AddAlbumDialog;
import com.hamom.photon.ui.custom.MenuItemHolder;
import com.hamom.photon.ui.custom.UserEditDialog;
import com.hamom.photon.ui.screens.base.AbstractView;
import com.hamom.photon.ui.screens.base.OnItemClickListener;
import com.hamom.photon.utils.ConstantManager;
import flow.Flow;
import mortar.MortarScope;

/**
 * Created by hamom on 05.06.17.
 */

public class ProfileView extends AbstractView<ProfileScreen.Presenter> {

  @BindView(R.id.profile_recycler)
  RecyclerView profileRecycler;

  private final GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);

  public ProfileView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  public boolean onBackPressed() {
    return false;
  }

  @Override
  protected void initDagger() {
    MortarScope.getScope(getContext())
        .<ProfileScreen.Component>getService(ConstantManager.DAGGER_SERVICE_NAME).inject(this);
  }


  //region===================== Toolbar ==========================
  public void initToolbar(RootPresenter.ToolbarBuilder builder) {
      builder.setTitle(getContext().getResources().getString(R.string.profile))
          .setShowBack(false)
          .addMenuItem(getAddAlbumItem())
          .addMenuItem(getEditItem())
          .addMenuItem(getChangeAvatarItem())
          .addMenuItem(getLogOutItem())
          .build();
  }

  private MenuItemHolder getLogOutItem() {
    return new MenuItemHolder(getContext().getString(R.string.exit), R.drawable.ic_circular_power_on_button, item -> onLogOutClick(), MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private MenuItemHolder getEditItem() {
    return new MenuItemHolder(getContext().getString(R.string.edit_profile), R.drawable.ic_edit, item -> onEditClick(), MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private MenuItemHolder getChangeAvatarItem() {
    return new MenuItemHolder(getContext().getString(R.string.change_avatar), R.drawable.ic_avatar_inside_a_circle, item -> onChangeAvatarClick(), MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private MenuItemHolder getAddAlbumItem() {
    return new MenuItemHolder(getContext().getString(R.string.add_album), R.drawable.ic_new_album, item -> onAddAlbumClick(), MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private boolean onAddAlbumClick() {
    new AddAlbumDialog(getContext(), this, req -> mPresenter.onAddAlbumClick(req)).show();
    return false;
  }

  private boolean onLogOutClick() {
    mPresenter.onLogOutClick();
    return false;
  }

  private boolean onChangeAvatarClick() {
    UserEditDialog dialog = new UserEditDialog(getContext(), this, mPresenter);
    dialog.setAvatarMode();
    dialog.show();
    return false;
  }

  private boolean onEditClick() {
    new UserEditDialog(getContext(), this, mPresenter).show();
    return false;
  }
  //endregion

  void initView(User user) {
    ProfileAdapter adapter = new ProfileAdapter(user, getClickListener());

    mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
      @Override
      public int getSpanSize(int position) {
        return position == 0 ? 2 : 1;
      }
    });
    profileRecycler.setLayoutManager(mLayoutManager);
    profileRecycler.setAdapter(adapter);
  }

  private OnItemClickListener getClickListener(){
    return itemId -> mPresenter.onAlbumClick(itemId);
  }

  public void restoreLayoutManagerState(Parcelable state) {
    if (state != null) mLayoutManager.onRestoreInstanceState(state);
  }

  @Override
  protected Parcelable onSaveInstanceState() {
    mPresenter.saveViewState(mLayoutManager.onSaveInstanceState());
    return super.onSaveInstanceState();
  }
}
