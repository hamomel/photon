package com.hamom.photon.ui.screens.base;

/**
 * Created by hamom on 04.06.17.
 */

public interface IBackView {

  boolean onBackPressed();
}
