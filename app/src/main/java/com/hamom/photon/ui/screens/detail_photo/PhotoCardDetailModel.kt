package com.hamom.photon.ui.screens.detail_photo

import android.text.TextUtils
import com.hamom.photon.data.network.requests.SignInReq
import com.hamom.photon.data.storage.realm.PhotoCard
import com.hamom.photon.data.storage.realm.User
import com.hamom.photon.jobs.jobs.AddFavoriteJob
import com.hamom.photon.jobs.jobs.AddViewJob
import com.hamom.photon.jobs.jobs.DeletePhotoCardJob
import com.hamom.photon.jobs.jobs.RemoveFavoriteJob
import com.hamom.photon.ui.screens.base.AbstractModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by hamom on 03.06.17.
 */

internal class PhotoCardDetailModel : AbstractModel() {

  fun addPhotoCardView(photoId: String) {
    mJobManager.addJobInBackground(AddViewJob(photoId))
  }

  fun signIn(signInReq: SignInReq): Observable<User> {
    return mDataManager.signIn(signInReq)
  }


  fun addToFavorite(photoId: String) {
    mJobManager.addJobInBackground(AddFavoriteJob(photoId))
  }

  fun removeFromFavorite(photoId: String) {
    mJobManager.addJobInBackground(RemoveFavoriteJob(photoId))
  }

  fun checkAuthUser(): Boolean {
    return !TextUtils.isEmpty(mDataManager.currentUserId)
  }

  fun getUserObs(userId: String): Observable<User> {
    val user: User? = mRealmManager.getUserById(userId)

    if (user != null) return Observable.mergeDelayError(createUserObs(user),
        createUserDownLoadObs(userId))
        .flatMap { if (it is User) Observable.just(it) else Observable.empty() }
    else return createUserDownLoadObs(userId)
        .observeOn(AndroidSchedulers.mainThread())
        .flatMap { createUserObs(mRealmManager.getUserById(userId))
    }
  }

  private fun createUserDownLoadObs(userId: String) = mDataManager.downloadUserObs(userId)

  private fun createUserObs(user: User?): Observable<User> = Observable.create { e ->
    if (!e.isDisposed) {
      if (user != null) e.onNext(user)
      user?.addChangeListener { t: User, _ -> e.onNext(t) }
    }
  }

  fun getCurrentUser(): User? = mRealmManager.getUserById(mDataManager.currentUserId)

  val currentUserId: String
    get() = mDataManager.currentUserId

  fun getPhotoCardByIdObs(photoId: String): Observable<PhotoCard> {
    val photoCard: PhotoCard = mRealmManager.getPhotoCardById(photoId)

    return Observable.mergeDelayError(createPhotoObs(photoCard),
        mDataManager.downloadPhotoCardById(photoCard.owner, photoId))
        .flatMap { if (it is PhotoCard) Observable.just(it) else Observable.empty() }
  }

  private fun createPhotoObs(photoCard: PhotoCard): Observable<PhotoCard> = Observable.create { e ->
    if (!e.isDisposed) {
      e.onNext(photoCard)
      photoCard.addChangeListener { t: PhotoCard, _ -> e.onNext(t) }
    }
  }

  fun getPhotoCardById(photoId: String): PhotoCard {
    return mRealmManager.getPhotoCardById(photoId)
  }

  fun deletePhotoCard(photoId: String) {
    mJobManager.addJobInBackground(DeletePhotoCardJob(photoId))
  }
}
