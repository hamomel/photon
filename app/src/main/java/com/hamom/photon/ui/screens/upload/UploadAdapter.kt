package com.hamom.photon.ui.screens.upload

import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup

/**
 * Created by hamom on 26.07.17.
 */
class UploadAdapter(val pages: List<View>) : PagerAdapter() {
  override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
    return view == `object`
  }

  override fun getCount() = 3

  override fun instantiateItem(container: ViewGroup?, position: Int): Any {
    container?.addView(pages[position])
    return pages[position]
  }

  override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
    container?.removeView(`object` as View)
  }
}