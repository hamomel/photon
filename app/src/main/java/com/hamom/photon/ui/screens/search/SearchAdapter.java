package com.hamom.photon.ui.screens.search;

import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hamom on 12.06.17.
 */

public class SearchAdapter extends PagerAdapter {
  private static String TAG = ConstantManager.TAG_PREFIX + "SearchAdapter: ";
  public static final String TITLE_SEARCH = "ПОИСК";
  public static final String TITLE_FILTERS = "ФИЛЬТРЫ";

  private List<View> pages;

  public SearchAdapter(List<View> pages) {
    if (AppConfig.DEBUG) Log.d(TAG, "SearchAdapter: " + pages);

    this.pages = pages;
  }

  @Override
  public int getCount() {
    return pages.size();
  }

  @Override
  public boolean isViewFromObject(View view, Object object) {
    return view.equals(object);
  }

  @Override
  public CharSequence getPageTitle(int position) {
    return position == 0 ? TITLE_SEARCH : TITLE_FILTERS;
  }

  @Override
  public Object instantiateItem(ViewGroup container, int position) {
    View v = pages.get(position);
    container.addView(v);
    if (AppConfig.DEBUG) Log.d(TAG, "instantiateItem: " + v);

    return v;
  }


}
