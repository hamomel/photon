package com.hamom.photon.ui.activities.splash;

import android.text.TextUtils;
import android.util.Log;
import com.hamom.photon.data.managers.DataManager;
import com.hamom.photon.data.managers.RealmManager;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.di.scopes.SplashScope;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

/**
 * Created by hamom on 29.05.17.
 */

@SplashScope
class SplashModel {
  private static String TAG = ConstantManager.TAG_PREFIX + "SplashModel: ";
  private DataManager mDataManager;
  private RealmManager mRealmManager;

  @Inject
  SplashModel(DataManager dataManager, RealmManager realmManager) {
    mDataManager = dataManager;
    mRealmManager = realmManager;
  }

  private Observable<PhotoCard> startPhotoDownload(){
    RealmResults<PhotoCard> photoCards = mRealmManager.getAllPhotoCard();
    if (photoCards.isEmpty()){
      return mDataManager.downloadAllPhotoToRealm()
          .doOnNext(photoCard -> {
                if (AppConfig.DEBUG) Log.d(TAG, "startPhotoDownload noPhoto: " + photoCard);
              })
          .timeout(5000, TimeUnit.MILLISECONDS)
          .subscribeOn(Schedulers.io());
    } else {
      return mDataManager.downloadAllPhotoToRealm()
          .doOnNext(photoCard -> {
            if (AppConfig.DEBUG) Log.d(TAG, "startPhotoDownload photoExist: " + photoCard);
          })
          .timeout(5000, TimeUnit.MILLISECONDS)
          .subscribeOn(Schedulers.io());
    }
  }

  private Observable<Long> getTimerObs(){
    return Observable.timer(3000, TimeUnit.MILLISECONDS)
        .doOnNext(aLong -> {
          if (AppConfig.DEBUG) Log.d(TAG, "getTimerObs: " + aLong);
        });
  }

  Observable<Boolean> getEndDownloadObs(){
    return Observable.zip(getTimerObs(), startPhotoDownload().buffer(60), (t1, t2) -> true);
  }

  Observable<User> getUserObs(){
    String userId = mDataManager.getCurrentUserId();
    if (TextUtils.isEmpty(userId)){
      return Observable.empty();
    } else {
      return mDataManager.downloadUserObs(userId).flatMap(aBoolean -> Observable.empty());
    }
  }

}
