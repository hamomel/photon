package com.hamom.photon.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hamom.photon.R;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by hamom on 11.06.17.
 */

public class FilterItemView extends FrameLayout {
  private static String TAG = ConstantManager.TAG_PREFIX + "FilterIView: ";

  @BindView(R.id.icon)
  ImageView icon;
  @BindView(R.id.title)
  TextView title;
  @BindView(R.id.color_icon)
  CircleImageView colorIcon;

  @DrawableRes int iconRes;
  @StringRes int titleRes;
  @ColorRes int colorRes;

  private boolean isChecked;
  private OnCheckedChangeListener onCheckedChangeListener;

  public FilterItemView(@NonNull Context context) {
    super(context);
  }

  public FilterItemView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    isChecked = false;

    View view = LayoutInflater.from(context).inflate(R.layout.view_item_filter, this, true);
    ButterKnife.bind(this, view);
    resolveAttrs(attrs);
    setDefaultColor();
    setOnClickListener(v -> onClick());
  }

  private void resolveAttrs(AttributeSet attrs) {
    if (attrs == null || isInEditMode()) {
      return;
    }
    TypedArray a =
        getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.FilterItemView, 0, 0);

    try {
      titleRes = a.getResourceId(R.styleable.FilterItemView_titleRes, 0);
      colorRes = a.getResourceId(R.styleable.FilterItemView_colorRes, 0);
      iconRes = a.getResourceId(R.styleable.FilterItemView_iconRes, 0);
    } finally {
      a.recycle();
    }

    setResources();
  }

  private void setResources() {
    if (titleRes != 0) title.setText(titleRes);
    if (colorRes != 0){
      colorIcon.setImageResource(colorRes);
      colorIcon.setVisibility(VISIBLE);
      icon.setVisibility(GONE);
    } else {
      if (iconRes != 0){
        icon.setImageResource(iconRes);
      }
    }
  }

  public void setOnCheckedChangeListener(OnCheckedChangeListener listener){
    onCheckedChangeListener = listener;
  }


  private void onClick() {
    if (isChecked) {
      setDefaultColor();
    } else {
      setCheckedColor();
    }
    isChecked = !isChecked;
    if (onCheckedChangeListener != null)
      onCheckedChangeListener.onCheckedCange(getId(), isChecked());
  }

  private void setDefaultColor() {
    int color = ContextCompat.getColor(getContext(), R.color.color_search_item_default);
    title.setTextColor(color);
    colorIcon.setBorderColor(color);
    Drawable drawable = icon.getDrawable();
    if (drawable != null) {
      DrawableCompat.setTint(icon.getDrawable(), color);
    }
  }

  private void setCheckedColor() {
    int color = ContextCompat.getColor(getContext(), R.color.color_accent);
    title.setTextColor(color);
    colorIcon.setBorderColor(color);
    Drawable drawable = icon.getDrawable();
    if (drawable != null) {
      DrawableCompat.setTint(icon.getDrawable(), color);
    }
  }

  public String getTitle(){
    return getContext().getString(titleRes);
  }

  public boolean isChecked() {
    return isChecked;
  }

  @Override
  protected Parcelable onSaveInstanceState() {
    if (AppConfig.DEBUG) Log.d(TAG, "onSaveInstanceState: ");

    Parcelable superState = super.onSaveInstanceState();
    State state = new State(superState);
    state.isChecked = isChecked();
    return state;
  }

  @Override
  protected void onRestoreInstanceState(Parcelable state) {
    if (AppConfig.DEBUG) Log.d(TAG, "onRestoreInstanceState: " + ((State) state).isChecked);

    super.onRestoreInstanceState(((State) state).getSuperState());
    this.isChecked = ((State) state).isChecked;
    setCheckedState();
  }

  private void setCheckedState() {
    if (isChecked){
      setCheckedColor();
    } else {
      setDefaultColor();
    }
  }

  public void setChecked(boolean isChecked) {
    this.isChecked = isChecked;
    setCheckedState();
  }

  private static class State extends BaseSavedState{
    boolean isChecked;

    State(Parcel source) {
      super(source);
      boolean[] arr = new boolean[1];
      source.readBooleanArray(arr);
      this.isChecked = arr[0];
    }

    State(Parcelable superState) {
      super(superState);
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
      super.writeToParcel(out, flags);
      boolean[] arr = new boolean[]{isChecked};
      out.writeBooleanArray(arr);
    }

    public static final Parcelable.Creator<State> CREATOR = new Creator<State>() {
      @Override
      public State createFromParcel(Parcel source) {
        return new State(source);
      }

      @Override
      public State[] newArray(int size) {
        return new State[size];
      }
    };
  }

  public interface OnCheckedChangeListener {
    void onCheckedCange(@IdRes int viewId, boolean isChecked);
  }

}
