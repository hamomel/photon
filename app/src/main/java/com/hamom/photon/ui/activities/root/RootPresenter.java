package com.hamom.photon.ui.activities.root;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import com.hamom.photon.R;
import com.hamom.photon.data.managers.DataManager;
import com.hamom.photon.di.scopes.DaggerScope;
import com.hamom.photon.ui.custom.MenuItemHolder;
import com.hamom.photon.utils.App;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import com.hamom.photon.utils.FileUriFromContentProvider;
import flow.History;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import java.util.ArrayList;
import java.util.List;
import mortar.Presenter;
import mortar.bundler.BundleService;

/**
 * Created by hamom on 01.06.17.
 */
@DaggerScope(RootActivity.class)
public class RootPresenter extends Presenter<IRootView>{
  private static String TAG = ConstantManager.TAG_PREFIX + "RootPresenter: ";

  private DataManager mDataManager;
  private PublishSubject<Integer> mPermissionSubject;
  private PublishSubject<String> mPhotoSubject;

  public RootPresenter() {
    mDataManager = App.getAppComponent().getDataManager();
  }

  @Override
  protected BundleService extractBundleService(IRootView view) {
    return BundleService.getBundleService(((RootActivity) view));
  }

  public IRootView getRootView() {
    return getView();
  }

  public boolean checkPermissionsAndRequestIfNotGranted(String[] permissions, int reqCode){
    boolean allGranted = true;
    for (String permission : permissions) {
      int selfPermission = ContextCompat.checkSelfPermission(((RootActivity) getView()), permission);
      if (selfPermission != PackageManager.PERMISSION_GRANTED){
        allGranted = false;
        break;
      }
    }
    if (!allGranted){
      requestPermissions(permissions, reqCode);
    }
    return allGranted;
  }

  private void requestPermissions(String[] permissions, int reqCode) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
      mPermissionSubject = PublishSubject.create();
      ((RootActivity) getView()).requestPermissions(permissions, reqCode);
    }
  }

  void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    if (AppConfig.DEBUG) Log.d(TAG, "onRequestPermissionsResult: " + grantResults);

    boolean allGranted = true;
    if (grantResults.length > 0){
      for (int grantResult : grantResults) {
        if (grantResult != PackageManager.PERMISSION_GRANTED){
          allGranted = false;
          break;
        }
      }
    } else {
      allGranted = false;
    }

    if (allGranted){
      mPermissionSubject.onNext(requestCode);
    } else {
      Activity activity = ((RootActivity) getView());

      getView().showSnackBar(activity.getString(R.string.should_grant_permissions),
          activity.getString(R.string.grant), R.color.color_accent,
          v -> openAppSettings(), 0);
    }
  }

  private void openAppSettings() {
    getView().openAppSettings();
  }

  public Observable<Integer> getPermissionResultObs() {
    if (mPermissionSubject == null){
      mPermissionSubject = PublishSubject.create();
    }
    return mPermissionSubject.hide();
  }

  public void requestPhotoFromGallery(){
    Intent intent = new Intent();
    intent.setType("image/*");
    intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
    intent.addCategory(Intent.CATEGORY_OPENABLE);
    ((RootActivity) getView()).startActivityForResult(intent,
        ConstantManager.REQUEST_PHOTO_FROM_GALLERY);
  }

  public Observable<String> getPhotoObs(){
    if (mPhotoSubject == null){
      mPhotoSubject = PublishSubject.create();
    }
    return mPhotoSubject.hide();
  }

  void onActivityResult(int requestCode, int resultCode, Intent data){
    if (resultCode != Activity.RESULT_OK){
      return;
    }

    if (requestCode == ConstantManager.REQUEST_PHOTO_FROM_GALLERY){
      Uri uri = data.getData();
      String fileUri = "file://" + FileUriFromContentProvider.getPath(((RootActivity) getView()), uri);
      mPhotoSubject.onNext(fileUri);
    }
  }

  public ToolbarBuilder toolbarBuilder(){
    return new ToolbarBuilder(getView());
  }

  public static class ToolbarBuilder {
    private boolean mIsVisible = true;
    private String mTitle = "";
    private boolean mShowBack = false;
    private List<MenuItemHolder> mMenuItems = new ArrayList<>();
    private IRootView mRootView;
    private @DrawableRes int mOverflowButtonIcon = 0;
    private ViewPager mViewPager;

    ToolbarBuilder(IRootView rootView) {
      mRootView = rootView;
    }

    public ToolbarBuilder setOverFlowButtonRes(@DrawableRes int iconRes){
      mOverflowButtonIcon = iconRes;
      return this;
    }

    public ToolbarBuilder setVisible(boolean visible) {
      mIsVisible = visible;
      return this;
    }

    public ToolbarBuilder setTitle(String title) {
      mTitle = title;
      return this;
    }

    public ToolbarBuilder setShowBack(boolean showBack) {
      mShowBack = showBack;
      return this;
    }

    public ToolbarBuilder addMenuItem(MenuItemHolder item){
      mMenuItems.add(item);
      return this;
    }

    public ToolbarBuilder setTabs(ViewPager pager){
      this.mViewPager = pager;
      return this;
    }

    public void build(){
      mRootView.setToolbarVisible(mIsVisible);
      mRootView.setOverFlowButtonIcon(mOverflowButtonIcon);
      mRootView.setToolbarTitle(mTitle);
      mRootView.setMenuItems(mMenuItems);
      mRootView.setShowBackButton(mShowBack);
      if (mViewPager != null){
        mRootView.setTabs(mViewPager);
      } else {
        mRootView.removeTabs();
      }

    }
  }
}
