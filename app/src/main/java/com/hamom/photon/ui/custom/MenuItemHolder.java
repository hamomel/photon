package com.hamom.photon.ui.custom;

import android.support.annotation.IdRes;
import android.view.MenuItem;

/**
 * Created by hamom on 08.06.17.
 */

public class MenuItemHolder {
  private String mTitle;
  private @IdRes int mItemResId;
  private MenuItem.OnMenuItemClickListener mListener;
  private int mShowAsAction;

  public MenuItemHolder(String title, int itemResId, MenuItem.OnMenuItemClickListener listener,
      int showAsAction) {
    mTitle = title;
    mItemResId = itemResId;
    mListener = listener;
    mShowAsAction = showAsAction;
  }

  public String getTitle() {
    return mTitle;
  }

  public void setTitle(String title) {
    mTitle = title;
  }

  public int getItemResId() {
    return mItemResId;
  }

  public void setItemResId(int itemResId) {
    mItemResId = itemResId;
  }

  public MenuItem.OnMenuItemClickListener getListener() {
    return mListener;
  }

  public void setListener(MenuItem.OnMenuItemClickListener listener) {
    mListener = listener;
  }

  public int getShowAsAction() {
    return mShowAsAction;
  }

  public void setShowAsAction(int showAsAction) {
    mShowAsAction = showAsAction;
  }
}
