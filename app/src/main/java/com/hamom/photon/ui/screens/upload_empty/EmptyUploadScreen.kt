package com.hamom.photon.ui.screens.upload_empty

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.hamom.photon.R
import com.hamom.photon.di.DaggerComponentFactory
import com.hamom.photon.di.scopes.DaggerScope
import com.hamom.photon.eventBus.EventBus
import com.hamom.photon.eventBus.events.ErrorEvent
import com.hamom.photon.flow.Screen
import com.hamom.photon.ui.activities.root.RootActivity
import com.hamom.photon.ui.screens.auth.AuthScreen
import com.hamom.photon.ui.screens.base.AbstractPresenter
import com.hamom.photon.ui.screens.base.AbstractScreen
import com.hamom.photon.ui.screens.upload.UploadScreen
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager
import dagger.Provides
import flow.Direction
import flow.Flow
import mortar.MortarScope

/**
 * Created by hamom on 27.07.17.
 */
@Screen(R.layout.screen_upload_empty)
class EmptyUploadScreen(val parentScreen: AbstractScreen<Any>? = null,
    val albumId: String = "") : AbstractScreen<RootActivity.Component>() {

  override fun createScreenComponent(parentComponent: RootActivity.Component?) =
      DaggerComponentFactory.createComponent(parentComponent, Component::class.java, Module(this))!!

  //region====================== Parcelable ============================
  override fun describeContents(): Int = 0

  override fun writeToParcel(dest: Parcel?, flags: Int) {
    dest?.writeString(albumId)
    dest?.writeParcelable(parentScreen, flags)
  }

  companion object CREATOR : Parcelable.Creator<EmptyUploadScreen> {
    override fun createFromParcel(parcel: Parcel): EmptyUploadScreen {
      return EmptyUploadScreen(parcel)
    }

    override fun newArray(size: Int): Array<EmptyUploadScreen?> {
      return arrayOfNulls(size)
    }
  }

  constructor(parcel: Parcel) : this(parcel.readParcelable(AbstractScreen::class.java.classLoader),
      parcel.readString())

//endregion

//region====================== DI ============================
@dagger.Module
class Module(val screen: EmptyUploadScreen) {

  @Provides
  @DaggerScope(EmptyUploadScreen::class)
  fun provideModel(): EmptyUploadModel = EmptyUploadModel()

  @Provides
  @DaggerScope(EmptyUploadScreen::class)
  fun providePresenter(): Presenter = screen.Presenter()
}

@dagger.Component(modules = arrayOf(Module::class),
    dependencies = arrayOf(RootActivity.Component::class))
@DaggerScope(EmptyUploadScreen::class)
interface Component {
  fun inject(presenter: Presenter)
  fun inject(view: EmptyUploadView)
}
//endregion

//region====================== Presenter ============================
inner class Presenter : AbstractPresenter<EmptyUploadView, EmptyUploadModel>() {
  val TAG = ConstantManager.TAG_PREFIX + "Presenter: "

  override fun initDagger(scope: MortarScope?) {
    if (AppConfig.DEBUG) Log.d(TAG, "initDagger: ")
    scope?.getService<Component>(ConstantManager.DAGGER_SERVICE_NAME)?.inject(this)
  }

  override fun initToolbar() {
    if (AppConfig.DEBUG) Log.d(TAG, "initToolbar: ")
    mRootPresenter.toolbarBuilder().setVisible(false).build()
  }

  override fun onLoad(savedInstanceState: Bundle?) {
    checkUserAuth()
    super.onLoad(savedInstanceState)
  }

  private fun checkUserAuth() {
    if (!mModel.isAuthUser()) Flow.get(view).set(AuthScreen(this@EmptyUploadScreen))
  }

  fun onChooseClick() {
    if (checkReadPermissions()) {
      requestPhoto()
    } else {
      subscribeOnPermissionObs()
    }
  }

  private fun checkReadPermissions(): Boolean {
    val permissions: Array<String> = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    return mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions,
        ConstantManager.READ_PERMISSIONS_REQUEST)
  }

  private fun requestPhoto() {
    mRootPresenter.requestPhotoFromGallery()
    subscribeOnPhotoObs()
  }

  private fun subscribeOnPhotoObs() {
    mCompositeDisposable.add(
        mRootPresenter.photoObs.subscribe(
            { Flow.get(view).set(UploadScreen(albumId = albumId, photoUri = it)) },
            { EventBus.publish(ErrorEvent(it)) }))
  }

  private fun subscribeOnPermissionObs() {
    mCompositeDisposable.add(mRootPresenter.permissionResultObs.subscribe(
        { if (it == ConstantManager.READ_PERMISSIONS_REQUEST) requestPhoto() },
        { EventBus.publish(ErrorEvent(it)) }))
  }

  fun onBackPressed(): Boolean{
    if (parentScreen != null) {
      Flow.get(view).replaceTop(parentScreen, Direction.BACKWARD)
      return true
    }
    else return false
  }
}
//endregion
}