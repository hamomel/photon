package com.hamom.photon.ui.screens.auth;

import com.hamom.photon.data.network.requests.SignInReq;
import com.hamom.photon.data.network.requests.SignUpReq;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.ui.screens.base.AbstractModel;
import io.reactivex.Observable;

/**
 * Created by hamom on 04.06.17.
 */

class AuthModel extends AbstractModel {

  Observable<User> signIn(SignInReq req){
    return mDataManager.signIn(req);
  }

  Observable<User> signUp(SignUpReq req){
    return mDataManager.signUp(req);
  }
}
