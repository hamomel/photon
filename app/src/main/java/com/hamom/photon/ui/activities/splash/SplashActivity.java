package com.hamom.photon.ui.activities.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.hamom.photon.R;
import com.hamom.photon.di.components.AppComponent;
import com.hamom.photon.di.scopes.SplashScope;
import com.hamom.photon.ui.activities.root.RootActivity;
import com.hamom.photon.utils.App;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import com.wang.avi.AVLoadingIndicatorView;
import javax.inject.Inject;

public class SplashActivity extends AppCompatActivity {
  private static String TAG = ConstantManager.TAG_PREFIX + "SplashActivity: ";
  @Inject
  SplashPresenter mPresenter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);
    AVLoadingIndicatorView indicatorView = (AVLoadingIndicatorView) findViewById(R.id.loading_indicator);
    indicatorView.show();
    createComponent().inject(this);
    mPresenter.takeView(this);
  }

  @Override
  protected void onDestroy() {
    mPresenter.dropView();
    super.onDestroy();
  }

  public void startMainActivity() {
    Intent intent = new Intent(SplashActivity.this, RootActivity.class);
    startActivity(intent);
  }

  //region===================== DI ==========================
  @SplashScope
  @dagger.Component(dependencies = AppComponent.class)
  public interface Component{
    void inject(SplashActivity activity);
  }

  private Component createComponent(){
    return DaggerSplashActivity_Component.builder()
        .appComponent(App.getAppComponent())
        .build();
  }

  //endregion
}
