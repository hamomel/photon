package com.hamom.photon.ui.screens.search;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import butterknife.BindView;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.dto.SearchDto;
import com.hamom.photon.ui.activities.root.RootPresenter;
import com.hamom.photon.ui.screens.base.AbstractView;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import flow.Flow;
import java.util.ArrayList;
import java.util.List;
import mortar.MortarScope;

/**
 * Created by hamom on 12.06.17.
 */

public class SearchView extends AbstractView<SearchScreen.Presenter> {
  private static String TAG = ConstantManager.TAG_PREFIX + "SearchView: ";

  @BindView(R.id.search_view_pager)
  ViewPager searchViewPager;

  private SearchPageViewHolder mSearchPageViewHolder;
  private FilterPageViewHolder mFilterPageViewHolder;

  public SearchView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    if (!isInEditMode()) {
      initView();
    }
  }

  private void initView() {
    if (AppConfig.DEBUG) Log.d(TAG, "initView: ");

    List<View> pages = new ArrayList<>();
    pages.add(getSearchView());
    pages.add(getFiltersView());

    SearchAdapter adapter = new SearchAdapter(pages);
    searchViewPager.setAdapter(adapter);
  }

  private View getSearchView() {
    View view = LayoutInflater.from(getContext()).inflate(R.layout.page_search,
        searchViewPager, false);
    mSearchPageViewHolder = new SearchPageViewHolder(view, getSearchCallback());
    return view;
  }

  private SearchPageViewHolder.SearchCallback getSearchCallback() {
    return dto -> mPresenter.onSearchClick(dto);
  }

  private View getFiltersView() {
    View view = LayoutInflater.from(getContext()).inflate(R.layout.page_filters,
        searchViewPager, false);
    mFilterPageViewHolder = new FilterPageViewHolder(view, getFiltersCallback());
    return view;
  }

  private FilterPageViewHolder.FilterCallback getFiltersCallback() {
    return dto -> mPresenter.onFilterClick(dto);
  }

  @Override
  public boolean onBackPressed() {
    return Flow.get(getContext()).goBack();
  }

  @Override
  protected void initDagger() {
    ((SearchScreen.Component) MortarScope.getScope(getContext())
        .getService(ConstantManager.DAGGER_SERVICE_NAME)).inject(this);
  }

  public void initToolbar(RootPresenter.ToolbarBuilder builder) {
    builder.setTabs(searchViewPager).build();
  }

  public void addSuggestion(String s) {
    mSearchPageViewHolder.addSuggestion(s);
  }

  public void addTag(String s) {
    mSearchPageViewHolder.addTag(s);
  }
}
