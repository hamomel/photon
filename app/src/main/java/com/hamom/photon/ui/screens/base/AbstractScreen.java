package com.hamom.photon.ui.screens.base;

import android.os.Parcelable;
import android.util.Log;
import com.hamom.photon.flow.Screen;
import com.hamom.photon.mortar.ScreenScoper;
import com.hamom.photon.utils.ConstantManager;
import flow.ClassKey;

/**
 * Created by hamom on 26.11.16.
 */

public abstract class AbstractScreen<T> extends ClassKey implements Parcelable {
  private static String TAG = ConstantManager.TAG_PREFIX + "AbstractScreen: ";

  public String getScopeName() {
    return getClass().getName() + hashCode();
  }

  public abstract Object createScreenComponent(T parentComponent);

  public void unregisterScope() {
    ScreenScoper.destroyScreenScope(getScopeName());
  }

  public int getLayoutResId() {
    int layout = 0;
    Screen screen;
    screen = this.getClass().getAnnotation(Screen.class);
    if (screen != null) {
      throw new IllegalStateException("@Screen annotation is missing on screen " + getScopeName());
    } else {
      layout = screen.value();
    }
    return layout;
  }
}
