package com.hamom.photon.ui.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import com.google.android.flexbox.FlexboxLayout;
import com.hamom.photon.R;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hamom on 11.06.17.
 */

public class TagsView extends FlexboxLayout {
  private static String TAG = ConstantManager.TAG_PREFIX + "TagsView: ";
  private static final int CHILD_MARGIN_TOP_DP = 16;
  private static final int CHILD_MARGIN_LEFT_DP = 16;
  private static final int CHILD_PADDING_DP = 4;
  private static final String TAG_PREFIX = "#";
  private int colorSelected;
  private int colorDeSelected;
  private float screenDensity;
  private boolean selectableTags;
  private OnSelectionChangeListener onSelectionChangeListener;

  private List<String> selectedTags;
  private List<String> tags;

  public TagsView(Context context, AttributeSet attrs) {
    super(context, attrs);
    resolveAttrs(attrs);
    screenDensity = getDensity(context);
    setFlexWrap(FLEX_WRAP_WRAP);
    setAlignItems(ALIGN_ITEMS_FLEX_START);
    setAlignContent(ALIGN_CONTENT_FLEX_START);
    colorDeSelected = getContext().getResources().getColor(R.color.color_text_dark);
    colorSelected = getContext().getResources().getColor(R.color.color_accent);
    selectedTags = new ArrayList<>();
    tags = new ArrayList<>();
  }

  private void resolveAttrs(AttributeSet attrs) {
    selectableTags = attrs.getAttributeBooleanValue("http://schemas.android.com/apk/res-auto",
        "selectable", false);
  }

  private float getDensity(Context context) {
    DisplayMetrics metrics = new DisplayMetrics();
    ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
    return metrics.density;
  }

  public void setOnSelectionChangeListener(OnSelectionChangeListener listener){
    onSelectionChangeListener = listener;
  }

  public List<String > getSelectedTags(){
    return selectedTags;
  }

  public List<String> getAllTags(){
    return tags;
  }

  public void clearSelections(){
    int count = getChildCount();
    for (int i = 0; i < count; i++){
      TextView child = ((TextView) getChildAt(i));
      String text = removePrefix(child.getText().toString());
      removeFromSelected(text);
      setDeselectedColor(child);
    }
  }

  public void selectTag(String tag){
    addTag(tag);
    selectedTags.add(tag);
    setSelectedColor(getViewWithTag(tag));
  }

  private TextView getViewWithTag(String tag){
    for (int i = 0; i < getChildCount(); i++){
      TextView child = ((TextView) getChildAt(i));
      String text = removePrefix(child.getText().toString().trim());
      if (text.equals(tag))
        return child;
    }
    return null;
  }


  public void addTags(List<String> tags){
    if (AppConfig.DEBUG) Log.d(TAG, "addTags: " + tags);

    for (String tag : tags) {
      addTag(tag);
    }
  }

  public void addTag(String tag) {
    if (!tags.contains(tag)){
      tags.add(tag);
      TextView textView = getTextView();
      tag = addPrefix(tag);
      textView.setText(tag);
      addView(textView);
    }
  }

  private TextView getTextView() {
    FlexboxLayout.LayoutParams lp = new FlexboxLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
        ViewGroup.LayoutParams.WRAP_CONTENT);
    TextView tv = new TextView(getContext());
    int marginLeft = (int) (CHILD_MARGIN_LEFT_DP * screenDensity);
    int marginTop = (int) (CHILD_MARGIN_TOP_DP * screenDensity);
    lp.setMargins(marginLeft, marginTop, 0, 0);
    tv.setLayoutParams(lp);
    tv.setBackgroundResource(R.drawable.tag_background_unselected);
    int padding = (int) (CHILD_PADDING_DP * screenDensity);
    tv.setPadding(padding, padding, padding, padding);
    tv.setTextColor(colorDeSelected);
    tv.setOnClickListener(v -> selectDeselectTag(((TextView) v)));
    return tv;
  }

  private void selectDeselectTag(TextView v) {
    if (!selectableTags){
      return;
    }
    String text = removePrefix(v.getText().toString());
    if (selectedTags.contains(text)){
      removeFromSelected(text);
      setDeselectedColor(v);
    } else {
      addToSelected(text);
      setSelectedColor(v);
    }
  }

  private void addToSelected(String text) {
    selectedTags.add(text);
    onSelectionChangeListener.onSelectionChanged(selectedTags.size());
  }

  private void removeFromSelected(String text) {
    selectedTags.remove(text);
    onSelectionChangeListener.onSelectionChanged(selectedTags.size());
  }

  private void setSelectedColor(TextView v) {
    v.setBackgroundResource(R.drawable.tag_background_selected);
    v.setTextColor(colorSelected);
  }

  private void setDeselectedColor(TextView v) {
    v.setBackgroundResource(R.drawable.tag_background_unselected);
    v.setTextColor(colorDeSelected);
  }

  private String addPrefix(String tag) {
    return  TAG_PREFIX + tag;
  }

  private String removePrefix(String text) {
    return text.replace(TAG_PREFIX, "");
  }

  public interface OnSelectionChangeListener {
    // takes count of selected items
    void onSelectionChanged(int selectedCount);
  }
}
