package com.hamom.photon.ui.screens.home;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import butterknife.BindView;
import butterknife.OnClick;
import com.hamom.photon.R;
import com.hamom.photon.data.network.requests.SignInReq;
import com.hamom.photon.data.network.requests.SignUpReq;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.ui.activities.root.RootPresenter;
import com.hamom.photon.ui.custom.LoginDialog;
import com.hamom.photon.ui.custom.MenuItemHolder;
import com.hamom.photon.ui.screens.base.AbstractView;
import com.hamom.photon.ui.screens.base.OnItemClickListener;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import io.realm.RealmResults;
import mortar.MortarScope;

/**
 * Created by hamom on 01.06.17.
 */

public class HomeView extends AbstractView<HomeScreen.HomePresenter>
    implements OnItemClickListener {
  private static String TAG = ConstantManager.TAG_PREFIX + "HomeView: ";

  @BindView(R.id.home_recycler)
  RecyclerView homeRecycler;

  private HomeAdapter mAdapter;
  private final GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);

  public HomeView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void initDagger() {
    ((HomeScreen.Component) MortarScope.getScope(getContext())
        .getService(ConstantManager.DAGGER_SERVICE_NAME)).inject(this);
  }

  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (!isInEditMode()) {
      initView();
      mPresenter.onViewReady();
    }
  }

  public void initView() {
    if (mAdapter == null) mAdapter = new HomeAdapter(this);
    homeRecycler.setLayoutManager(mLayoutManager);
    homeRecycler.setAdapter(mAdapter);
  }

  //region===================== Toolbar ==========================
  public void initToolbar(RootPresenter.ToolbarBuilder builder, boolean isAutUser,
      boolean isSearchOrFilter) {
    builder.setTitle(getContext().getString(R.string.photon))
        .setShowBack(false)
        .setOverFlowButtonRes(R.drawable.ic_custom_gear_black_24dp)
        .addMenuItem(getFindItem(isSearchOrFilter))
        .addMenuItem(getEnterExitItem(isAutUser));

    if (!isAutUser) {
      builder.addMenuItem(getRegisterItem());
    }

    builder.build();
  }

  private MenuItemHolder getFindItem(boolean isSearchOrFilter) {
    @DrawableRes int iconRes;
    if (isSearchOrFilter) {
      iconRes = R.drawable.ic_custom_search_accent_24dp;
    } else {
      iconRes = R.drawable.ic_custom_search_black_24dp;
    }
    return new MenuItemHolder("Поиск", iconRes, item -> onSearchClick(),
        MenuItem.SHOW_AS_ACTION_ALWAYS);
  }

  private MenuItemHolder getEnterExitItem(boolean isAuthUser) {
    if (isAuthUser) {
      return new MenuItemHolder("Выйти", 0, item -> onLogOutClick(), MenuItem.SHOW_AS_ACTION_NEVER);
    } else {
      return new MenuItemHolder("Войти", 0, item -> onSignInClick(), MenuItem.SHOW_AS_ACTION_NEVER);
    }
  }

  private MenuItemHolder getRegisterItem() {
    return new MenuItemHolder("Зарегистрироваться", 0, item -> onRegisterClick(),
        MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private boolean onSearchClick() {
    mPresenter.onSearchClick();
    return false;
  }

  private boolean onLogOutClick() {
    mPresenter.onLogOutClick();
    return false;
  }

  private boolean onSignInClick() {
    mPresenter.onSignInClick();
    return false;
  }

  private boolean onRegisterClick() {
    mPresenter.onRegisterClick();
    return false;
  }
  //endregion

  public void addPhotoCard(PhotoCard photoCard) {
    mAdapter.addPhotoCard(photoCard);
  }

  @Override
  public void onItemClick(String itemId) {
    mPresenter.onItemClick(itemId);
  }

  @Override
  public boolean onBackPressed() {
    return false;
  }

  public void showSignInDialog() {
    new LoginDialog.Builder(getContext(), this).setRegisterMode(false)
        .setSignInCallBack(this::onSignIn)
        .build()
        .show();
  }

  private void onSignIn(SignInReq signInReq) {
    mPresenter.onSignInReq(signInReq);
  }

  public void showRegisterDialog() {
    new LoginDialog.Builder(getContext(), this).setRegisterMode(true)
        .setRegisterCallback(this::onSignUp)
        .build()
        .show();
  }

  private void onSignUp(SignUpReq signUpReq) {
    mPresenter.onSignUpReq(signUpReq);
  }

  public void clearData() {
    mAdapter.clearData();
  }

  @Override
  protected Parcelable onSaveInstanceState() {
    if (AppConfig.DEBUG) Log.d(TAG, "onSaveInstanceState: ");
    mPresenter.saveViewState(mLayoutManager.onSaveInstanceState());
     return super.onSaveInstanceState();

  }

  public void restoreLayoutManagerState(Parcelable state) {
    if (state != null) mLayoutManager.onRestoreInstanceState(state);
  }
}
