package com.hamom.photon.ui.screens.author

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import com.hamom.photon.R
import com.hamom.photon.di.DaggerComponentFactory
import com.hamom.photon.di.scopes.DaggerScope
import com.hamom.photon.flow.Screen
import com.hamom.photon.ui.activities.root.RootActivity
import com.hamom.photon.ui.screens.album.AlbumScreen
import com.hamom.photon.ui.screens.base.AbstractPresenter
import com.hamom.photon.ui.screens.base.AbstractScreen
import com.hamom.photon.utils.ConstantManager
import dagger.Provides
import flow.Flow
import mortar.MortarScope

/**
 * Created by hamom on 10.07.17.
 */
@Screen(R.layout.screen_autor)
class AuthorScreen(val userId: String) : AbstractScreen<RootActivity.Component>() {

  private var viewState: Parcelable? = null
  override fun createScreenComponent(
      parentComponent: RootActivity.Component?): Any = DaggerComponentFactory.createComponent(
      parentComponent, Component::class.java, Module(this))


  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other?.javaClass != javaClass) return false
    if (!super.equals(other)) return false

    other as AuthorScreen

    if (userId != other.userId) return false

    return true
  }

  override fun hashCode(): Int {
    var result = super.hashCode()
    result = 31 * result + userId.hashCode()
    return result
  }

  //region====================== Parcelable ============================
  override fun describeContents(): Int = 0

  override fun writeToParcel(dest: Parcel?, flags: Int) {
    dest?.writeString(userId)
  }

  companion object CREATOR : Parcelable.Creator<AuthorScreen> {
    override fun createFromParcel(parcel: Parcel): AuthorScreen {
      return AuthorScreen(parcel.readString())
    }

    override fun newArray(size: Int): Array<AuthorScreen?> {
      return Array(size, { null })
    }
  }
  //endregion

  //region====================== DI ============================
  @dagger.Module
  class Module(val screen: AuthorScreen) {
    @Provides
    @DaggerScope(AuthorScreen::class)
    fun providePresenter(): Presenter = screen.Presenter()

    @Provides
    @DaggerScope(AuthorScreen::class)
    fun provideModel(): AuthorModel = AuthorModel()
  }

  @dagger.Component(modules = arrayOf(Module::class),
      dependencies = arrayOf(RootActivity.Component::class))
  @DaggerScope(AuthorScreen::class)
  interface Component {
    fun inject(view: AuthorView)
    fun inject(presenter: Presenter)
  }
  //endregion

  //region====================== Presenter ============================
  inner class Presenter : AbstractPresenter<AuthorView, AuthorModel>() {
    override fun initDagger(scope: MortarScope) {
      scope.getService<AuthorScreen.Component>(ConstantManager.DAGGER_SERVICE_NAME).inject(this)
    }

    override fun initToolbar() {
      if (hasView()) view.initToolbar(mRootPresenter.toolbarBuilder())
    }

    override fun onLoad(savedInstanceState: Bundle?) {
      super.onLoad(savedInstanceState)
      view.restoreLayoutManagerState(viewState)
      subscribeOnUserObs()
    }

    fun saveViewState(state: Parcelable?) {
      viewState = state
    }

    private fun subscribeOnUserObs() {
      mCompositeDisposable.add(mModel.getUserObs(userId)
          .subscribe( {if (hasView()) view.initView(it) }, { rootView.showError(it) }))
    }

    fun onAlbumClick(albumId: String) {
      Flow.get(view).set(AlbumScreen(userId, albumId, this@AuthorScreen))
    }
  }
//endregion
}