package com.hamom.photon.ui.screens.detail_photo;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.hamom.photon.R;
import com.hamom.photon.data.network.requests.SignInReq;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.ui.activities.root.RootPresenter;
import com.hamom.photon.ui.custom.LoginDialog;
import com.hamom.photon.ui.custom.MenuItemHolder;
import com.hamom.photon.ui.custom.TagsView;
import com.hamom.photon.ui.custom.UserInfoView;
import com.hamom.photon.ui.screens.base.AbstractView;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import flow.Flow;
import java.util.List;
import mortar.MortarScope;

/**
 * Created by hamom on 03.06.17.
 */

public class PhotoCardDetailView extends AbstractView<PhotoCardDetailScreen.Presenter> {
  private static String TAG = ConstantManager.TAG_PREFIX + "DetailView: ";

  @BindView(R.id.detail_photo_iv)
  ImageView detailPhotoIv;
  @BindView(R.id.photo_title_iv)
  TextView photoTitleIv;
  @BindView(R.id.user_info)
  UserInfoView userInfo;
  @BindView(R.id.tags_view)
  TagsView tagsView;

  @OnClick(R.id.user_info)
  void onUserInfoClick() {
    mPresenter.onUserInfoClick();
  }

  public PhotoCardDetailView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void initDagger() {
    ((PhotoCardDetailScreen.Component) MortarScope.getScope(getContext())
        .getService(ConstantManager.DAGGER_SERVICE_NAME)).inject(this);
  }

  //region===================== Toolbar ==========================
  public void initToolbar(RootPresenter.ToolbarBuilder builder, boolean isFavorite,
      boolean isOwner) {
    builder.setTitle(getContext().getString(R.string.photocard))
        .setShowBack(true);
    if (isFavorite) {
      builder.addMenuItem(getRemoveFavoriteItem());
    } else {
      builder.addMenuItem(getAdToFavoriteItem());
    }

    if (isOwner) {
      builder.addMenuItem(getDeletePhotoItem());
    }
    builder.addMenuItem(getShareItem());
    builder.addMenuItem(getDownloadItem());

    builder.build();
  }

  private MenuItemHolder getShareItem() {
    return new MenuItemHolder(getContext().getString(R.string.share), R.drawable.ic_share_black, item -> onShareClick(),
        MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private boolean onShareClick() {
    mPresenter.onShareClick(detailPhotoIv.getDrawable());
    return false;
  }

  private MenuItemHolder getDownloadItem() {
    return new MenuItemHolder(getContext().getString(R.string.download), R.drawable.ic_download,
        item -> onDownloadClick(), MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private boolean onDownloadClick() {
    if (AppConfig.DEBUG) Log.d(TAG, "onDownloadClick: " + detailPhotoIv.getDrawable());

    mPresenter.onDownloadClick(detailPhotoIv.getDrawable());
    return false;
  }

  private MenuItemHolder getRemoveFavoriteItem() {
    return new MenuItemHolder(getContext().getString(R.string.remove_from_favorite), R.drawable.ic_favorites_black,
        item -> onRemoveFavoriteClick(), MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private boolean onRemoveFavoriteClick() {
    mPresenter.onRemoveFavoriteClick();
    return false;
  }

  private MenuItemHolder getAdToFavoriteItem() {
    return new MenuItemHolder(getContext().getString(R.string.add_favorite), R.drawable.ic_favorites_black,
        item -> onAddFavoriteClick(), MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private boolean onAddFavoriteClick() {
    if (AppConfig.DEBUG) Log.d(TAG, "onAddFavoriteClick: ");

    mPresenter.onAddFavoriteClick();
    return false;
  }

  private MenuItemHolder getDeletePhotoItem() {
    return new MenuItemHolder(getContext().getString(R.string.delete_photocard), R.drawable.ic_delete,
        item -> onDeleteClick(), MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private boolean onDeleteClick() {
    mPresenter.onDeleteClick();
    return false;
  }
  //endregion

  public void initView(PhotoCard photoCard) {
    if (AppConfig.DEBUG) Log.d(TAG, "initView: " + photoCard);

    Glide.with(getContext()).load(photoCard.getPhoto()).into(detailPhotoIv);
    photoTitleIv.setText(photoCard.getTitle());
    setTags(photoCard.getTags());
    detailPhotoIv.setOnTouchListener(new AnimatedOnTouchListener(detailPhotoIv, this));

  }

  private void setTags(List<String> tags) {
    tagsView.addTags(tags);
  }

  public void setUser(User user) {
    userInfo.setUser(user);
  }

  @Override
  public boolean onBackPressed() {
    return Flow.get(getContext()).goBack();
  }

  public void showLoginDialog() {
    new LoginDialog.Builder(getContext(), this).setRegisterMode(false)
        .setSignInCallBack(this::onSignIn)
        .build()
        .show();
  }

  private void onSignIn(SignInReq signInReq) {
    mPresenter.onLogin(signInReq);
  }

  public void sendShareIntent(Uri uri) {
    // TODO: 15.06.17 fix mms crush on 19 & 21 api
    Intent intent = new Intent(Intent.ACTION_SEND);
    intent.putExtra(Intent.EXTRA_STREAM, uri);
    intent.setType("image/*");
    PackageManager packageManager = getContext().getPackageManager();
    List activities =
        packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
    if (activities.size() > 0) {
      getContext().startActivity(
          Intent.createChooser(intent, getContext().getString(R.string.share_with)));
    } else {
      mPresenter.showNoAppFitsMessage(getContext().getString(R.string.no_apps_fit));
    }
  }
}
