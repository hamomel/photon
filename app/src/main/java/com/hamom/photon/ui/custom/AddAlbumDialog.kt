package com.hamom.photon.ui.custom

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.hamom.photon.R
import com.hamom.photon.data.network.requests.AddAlbumReq
import com.hamom.photon.data.storage.realm.Album

/**
 * Created by hamom on 30.06.17.
 */
class AddAlbumDialog(context: Context, parentView: ViewGroup,
    val callback: AddAlbumCallback) : AlertDialog(context) {

  @BindView(R.id.album_name_et) lateinit var albumNameEt: EditText
  @BindView(R.id.album_desc_et) lateinit var albumDescEt: EditText
  @BindView(R.id.title_tv) lateinit var titleTv: TextView

  init {
    window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_add_album, parentView,
        false)
    setView(view)
    ButterKnife.bind(this, view)
    albumNameEt.setHint(R.string.name)
    albumNameEt.clearFocus()
    albumDescEt.setHint(R.string.description)
    albumDescEt.clearFocus()
  }

  @OnClick(R.id.ok_btn, R.id.cancel_btn)
  fun onButtonClick(btn: Button): Unit {
    when (btn.id) {
      R.id.cancel_btn -> dismiss()
      R.id.ok_btn -> sendCallback()
    }
  }

  fun setTitle(title: String) {
    titleTv.text = title
  }

  fun setAlbum(album: Album) {
    albumNameEt.setText(album.title)
    albumDescEt.setText(album.description)
  }

  private fun sendCallback() {
    if (!albumNameEt.text.isEmpty() && !albumDescEt.text.isEmpty()){
      callback.onFinish(AddAlbumReq(albumDescEt.text.toString(), albumNameEt.text.toString()))
      dismiss()
    }
  }

  interface AddAlbumCallback {
    fun onFinish(req: AddAlbumReq)
  }

}

