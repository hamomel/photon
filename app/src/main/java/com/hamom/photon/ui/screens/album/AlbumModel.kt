package com.hamom.photon.ui.screens.album

import android.util.Log
import com.hamom.photon.data.network.requests.AddAlbumReq
import com.hamom.photon.data.storage.realm.Album
import com.hamom.photon.jobs.jobs.DeleteAlbumJob
import com.hamom.photon.jobs.jobs.EditAlbumJob
import com.hamom.photon.ui.screens.base.AbstractModel
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager
import io.reactivex.Observable

/**
 * Created by hamom on 06.06.17.
 */

internal class AlbumModel : AbstractModel() {
val TAG = ConstantManager.TAG_PREFIX + "AlbumModel: "

  fun getAlbumObs(userId: String, albumId: String): Observable<Album> {
    val album: Album? = mRealmManager.getAlbumById(albumId)

    if (album != null) {
      return Observable.mergeDelayError(createAlbumObs(album), getDownloadAlbumObs(userId, albumId))
          .flatMap { if (it is Album) Observable.just(it) else Observable.empty() }
          .doOnComplete { if (AppConfig.DEBUG) Log.d(TAG, "getAlbumObs onCompleted: ") }
    } else {
      if (AppConfig.DEBUG) Log.d(TAG, "getAlbumObs album = " + album)
      return getDownloadAlbumObs(userId, albumId)
          .flatMap { createAlbumObs(mRealmManager.getAlbumById(albumId)) }
    }
  }

  private fun getDownloadAlbumObs(userId: String,
      albumId: String) = mDataManager.downloadAlbumByIdObs(userId, albumId)

  private fun createAlbumObs(album: Album): Observable<Album> {
    return Observable.create { e ->
      if (!e.isDisposed) e.onNext(album); album.addChangeListener { a: Album, _ -> e.onNext(a) }
    }
  }

  fun getCurrentUserId(): String = mDataManager.currentUserId

  fun deleteAlbum(albumId: String) {
    mJobManager.addJobInBackground(DeleteAlbumJob(albumId))
  }

  fun editAlbum(req: AddAlbumReq, albumId: String) {
    mJobManager.addJobInBackground(EditAlbumJob(req, albumId))
  }
}
