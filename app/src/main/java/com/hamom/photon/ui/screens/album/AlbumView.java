package com.hamom.photon.ui.screens.album;

import android.content.Context;
import android.os.Parcelable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MenuItem;
import butterknife.BindView;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.realm.Album;
import com.hamom.photon.ui.activities.root.RootPresenter;
import com.hamom.photon.ui.custom.AddAlbumDialog;
import com.hamom.photon.ui.custom.MenuItemHolder;
import com.hamom.photon.ui.screens.base.AbstractView;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import flow.Flow;
import mortar.MortarScope;

/**
 * Created by hamom on 06.06.17.
 */

class AlbumView extends AbstractView<AlbumScreen.Presenter>{
  private static String TAG = ConstantManager.TAG_PREFIX + "AlbumView: ";
  @BindView(R.id.album_recycler)
  RecyclerView albumRecycler;

  private final GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 3);
  private boolean isViewMode;
  private Album mAlbum;

  public AlbumView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  public boolean onBackPressed() {
    return Flow.get(getContext()).goBack();
  }

  @Override
  protected void initDagger() {
    ((AlbumScreen.Component) MortarScope.getScope(getContext())
        .getService(ConstantManager.DAGGER_SERVICE_NAME)).inject(this);
  }

  void setViewMode(boolean isViewMode){
    this.isViewMode = isViewMode;
  }

  //region===================== Toolbar ==========================
  public void initToolbar(RootPresenter.ToolbarBuilder builder) {
    if (isViewMode){
      builder.setTitle(getContext().getString(R.string.album))
          .setShowBack(true)
          .build();
    } else {
      builder.setTitle(getContext().getString(R.string.album))
          .setShowBack(true)
          .addMenuItem(getEditItem())
          .addMenuItem(getDeleteItem())
          .addMenuItem(getAddItem())
          .build();
    }
  }

  private MenuItemHolder getAddItem() {
    return new MenuItemHolder(getContext().getString(R.string.add_photocard), R.drawable.ic_new_album, item -> onAddClick(),
        MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private MenuItemHolder getDeleteItem() {
    return new MenuItemHolder(getContext().getString(R.string.delete_album), R.drawable.ic_delete, item -> onDeleteClick(),
        MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private MenuItemHolder getEditItem() {
    return new MenuItemHolder(getContext().getString(R.string.edit_album), R.drawable.ic_edit, item -> onEditClick(),
        MenuItem.SHOW_AS_ACTION_NEVER);
  }

  private boolean onDeleteClick() {
    mPresenter.onDeleteClick();
    return false;
  }

  private boolean onAddClick() {
    mPresenter.onAddClick();
    return false;
  }

  private boolean onEditClick() {
    AddAlbumDialog dialog = new AddAlbumDialog(getContext(), this, req -> mPresenter.onEditClick(req));
    dialog.setTitle(getContext().getString(R.string.edit_album));
    dialog.setAlbum(mAlbum);
    dialog.show();
    return false;
  }
  //endregion

  public void initView(Album album) {
    mAlbum = album;
    if (AppConfig.DEBUG) Log.d(TAG, "initView: " + album);
    albumRecycler.setAdapter(new AlbumAdapter(album, itemId -> mPresenter.onItemClick(itemId)));

    mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
      @Override
      public int getSpanSize(int position) {
        return position == 0 ? 3 : 1;
      }
    });

    albumRecycler.setLayoutManager(mLayoutManager);
  }

  @Override
  protected Parcelable onSaveInstanceState() {
    mPresenter.saveViewState(mLayoutManager.onSaveInstanceState());
    return super.onSaveInstanceState();
  }

  void restoreLayoutManagerState(Parcelable state){
    if (state != null) mLayoutManager.onRestoreInstanceState(state);
  }
}
