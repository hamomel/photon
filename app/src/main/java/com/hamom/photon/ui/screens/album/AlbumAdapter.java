package com.hamom.photon.ui.screens.album;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.realm.Album;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.ui.screens.base.OnItemClickListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hamom on 07.06.17.
 */

class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {
  private static final int TYPE_HEADER = 0;
  private static final int TYPE_ITEM = 1;

  private OnItemClickListener mItemClickListener;
  private Context mContext;
  private Album mAlbum;
  private List<PhotoCard> mPhotoCards;

  AlbumAdapter(Album album, OnItemClickListener listener) {
    mAlbum = album;
    mItemClickListener = listener;
    mPhotoCards = getActivePhotocards(album);
  }

  private List<PhotoCard> getActivePhotocards(Album album) {
    List<PhotoCard> result = new ArrayList<>();
    for (PhotoCard photoCard : album.getPhotocards()) {
      if (photoCard.isActive()) result.add(photoCard);
    }
    return result;
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
    mContext= recyclerView.getContext();
  }

  @Override
  public int getItemViewType(int position) {
    if (position == 0){
      return TYPE_HEADER;
    } else {
      return TYPE_ITEM;
    }
  }

  @Override
  public AlbumAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view;
    if (viewType == TYPE_HEADER){
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_album, parent, false);
    } else {
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_simple, parent, false);
    }
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(AlbumAdapter.ViewHolder holder, int position) {
    if (position == 0){
      holder.mHeaderView.setAlbum(mAlbum);
    } else {
      position--;
      PhotoCard photoCard = mPhotoCards.get(position);
      String photoUrl = photoCard.getPhoto();
      Glide.with(mContext).load(photoUrl).centerCrop().into(holder.simpleItemPhotoIv);
      holder.simpleItemPhotoIv.setOnClickListener(v -> mItemClickListener.onItemClick(photoCard.getId()));
    }
  }

  @Override
  public int getItemCount() {
    return mPhotoCards.size() + 1;
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    AlbumHeaderView mHeaderView;
    @BindView(R.id.simple_item_photo_iv)
    ImageView simpleItemPhotoIv;

    ViewHolder(View itemView) {
      super(itemView);
      if (itemView instanceof AlbumHeaderView){
        mHeaderView = ((AlbumHeaderView) itemView);
      } else {
        ButterKnife.bind(this, itemView);
      }
    }
  }
}
