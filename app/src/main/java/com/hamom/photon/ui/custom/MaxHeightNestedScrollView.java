package com.hamom.photon.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import com.hamom.photon.R;

/**
 * Created by hamom on 21.06.17.
 */

public class MaxHeightNestedScrollView extends NestedScrollView {
  private int maxHeight;

  public MaxHeightNestedScrollView(Context context, AttributeSet attrs) {
    super(context, attrs);
    if (!isInEditMode()) resolveAttrs(attrs);
  }

  private void resolveAttrs(AttributeSet attrs) {
    if (attrs == null) return;
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MaxHeightNestedScrollView, 0, 0);
    try {
      maxHeight = a.getDimensionPixelSize(R.styleable.MaxHeightNestedScrollView_maxHeight, 0);
    } finally {
      a.recycle();
    }
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    if (maxHeight > 0){
      heightMeasureSpec = MeasureSpec.makeMeasureSpec(maxHeight, MeasureSpec.AT_MOST);
    }
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  }
}
