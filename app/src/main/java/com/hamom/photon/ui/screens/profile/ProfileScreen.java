package com.hamom.photon.ui.screens.profile;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.hamom.photon.R;
import com.hamom.photon.data.network.requests.AddAlbumReq;
import com.hamom.photon.data.storage.dto.EditUserDto;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.di.DaggerComponentFactory;
import com.hamom.photon.di.scopes.DaggerScope;
import com.hamom.photon.flow.Screen;
import com.hamom.photon.ui.activities.root.RootActivity;
import com.hamom.photon.ui.custom.UserEditDialog;
import com.hamom.photon.ui.screens.album.AlbumScreen;
import com.hamom.photon.ui.screens.auth.AuthScreen;
import com.hamom.photon.ui.screens.base.AbstractPresenter;
import com.hamom.photon.ui.screens.base.AbstractScreen;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

/**
 * Created by hamom on 05.06.17.
 */
@Screen(R.layout.screen_profile)
public class ProfileScreen extends AbstractScreen<RootActivity.Component> {
  private static String TAG = ConstantManager.TAG_PREFIX + "ProfileScreen: ";

  private Parcelable mViewState;

  public ProfileScreen() {
  }

  public ProfileScreen(Parcel source) {
    mViewState = source.readParcelable(Parcelable.class.getClassLoader());
  }

  @Override
  public Object createScreenComponent(RootActivity.Component parentComponent) {
    return DaggerComponentFactory.createComponent(parentComponent, Component.class, new Module());
  }

  @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
  @DaggerScope(ProfileScreen.class)
  public interface Component {

    void inject(Presenter presenter);

    void inject(ProfileView view);
  }

  @dagger.Module
  class Module {
    @Provides
    @DaggerScope(ProfileScreen.class)
    Presenter provideProfilePresenter() {
      return new Presenter();
    }

    @Provides
    @DaggerScope(ProfileScreen.class)
    ProfileModel provideProfileModel() {
      return new ProfileModel();
    }
  }

  //region===================== Parcelable ==========================
  public static final Creator<ProfileScreen> CREATOR = new Creator<ProfileScreen>() {
    @Override
    public ProfileScreen createFromParcel(Parcel source) {
      return new ProfileScreen(source);
    }

    @Override
    public ProfileScreen[] newArray(int size) {
      return new ProfileScreen[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(mViewState, flags);
  }
  //endregion

  //region===================== Presenter ==========================

  public class Presenter extends AbstractPresenter<ProfileView, ProfileModel> {
    private String mUserId;
    private User mUser;

    @Override
    protected void initDagger(MortarScope scope) {
      scope.<ProfileScreen.Component>getService(ConstantManager.DAGGER_SERVICE_NAME).inject(this);
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
      checkUserAuth();
      mUserId = mModel.getCurrentUserId();
      super.onLoad(savedInstanceState);
      getView().restoreLayoutManagerState(mViewState);
      subscribeOnUser();
    }

    void saveViewState(Parcelable state){
      mViewState = state;
    }

    private void checkUserAuth() {
      if (!mModel.isAuthUser()) Flow.get(getView()).set(new AuthScreen(ProfileScreen.this));
    }

    @Override
    protected void initToolbar() {
      if (hasView()) {
        getView().initToolbar(mRootPresenter.toolbarBuilder());
      }
    }

    public User getUser() {
      return mUser;
    }

    public void editUser(EditUserDto dto) {
      mModel.updateUser(dto);
    }

    public void getNewAvatar(UserEditDialog.AvatarCallback callback) {

      if (checkReadPermissions()){
        mRootPresenter.requestPhotoFromGallery();
        callback.onPhotoRequest(mRootPresenter.getPhotoObs());
      } else {
        subscribeOnPermissionObs(callback);
      }
    }

    private void subscribeOnPermissionObs(UserEditDialog.AvatarCallback callback) {
      mCompositeDisposable.add(mRootPresenter.getPermissionResultObs()
          .subscribe(integer -> {
            if (integer == ConstantManager.READ_PERMISSIONS_REQUEST){
              mRootPresenter.requestPhotoFromGallery();
              callback.onPhotoRequest(mRootPresenter.getPhotoObs());
            }
          }));
    }

    private boolean checkReadPermissions() {
      String[] permissions = new String[] { READ_EXTERNAL_STORAGE };
      return mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions,
          ConstantManager.READ_PERMISSIONS_REQUEST);
    }

    void onAddAlbumClick(AddAlbumReq req) {
      mModel.createAlbum(req);
    }

    void onLogOutClick() {
      mModel.signOut();
      checkUserAuth();
    }

    private void subscribeOnUser() {
      mCompositeDisposable.add(mModel.getUserObs(mUserId)
          .subscribe(user -> {
        if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnUser: 1032 " + user);

        mUser = user;
        if (hasView()) {
          getView().initView(user);
        }
      }, throwable -> getRootView().showError(throwable)));
    }

    void onAlbumClick(String albumId) {
      Flow.get(getView().getContext()).set(new AlbumScreen(mUserId, albumId, ProfileScreen.this));
    }
  }
  //endregion
}
