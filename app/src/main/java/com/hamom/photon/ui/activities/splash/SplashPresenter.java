package com.hamom.photon.ui.activities.splash;

import android.util.Log;
import com.hamom.photon.di.scopes.SplashScope;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

/**
 * Created by hamom on 29.05.17.
 */

@SplashScope
class SplashPresenter {
  private static String TAG = ConstantManager.TAG_PREFIX + "SplashPres: ";
  private SplashActivity mView;
  private SplashModel mModel;
  private CompositeDisposable mCompositeDisposable;

  @Inject
  SplashPresenter(SplashModel model) {
    mCompositeDisposable = new CompositeDisposable();
    mModel = model;
  }

  void takeView(SplashActivity view) {
    mView = view;
    subscribeOnUserObs();
    subscribeOnDownloadObs();
  }

  void dropView() {
    mView = null;
  }

  private void subscribeOnDownloadObs() {
    mCompositeDisposable.add(mModel.getEndDownloadObs()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(aBoolean -> startMainActivity(),
            throwable -> {
              throwable.printStackTrace();
              startMainActivity();
              },
            this::startMainActivity));
  }

  private void subscribeOnUserObs(){
    mCompositeDisposable.add(mModel.getUserObs().subscribe(user -> {}, Throwable::printStackTrace));
  }

  private void startMainActivity() {
    mCompositeDisposable.dispose();
    if (mView != null) mView.startMainActivity();
  }
}
