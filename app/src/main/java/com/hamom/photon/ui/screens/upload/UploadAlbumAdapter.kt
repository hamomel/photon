package com.hamom.photon.ui.screens.upload

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.hamom.photon.R
import com.hamom.photon.data.storage.realm.Album
import com.hamom.photon.ui.screens.base.OnItemClickListener
import com.hamom.photon.utils.App
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager

const val TYPE_ITEM_ALBUM: Int = 1
const val TYPE_ITEM_NO_ALBUM: Int = 2

/**
 * Created by hamom on 07.07.17.
 */
class UploadAlbumAdapter(
    val onItemClickListener: OnItemClickListener,
    val addAlbumClickListener: AddAlbumClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
  val TAG = ConstantManager.TAG_PREFIX + "UploadAlbumA: "
  val albums: ArrayList<Album> = ArrayList()
  var selected: String = ""

  init {
    setHasStableIds(true)
    if (AppConfig.DEBUG) Log.d(TAG, "init: ")
  }

  fun addAlbum(album: Album) {
    if (!albums.contains(album)) {
      albums.add(album)
      if (albums.size == 1) selectItem(album.id)
      else selectItem("")
      notifyDataSetChanged()
    }
  }

  fun selectItem(id: String) {
    selected = id
    onItemClickListener.onItemClick(selected)
  }

  override fun getItemId(position: Int): Long {
    return if (!albums.isEmpty()) albums[position].id.hashCode().toLong() else 0
  }

  override fun getItemViewType(position: Int): Int {
    if (AppConfig.DEBUG) Log.d(TAG, "getItemViewType: " + albums.isEmpty())
    return if (albums.isEmpty()) TYPE_ITEM_NO_ALBUM else TYPE_ITEM_ALBUM
  }

  override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
    if (AppConfig.DEBUG) Log.d(TAG, "onCreateViewHolder: " + viewType)
    if (viewType == TYPE_ITEM_ALBUM) {
      val view: View = LayoutInflater.from(parent?.context).inflate(R.layout.item_album, parent,
          false)
      return AlbumViewHolder(view)
    } else if (viewType == TYPE_ITEM_NO_ALBUM) {
      val view: View = LayoutInflater.from(parent?.context).inflate(R.layout.item_no_albums_upload,
          parent, false)
      return EmptyViewHolder(view)
    }
    throw RuntimeException("there is no type matches $viewType ")
  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
    if (holder is AlbumViewHolder) {
      val album: Album = albums[position]
      if (album.id == selected) holder.selectionLayout.visibility = View.VISIBLE
      else holder.selectionLayout.visibility = View.GONE

      holder.albumTitleTv.text = album.title
      holder.cardsCountTv.text = album.photocards.size.toString()
      holder.itemFavoriteTv.text = album.favorits.toString()
      holder.itemViewsTv.text = album.views.toString()
      holder.itemPhotoIv.setOnClickListener {
        selectItem(album.id); notifyDataSetChanged()
      }

      if (!album.photocards.isEmpty()) Glide.with(App.getAppContext())
          .load(album.photocards[0].photo)
          .into(holder.itemPhotoIv)

    } else if (holder is EmptyViewHolder) {
      holder.addAlbumBtn.setOnClickListener { addAlbumClickListener.onAddAlbumClick() }
    }
  }

  override fun getItemCount(): Int = if (albums.isEmpty()) 1 else albums.size

  class AlbumViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @BindView(R.id.selection_layout) lateinit var selectionLayout: FrameLayout
    @BindView(R.id.album_title_tv) lateinit var albumTitleTv: TextView
    @BindView(R.id.cards_count_tv) lateinit var cardsCountTv: TextView
    @BindView(R.id.item_photo_iv) lateinit var itemPhotoIv: ImageView
    @BindView(R.id.item_favorite_tv) lateinit var itemFavoriteTv: TextView
    @BindView(R.id.item_views_tv) lateinit var itemViewsTv: TextView
    @BindView(R.id.cards_tv) lateinit var cardsTv: TextView

    init {
      ButterKnife.bind(this, itemView)
      cardsTv.visibility = View.VISIBLE
    }
  }

  class EmptyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @BindView(R.id.add_album_btn) lateinit var addAlbumBtn: Button

    init {
      ButterKnife.bind(this, itemView)
    }
  }

  interface AddAlbumClickListener {
    fun onAddAlbumClick()
  }
}