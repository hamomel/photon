package com.hamom.photon.ui.screens.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.ui.screens.base.OnItemClickListener;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hamom on 01.06.17.
 */

class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
  private static String TAG = ConstantManager.TAG_PREFIX + "HomeAdapter: ";
  private List<PhotoCard> mPhotoCards = new ArrayList();
  private Context mContext;
  private OnItemClickListener mItemClickListener;

  HomeAdapter(OnItemClickListener itemClickListener) {
    mItemClickListener = itemClickListener;
  }

  void addPhotoCard(PhotoCard photoCard) {
    if (!mPhotoCards.contains(photoCard)){
      mPhotoCards.add(photoCard);
      notifyItemInserted(mPhotoCards.indexOf(photoCard));
    }
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
    mContext = recyclerView.getContext();
  }

  @Override
  public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.item_photo, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(HomeAdapter.ViewHolder holder, int position) {
    PhotoCard photoCard = mPhotoCards.get(position);
    holder.favoriteHomeTv.setText(String.valueOf(photoCard.getFavorits()));
    holder.viewsHomeIv.setText(String.valueOf(photoCard.getViews()));
    Glide.with(mContext).load(photoCard.getPhoto())
        .into(holder.homePhotoIv);

    holder.homePhotoIv.setOnClickListener(v -> mItemClickListener.onItemClick(photoCard.getId()));
  }

  @Override
  public int getItemCount() {
    return mPhotoCards.size();
  }

  void clearData() {
    mPhotoCards.clear();
    notifyDataSetChanged();
  }

  class ViewHolder extends RecyclerView.ViewHolder{
    @BindView(R.id.item_photo_iv)
    ImageView homePhotoIv;
    @BindView(R.id.item_views_tv)
    TextView viewsHomeIv;
    @BindView(R.id.item_favorite_tv)
    TextView favoriteHomeTv;

    ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }

}
