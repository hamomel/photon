package com.hamom.photon.ui.screens.home;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import com.hamom.photon.R;
import com.hamom.photon.data.network.requests.SignInReq;
import com.hamom.photon.data.network.requests.SignUpReq;
import com.hamom.photon.data.storage.dto.FiltersDto;
import com.hamom.photon.data.storage.dto.SearchDto;
import com.hamom.photon.data.storage.realm.Filters;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.di.DaggerComponentFactory;
import com.hamom.photon.di.scopes.DaggerScope;
import com.hamom.photon.eventBus.EventBus;
import com.hamom.photon.eventBus.events.ToastEvent;
import com.hamom.photon.ui.screens.base.AbstractScreen;
import com.hamom.photon.flow.Screen;
import com.hamom.photon.ui.activities.root.RootActivity;
import com.hamom.photon.ui.activities.root.RootPresenter;
import com.hamom.photon.ui.screens.base.AbstractPresenter;
import com.hamom.photon.ui.screens.detail_photo.PhotoCardDetailScreen;
import com.hamom.photon.ui.screens.search.SearchScreen;
import com.hamom.photon.utils.App;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import dagger.Provides;
import flow.Flow;
import io.reactivex.Observable;
import io.realm.RealmResults;
import java.util.List;
import mortar.MortarScope;

/**
 * Created by hamom on 01.06.17.
 */
@Screen(R.layout.screen_home)
public class HomeScreen extends AbstractScreen<RootActivity.Component> {
  private static String TAG = ConstantManager.TAG_PREFIX + "HomeScreen: ";
  private static FiltersDto mFilters;
  private static SearchDto mSearch;
  private static Parcelable mViewState;

  public HomeScreen() {
    mFilters = new FiltersDto();
    mSearch = new SearchDto();
  }

  public HomeScreen(FiltersDto filters) {
    mFilters = filters;
    mSearch = new SearchDto();
    if (AppConfig.DEBUG) Log.d(TAG, "HomeScreen: " + filters + " " + mFilters + " " + this.hashCode());

  }

  public HomeScreen(SearchDto search) {
    mSearch = search;
    mFilters = new FiltersDto();
    if (AppConfig.DEBUG) Log.d(TAG, "HomeScreen: " + search + " " + mSearch + " " + this.hashCode());
  }

  public HomeScreen(Parcel source) {
    mViewState = source.readParcelable(Parcelable.class.getClassLoader());
  }

  @Override
  public Object createScreenComponent(RootActivity.Component parentComponent) {
    return DaggerComponentFactory.createComponent(parentComponent, Component.class, new Module());
  }

  public static FiltersDto getFilters() {
    return mFilters;
  }

  public static SearchDto getSearch(){
    return mSearch;
  }

  @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
  @DaggerScope(HomeScreen.class)
  public interface Component {
    void inject(HomePresenter homePresenter);

    void inject(HomeView homeView);

    RootPresenter getRootPresenter();
  }

  @dagger.Module
  public class Module {
    @Provides
    @DaggerScope(HomeScreen.class)
    HomeModel provideHomeModel() {
      return new HomeModel();
    }

    @Provides
    HomePresenter provideHomePresenter() {
      return new HomePresenter();
    }
  }

  //region===================== Parcelable ==========================
  public static final Creator<HomeScreen> CREATOR = new Creator<HomeScreen>() {
    @Override
    public HomeScreen createFromParcel(Parcel source) {
      return new HomeScreen(source);
    }

    @Override
    public HomeScreen[] newArray(int size) {
      return new HomeScreen[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(mViewState, flags);
  }
  //endregion

  //region===================== Presenter ==========================
  public class HomePresenter extends AbstractPresenter<HomeView, HomeModel> {
    private String TAG = ConstantManager.TAG_PREFIX + "HomePresenter: ";
    private RealmResults<PhotoCard> mPhotoCards;
    private int mPage = 1;

    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(ConstantManager.DAGGER_SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initToolbar() {
     if (hasView()){
       getView().initToolbar(mRootPresenter.toolbarBuilder(), isAuthUser(), isSearchOrFilter());
     }
    }

    private boolean isSearchOrFilter() {
      return !mSearch.isEmpty() || !mFilters.isEmpty();
    }

    private boolean isAuthUser() {
      String userId = mModel.getUserId();
      return !TextUtils.isEmpty(userId);
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
      super.onEnterScope(scope);
      mPhotoCards = mModel.getAllPhotoCard();
      mFilters = mModel.getCurrentFilters();
      mSearch = mModel.getCurrentSearch();
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
      super.onLoad(savedInstanceState);
      getView().restoreLayoutManagerState(mViewState);
      showFilterSnackBar();
    }

    void saveViewState(Parcelable state) {
      mViewState = state;
    }

    public void onViewReady() {
      subscribeOnPhotoObs();
    }

    private void showFilterSnackBar() {
      if (!mFilters.isEmpty()){
        getRootView().showSnackBar(App.getAppContext().getString(R.string.filters_applied),
            App.getAppContext().getString(R.string.clear_filters), 0, v -> resetFilters(), 0);
      }

      if (!mSearch.isEmpty()){
        getRootView().showSnackBar(App.getAppContext().getString(R.string.filters_applied),
            App.getAppContext().getString(R.string.clear_filters), R.color.color_text_dark,
            v -> resetFilters(), R.color.color_accent);
      }
    }

    public void resetFilters() {
      mFilters = new FiltersDto();
      mSearch = new SearchDto();
      mModel.saveCurrentFilters(mFilters);
      if (hasView()){
        getView().clearData();
      }
      initToolbar();
      subscribeOnPhotoObs();
    }

    void onSearchClick() {
      Flow.get(getView().getContext()).set(new SearchScreen());
    }

    void onLogOutClick() {
      if (AppConfig.DEBUG) Log.d(TAG, "onLogOutClick: ");

      mModel.logOutUser();
      initToolbar();
    }

    void onSignInClick() {
      getView().showSignInDialog();
    }

    void onRegisterClick() {
      getView().showRegisterDialog();
    }


    private void subscribeOnPhotoObs() {
      if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnPhotoObs: ");

      Observable<PhotoCard> photoCardObservable = Observable.create(e -> {
        if (!e.isDisposed()){
          for (PhotoCard photoCard : mPhotoCards) {
            e.onNext(photoCard);
          }
          mPhotoCards.addChangeListener(photoCards -> {
            for (PhotoCard photoCard : photoCards) {
              e.onNext(photoCard);
            }
          });
        }
      });

      mCompositeDisposable.add(photoCardObservable
          .filter(PhotoCard::isActive)
          .subscribe(photoCard -> {
        if (hasView()) {
          filterAndAdToView(photoCard);
        }
      }, throwable -> getRootView().showError(throwable)));
    }

    private void filterAndAdToView(PhotoCard photoCard) {

      boolean isSuitablePhoto = true;

     if (!mFilters.isEmpty()){
       isSuitablePhoto = applyFilters(photoCard);
     }

     if (!mSearch.isEmpty()){
       isSuitablePhoto = applySearch(photoCard);
     }
     if (isSuitablePhoto){
       getView().addPhotoCard(photoCard);
     }
    }

    private boolean applyFilters(PhotoCard photoCard) {
      if (AppConfig.DEBUG) Log.d(TAG, "applyFilters: " + mFilters);

      Filters filters = photoCard.getFilters();

      if ((!TextUtils.isEmpty(mFilters.getDish()) && !mFilters.getDish().equals(filters.getDish()))
      || (!TextUtils.isEmpty(mFilters.getDecor()) && !mFilters.getDecor().equals(filters.getDecor()))
      || (!TextUtils.isEmpty(mFilters.getLight()) && !mFilters.getLight().equals(filters.getLight()))
      || (!TextUtils.isEmpty(mFilters.getLightDirection()) && !mFilters.getLightDirection().equals(filters.getLightDirection()))
      || (!TextUtils.isEmpty(mFilters.getLightSource()) && !mFilters.getLightSource().equals(filters.getLightSource()))
      || (!TextUtils.isEmpty(mFilters.getTemperature()) && !mFilters.getTemperature().equals(filters.getTemperature()))){
        return false;
      }

      boolean result = true;
      if (!mFilters.getNuances().isEmpty()){
        for (String s : mFilters.getNuances()) {
          if (filters.getNuances().contains(s)){
            result = true;
            break;
          } else {
            result = false;
          }
        }
      }
      return result;
    }

    private boolean applySearch(PhotoCard photoCard) {
      if (AppConfig.DEBUG) Log.d(TAG, "applySearch: " + mSearch);

      String searchPhrase = mSearch.getSearchPhrase().toLowerCase().trim();
      List<String> tags = mSearch.getTags();
      if (!tags.isEmpty()){
        for (String tag : tags) {
          if (photoCard.getTags().contains(tag)
              || tag.trim().equals(searchPhrase)){
            if (AppConfig.DEBUG) Log.d(TAG, "applySearch: " + photoCard.getTags());

            return true;
          }
        }
      }

      return !TextUtils.isEmpty(searchPhrase)
          && (photoCard.getTitle().toLowerCase().contains(searchPhrase));
    }

    void onItemClick(String itemId) {
      if (hasView()) Flow.get(getView()).set(new PhotoCardDetailScreen(itemId, HomeScreen.this));
    }

    void onSignInReq(SignInReq signInReq) {
      mCompositeDisposable.add(mModel.signInUser(signInReq).subscribe(user -> {
        if (user != null){
          initToolbar();
          EventBus.INSTANCE.publish(new ToastEvent(App.getAppContext().getString(R.string.successful_login) + user.getName()));
        }
      }, throwable -> getRootView().showError(throwable)));
    }

    void onSignUpReq(SignUpReq signUpReq) {
      mCompositeDisposable.add(mModel.signUpUser(signUpReq).subscribe(user -> {
        if (user != null){
          initToolbar();
          EventBus.INSTANCE.publish(new ToastEvent(App.getAppContext().getString(R.string.sugn_up_succesfull) + user.getName()));
        }
      }, throwable -> getRootView().showError(throwable)));
    }

  }
  //endregion
}
