package com.hamom.photon.ui.screens.search;

import android.os.Bundle;
import android.os.Parcel;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.dto.FiltersDto;
import com.hamom.photon.data.storage.dto.SearchDto;
import com.hamom.photon.di.DaggerComponentFactory;
import com.hamom.photon.di.scopes.DaggerScope;
import com.hamom.photon.flow.Screen;
import com.hamom.photon.ui.activities.root.RootActivity;
import com.hamom.photon.ui.screens.base.AbstractPresenter;
import com.hamom.photon.ui.screens.base.AbstractScreen;
import com.hamom.photon.ui.screens.home.HomeScreen;
import com.hamom.photon.utils.ConstantManager;
import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;

/**
 * Created by hamom on 12.06.17.
 */
@Screen(R.layout.screen_search)
public class SearchScreen extends AbstractScreen<RootActivity.Component> {
  @Override
  public Object createScreenComponent(RootActivity.Component parentComponent) {
    return DaggerComponentFactory.createComponent(parentComponent, Component.class, new Module());
  }

  @DaggerScope(SearchScreen.class)
  @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
  public interface Component {
    void inject(Presenter presenter);
    void  inject(SearchView view);
  }

  @dagger.Module
  class Module {

    @Provides
    @DaggerScope(SearchScreen.class)
    SearchModel provideSearchModel() {
      return new SearchModel();
    }

    @Provides
    @DaggerScope(SearchScreen.class)
    Presenter providePresenter() {
      return new Presenter();
    }

  }

  //region===================== Parcelable ==========================
  public static final Creator<SearchScreen> CREATOR = new Creator<SearchScreen>() {
    @Override
    public SearchScreen createFromParcel(Parcel source) {
      return new SearchScreen();
    }

    @Override
    public SearchScreen[] newArray(int size) {
      return new SearchScreen[0];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {

  }
  //endregion
  //region===================== Presenter ==========================

  //endregion
  public class Presenter extends AbstractPresenter<SearchView, SearchModel> {

    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(ConstantManager.DAGGER_SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initToolbar() {
      if (hasView()){
        getView().initToolbar(mRootPresenter.toolbarBuilder());
      }
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
      super.onLoad(savedInstanceState);
      subscribeOnPhrasesObs();
      subscribeOnTagsObs();
    }

    private void subscribeOnPhrasesObs() {
      mCompositeDisposable.add(mModel.getSearchPhrases().subscribe(s -> {
        if (hasView()){
          getView().addSuggestion(s);
        }
      }, throwable -> getRootView().showError(throwable)));
    }

    private void subscribeOnTagsObs() {
      mCompositeDisposable.add(mModel.getTagObs().subscribe(s -> {
        if (hasView()){
          getView().addTag(s);
        }
      }, throwable -> getRootView().showError(throwable)));
    }

    void onSearchClick(SearchDto dto) {
      mModel.saveCurrentSearch(dto);
      mModel.saveSearchPhrase(dto.getSearchPhrase().trim());
      Flow.get(getView().getContext()).set(new HomeScreen(dto));
    }

    void onFilterClick(FiltersDto dto) {
      mModel.saveCurrentFilters(dto);
      Flow.get(getView().getContext()).set(new HomeScreen(dto));
    }
  }
}
