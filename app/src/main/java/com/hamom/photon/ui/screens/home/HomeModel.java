package com.hamom.photon.ui.screens.home;

import com.hamom.photon.data.network.requests.SignInReq;
import com.hamom.photon.data.network.requests.SignUpReq;
import com.hamom.photon.data.storage.dto.FiltersDto;
import com.hamom.photon.data.storage.dto.SearchDto;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.ui.screens.base.AbstractModel;
import io.reactivex.Observable;
import io.realm.RealmResults;

/**
 * Created by hamom on 01.06.17.
 */

class HomeModel extends AbstractModel {
  RealmResults<PhotoCard> getAllPhotoCard(){
    return mRealmManager.getAllPhotoCard();
  }

  public String getUserId() {
    return mDataManager.getCurrentUserId();
  }

  void logOutUser() {
    mDataManager.signOutCurrentUser();
  }

  Observable<User> signInUser(SignInReq signInReq) {
    return mDataManager.signIn(signInReq);
  }

  Observable<User> signUpUser(SignUpReq signUpReq){
    return mDataManager.signUp(signUpReq);
  }

  FiltersDto getCurrentFilters(){
    return mDataManager.getCurrentFilters();
  }

  SearchDto getCurrentSearch(){
    return mDataManager.getCurrentSearch();
  }

  void saveCurrentFilters(FiltersDto filtersDto) {
    mDataManager.saveCurrentFilters(filtersDto);
  }

  public Observable<PhotoCard> loadNextPage(int page) {
    return mDataManager.downloadListOfPhotoToRealm(page);
  }
}
