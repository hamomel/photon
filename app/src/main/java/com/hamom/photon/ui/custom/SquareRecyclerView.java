package com.hamom.photon.ui.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by hamom on 06.07.17.
 */

public class SquareRecyclerView extends RecyclerView {

  public SquareRecyclerView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void onMeasure(int widthSpec, int heightSpec) {
    heightSpec = MeasureSpec.makeMeasureSpec(widthSpec, MeasureSpec.AT_MOST);
    super.onMeasure(widthSpec, heightSpec);
  }
}
