package com.hamom.photon.ui.activities.root;

import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.hamom.photon.ui.custom.MenuItemHolder;
import java.util.List;

/**
 * Created by hamom on 01.06.17.
 */

public interface IRootView {
  void showMessage(String message);
  void showError(Throwable e);
  void showSnackBar(String message, String action, @ColorRes int actionTextColor,
      View.OnClickListener listener, @ColorRes int backGroundColor);

  void setToolbarVisible(boolean isVisible);
  void setToolbarTitle(String title);
  void setMenuItems(List<MenuItemHolder> menuItems);
  void setShowBackButton(boolean show);
  void setOverFlowButtonIcon(@DrawableRes int iconRes);
  void setTabs(ViewPager pager);

  void removeTabs();

  void openAppSettings();

}
