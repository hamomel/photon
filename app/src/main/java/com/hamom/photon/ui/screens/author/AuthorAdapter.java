package com.hamom.photon.ui.screens.author;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.realm.Album;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.ui.custom.UserInfoView;
import com.hamom.photon.ui.screens.base.OnItemClickListener;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hamom on 05.06.17.
 */

public class AuthorAdapter extends RecyclerView.Adapter<AuthorAdapter.ViewHolder> {
  private static String TAG = ConstantManager.TAG_PREFIX + "ProfileAdapt: ";
  private static final int TYPE_HEADER = 0;
  private static final int TYPE_ITEM = 1;
  private User mUser;
  private Context mContext;
  private List<Album> mAlbums = new ArrayList<>();
  private OnItemClickListener mClickListener;

  public AuthorAdapter(User user, OnItemClickListener listener){
    mUser = user;
    mAlbums = getActiveAlbums(user.getAlbums());
    mClickListener = listener;
  }

  private List<Album> getActiveAlbums(List<Album> albums) {
    List<Album> result = new ArrayList<>();
    for (Album album : albums) {
      if (album.isActive()) result.add(album);
    }
    return result;
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
    mContext = recyclerView.getContext();
  }

  @Override
  public int getItemViewType(int position) {
    if (position == 0){
      return TYPE_HEADER;
    } else {
      return TYPE_ITEM;
    }
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    View view;
    if (viewType == TYPE_HEADER){
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_info, parent, false);
    } else {
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_album, parent, false);
    }
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {

    if (position == 0){
      if (mUser != null)holder.mUserInfoView.setUser(mUser);
    } else {
      position = position - 1;
      Album album = mAlbums.get(position);
      String photoCardUrl = getActivePhotoCard(album);
      Glide.with(mContext).load(photoCardUrl).into(holder.itemPhotoIv);
      holder.albumTitleTv.setText(album.getTitle());
      holder.cardsCountTv.setText(String.valueOf(album.getPhotocards().size()));
      holder.itemFavoriteTv.setText(String.valueOf(getFavoritesCount(album)));
      holder.itemViewsTv.setText(String .valueOf(getViewsCount(album)));


      holder.itemPhotoIv.setOnClickListener(v -> mClickListener.onItemClick(album.getId()));
    }

  }

  private String getActivePhotoCard(Album album) {
    String result = "";
    if (!album.getPhotocards().isEmpty()){
      for (PhotoCard photoCard : album.getPhotocards()) {
        if (photoCard.isActive()) {
          result = photoCard.getPhoto();
          break;
        }
      }
    }
    return result;
  }

  private int getViewsCount(Album album) {
    int result = 0;
    for (PhotoCard photoCard : album.getPhotocards()) {
      result += photoCard.getViews();
    }
    return result;
  }

  private int getFavoritesCount(Album album) {
    int result = 0;
    for (PhotoCard photoCard : album.getPhotocards()) {
      result += photoCard.getFavorits();
    }
    return result;
  }

  @Override
  public int getItemCount() {
    return mAlbums.size() + 1;
  }

  class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.album_title_tv)
    TextView albumTitleTv;
    @BindView(R.id.cards_count_tv)
    TextView cardsCountTv;
    @BindView(R.id.item_photo_iv)
    ImageView itemPhotoIv;
    @BindView(R.id.item_favorite_tv)
    TextView itemFavoriteTv;
    @BindView(R.id.item_views_tv)
    TextView itemViewsTv;
    @BindView(R.id.cards_tv)
    TextView cardsTv;

    UserInfoView mUserInfoView;

    ViewHolder(View itemView) {
      super(itemView);
      if (AppConfig.DEBUG) Log.d(TAG, "AlbumViewHolder: " + itemView);
      if (itemView instanceof UserInfoView){
        mUserInfoView = ((UserInfoView) itemView);
        mUserInfoView.setShowLogin(true);
      } else {

        ButterKnife.bind(this, itemView);
        cardsTv.setVisibility(View.VISIBLE);
      }
    }
  }


}
