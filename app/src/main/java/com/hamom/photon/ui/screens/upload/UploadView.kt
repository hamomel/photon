package com.hamom.photon.ui.screens.upload

import android.content.Context
import android.support.annotation.IdRes
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import butterknife.*
import com.hamom.photon.R
import com.hamom.photon.data.network.requests.AddAlbumReq
import com.hamom.photon.data.network.requests.CreatePhotoReq
import com.hamom.photon.data.storage.dto.FiltersDto
import com.hamom.photon.data.storage.realm.Album
import com.hamom.photon.data.storage.realm.Filters
import com.hamom.photon.ui.activities.root.RootPresenter
import com.hamom.photon.ui.custom.AddAlbumDialog
import com.hamom.photon.ui.custom.FiltersView
import com.hamom.photon.ui.custom.TagsView
import com.hamom.photon.ui.screens.base.AbstractView
import com.hamom.photon.ui.screens.base.OnItemClickListener
import com.hamom.photon.ui.screens.upload.UploadAlbumAdapter.AddAlbumClickListener
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager
import flow.Flow
import me.relex.circleindicator.CircleIndicator
import mortar.MortarScope

/**
 * Created by hamom on 06.07.17.
 */
class UploadView(context: Context, attrs: AttributeSet) : AbstractView<UploadScreen.Presenter>(
    context, attrs) {
  val TAG = ConstantManager.TAG_PREFIX + "UploadView: "

  inner class FirstStepViewHolder {
    @BindView(R.id.photo_name_et) lateinit var photoNameEt: EditText
    @BindView(R.id.clear_name_ib) lateinit var clearNameIb: ImageButton
    @BindView(R.id.photo_tag_et) lateinit var photoTagEt: EditText
    @BindView(R.id.clear_tag_ib) lateinit var clearTagIb: ImageButton
    @BindView(R.id.add_tag_ib) lateinit var addTagIb: ImageButton
    @BindView(R.id.suggestion_layout) lateinit var suggestionLayout: LinearLayout
    @BindView(R.id.tags_view) lateinit var tagsView: TagsView
    @BindView(R.id.page_1_upload) lateinit var firstPageNestedScroll: NestedScrollView

    @OnClick(R.id.clear_name_ib, R.id.clear_tag_ib, R.id.add_tag_ib)
    fun onClearClick(view: View) {
      when (view.id) {
        R.id.clear_name_ib -> photoNameEt.setText("")
        R.id.clear_tag_ib -> photoTagEt.setText("")
        R.id.add_tag_ib -> {
          chooseTag(photoTagEt.text.toString()); photoTagEt.setText("")
        }
      }
    }

    @OnTextChanged(R.id.photo_tag_et)
    fun onTagTextChanged(text: CharSequence) {
      if (text.isEmpty()) addTagIb.visibility = View.GONE else addTagIb.visibility = View.VISIBLE
      setSuggestedTags(text.toString())
    }

    @OnTextChanged(R.id.photo_name_et)
    fun onTextChanged(text: CharSequence){
      setSaveBtnState()
    }

    private fun setSuggestedTags(text: String) {
      suggestionLayout.removeAllViews()
      tagsFromApi
          .filter { it.contains(text) }
          .forEach { suggestionLayout.addView(getTagTextView(it)) }
    }
  }

  inner class SecondStepViewHolder {
    @BindView(R.id.filters_view) lateinit var filtersView: FiltersView
  }

  inner class ThirdStepViewHolder {
    @BindView(R.id.albums_recycler) lateinit var albumsRecycler: RecyclerView
  }

  private val firstStepViewHolder: FirstStepViewHolder = FirstStepViewHolder()
  private val secondStepViewHolder: SecondStepViewHolder = SecondStepViewHolder()
  private val thirdStepViewHolder: ThirdStepViewHolder = ThirdStepViewHolder()

  private val albumsAdapter: UploadAlbumAdapter = UploadAlbumAdapter(
      OnItemClickListener { album = it; setSaveBtnState() },
      object : AddAlbumClickListener {
        override fun onAddAlbumClick() {
          showAddAlbumDialog()
        }
      })

  private fun showAddAlbumDialog() {
    AddAlbumDialog(context, this,
        object : AddAlbumDialog.AddAlbumCallback {
          override fun onFinish(req: AddAlbumReq) {
            mPresenter.addAlbum(req)
          }
        }).show()
  }

  private var album: String = ""
  private var tagsFromApi: ArrayList<String> = ArrayList()

  @BindView(R.id.upload_view_pager) lateinit var uploadViewPager: ViewPager
  @BindView(R.id.circle_indicator) lateinit var circleIndicator: CircleIndicator
  @BindView(R.id.step_tv) lateinit var stepTv: TextView
  @BindView(R.id.save_btn) lateinit var saveBtn: Button

  @OnClick(R.id.back_ib, R.id.forward_ib)
  fun onDirectionClick(button: ImageButton) {
    when (button.id) {
      R.id.back_ib -> goBack()
      R.id.forward_ib -> goForward()
    }
  }

  private fun goBack() {
    val newPage: Int = uploadViewPager.currentItem - 1
    if (newPage >= 0) uploadViewPager.setCurrentItem(newPage, true)
  }

  private fun goForward() {
    val newPage: Int = uploadViewPager.currentItem + 1
    val itemCount: Int = uploadViewPager.childCount
    if (newPage <= itemCount) uploadViewPager.setCurrentItem(newPage, true)
  }

  @OnClick(R.id.save_btn, R.id.cancel_btn)
  fun onButtonClick(view: View) {
    when (view.id) {
      R.id.save_btn -> onSaveClick()
      R.id.cancel_btn -> mPresenter.onCancelClick()
    }
  }

  override fun onBackPressed(): Boolean {
    return Flow.get(this).goBack()
  }

  override fun initDagger() {
    MortarScope.getScope(context).getService<UploadScreen.Component>(
        ConstantManager.DAGGER_SERVICE_NAME).inject(this)
  }

  fun initToolbar(builder: RootPresenter.ToolbarBuilder) {
    builder.setTitle(context.getString(R.string.photon)).build()
  }

  override fun onFinishInflate() {
    super.onFinishInflate()
    if (!isInEditMode) initView()
  }

  private fun initView() {
    setSaveBtnState()
    stepTv.setText(R.string.step_1_from_3)
    val pages: List<View> = createViews()
    val pagerAdapter: UploadAdapter = UploadAdapter(pages)
    pagerAdapter.registerDataSetObserver(circleIndicator.dataSetObserver)
    uploadViewPager.adapter = pagerAdapter
    circleIndicator.setViewPager(uploadViewPager)
    uploadViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
      override fun onPageScrollStateChanged(state: Int) {}

      override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

      override fun onPageSelected(position: Int) {
        @IdRes val textId: Int = when(position) {
          0 -> R.string.step_1_from_3
          1 -> R.string.step_2_from_3
          2 -> R.string.step_3_from_3
          else -> throw IllegalStateException("Illegal number of page $position")
        }
        stepTv.setText(textId)
      }

    })
  }

  private fun setSaveBtnState() {
    if (album.isEmpty() || firstStepViewHolder.photoNameEt.text.length < 3){
      saveBtn.isClickable = false
      saveBtn.background = ContextCompat.getDrawable(context, R.drawable.button_background_inactive)
    } else {
      saveBtn.isClickable = true
      saveBtn.background = ContextCompat.getDrawable(context, R.drawable.button_background)

    }
  }

  private fun createViews(): List<View> {
    val firstView: View = LayoutInflater.from(context).inflate(R.layout.page_1_upload,
        uploadViewPager, false)
    ButterKnife.bind(firstStepViewHolder, firstView)
    val secondView: View = LayoutInflater.from(context).inflate(R.layout.page_2_upload,
        uploadViewPager, false)
    ButterKnife.bind(secondStepViewHolder, secondView)
    val thirdView: View = LayoutInflater.from(context).inflate(R.layout.page_3_upload,
        uploadViewPager, false)
    ButterKnife.bind(thirdStepViewHolder, thirdView)
    initAlbumRecycler()
    return arrayListOf(firstView, secondView, thirdView)
  }

  private fun initAlbumRecycler() {
    val layoutManager: GridLayoutManager = GridLayoutManager(context, 2)
    layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
      override fun getSpanSize(position: Int): Int {
        return if (albumsAdapter.albums.isEmpty()) 2 else 1
      }
    }
    thirdStepViewHolder.albumsRecycler.layoutManager = layoutManager
    thirdStepViewHolder.albumsRecycler.adapter = albumsAdapter
  }

  fun addAlbum(album: Album) {
    albumsAdapter.addAlbum(album)
  }

  fun addTag(tag: String) {
    if (!tagsFromApi.contains(tag)) {
      tagsFromApi.add(tag)
      firstStepViewHolder.suggestionLayout.addView(getTagTextView(tag))
    }
  }

  private fun getTagTextView(tag: String): View {
    val textView: TextView = TextView(context)
    val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    textView.layoutParams = layoutParams
    textView.text = tag
    textView.setTextColor(ContextCompat.getColor(context, R.color.color_text_dark))
    textView.setOnClickListener { v -> chooseTag((v as TextView).text.toString()) }
    return textView
  }

  private fun chooseTag(tag: String) {
    firstStepViewHolder.tagsView.addTag(tag)
  }

  private fun onSaveClick() {
    mPresenter.onSaveClick(getRequest())
    showToast(context.getString(R.string.upload_started))
  }

  fun getRequest(): CreatePhotoReq {

    return CreatePhotoReq(
        album,
        "",
        getFilters(),
        firstStepViewHolder.photoNameEt.text.toString(),
        firstStepViewHolder.tagsView.allTags)
  }

  private fun getFilters(): Filters {
    val dto: FiltersDto = secondStepViewHolder.filtersView.dto
    val nuances: String = getNuances(dto)
    val filters: Filters = Filters(
        nuances,
        dto.lightDirection,
        dto.lightSource,
        dto.dish,
        dto.decor,
        dto.light,
        dto.temperature)
    return filters
  }

  private fun getNuances(dto: FiltersDto): String {
    var nuances: String = ""
    for (s in dto.nuances) {
      nuances = nuances + s + ", "
    }
    return if (nuances.isEmpty()) nuances else nuances.substring(0, nuances.lastIndexOf(","))
  }

  private fun showToast(s: String) {
    Toast.makeText(context, s, Toast.LENGTH_LONG).show()
  }

}