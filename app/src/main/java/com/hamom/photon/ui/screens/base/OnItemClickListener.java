package com.hamom.photon.ui.screens.base;

/**
 * Created by hamom on 03.06.17.
 */

public interface OnItemClickListener {
  void onItemClick(String itemId);
}
