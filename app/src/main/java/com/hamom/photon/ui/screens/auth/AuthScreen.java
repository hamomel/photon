package com.hamom.photon.ui.screens.auth;

import android.os.Parcel;
import android.support.annotation.NonNull;
import com.hamom.photon.R;
import com.hamom.photon.data.network.requests.SignInReq;
import com.hamom.photon.data.network.requests.SignUpReq;
import com.hamom.photon.di.DaggerComponentFactory;
import com.hamom.photon.di.scopes.DaggerScope;
import com.hamom.photon.ui.screens.base.AbstractScreen;
import com.hamom.photon.flow.Screen;
import com.hamom.photon.ui.activities.root.RootActivity;
import com.hamom.photon.ui.screens.base.AbstractPresenter;
import com.hamom.photon.ui.screens.profile.ProfileScreen;
import com.hamom.photon.utils.ConstantManager;
import dagger.Provides;
import flow.ClassKey;
import flow.Direction;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;

/**
 * Created by hamom on 04.06.17.
 */

@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.Component> implements TreeKey{
  private AbstractScreen parentKey;

  public AuthScreen(AbstractScreen parentKey) {
    this.parentKey = parentKey;
  }

  @Override
  public Object createScreenComponent(RootActivity.Component parentComponent) {
    return DaggerComponentFactory.createComponent(parentComponent, Component.class, new Module());
  }

  @NonNull
  @Override
  public Object getParentKey() {
    return parentKey;
  }

  @DaggerScope(AuthScreen.class)
  @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
  public interface Component{
    void inject(AuthPresenter authPresenter);
    void inject(AuthView authView);
  }

  @dagger.Module
  class Module{
    @Provides
    @DaggerScope(AuthScreen.class)
    AuthPresenter provideAuthPresenter() {
      return new AuthPresenter();
    }

    @Provides
    @DaggerScope(AuthScreen.class)
    AuthModel provideAuthModel() {
      return new AuthModel();
    }
  }

  //region===================== Parcelable ==========================
  public static final Creator<AuthScreen> CREATOR = new Creator<AuthScreen>() {
    @Override
    public AuthScreen createFromParcel(Parcel source) {
      return new AuthScreen(source.readParcelable(AbstractScreen.class.getClassLoader()));
    }

    @Override
    public AuthScreen[] newArray(int size) {
      return new AuthScreen[0];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(parentKey, flags);
  }
  //endregion

  //region===================== Presenter ==========================

  public class AuthPresenter extends AbstractPresenter<AuthView, AuthModel>{

    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(ConstantManager.DAGGER_SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initToolbar() {
      mRootPresenter.toolbarBuilder()
          .setVisible(false).build();
    }

    public void onLoginClick(SignInReq signInReq) {
      subscribeOnSignInObs(signInReq);
    }

    public void onRegisterClock(SignUpReq signUpReq) {
      subscribeOnSignUpObs(signUpReq);
    }

    private void subscribeOnSignUpObs(SignUpReq signUpReq) {
      mCompositeDisposable.add(mModel.signUp(signUpReq)
          .subscribe(user -> Flow.get(getView()).replaceTop(getParentKey(), Direction.BACKWARD),
              throwable -> getRootView().showError(throwable)
          ));
    }

    private void subscribeOnSignInObs(SignInReq signInReq) {
      mCompositeDisposable.add(mModel.signIn(signInReq)
          .subscribe(user -> Flow.get(getView()).replaceTop(getParentKey(), Direction.BACKWARD),
              throwable -> getRootView().showError(throwable)
          ));
    }
  }

  //endregion
}
