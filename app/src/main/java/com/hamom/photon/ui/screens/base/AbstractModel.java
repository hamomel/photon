package com.hamom.photon.ui.screens.base;

import com.birbit.android.jobqueue.JobManager;
import com.hamom.photon.data.managers.DataManager;
import com.hamom.photon.data.managers.RealmManager;
import com.hamom.photon.utils.App;
import javax.inject.Inject;

/**
 * Created by hamom on 01.06.17.
 */

public abstract class AbstractModel {

  @Inject
  protected DataManager mDataManager;
  @Inject
  protected JobManager mJobManager;
  @Inject
  protected RealmManager mRealmManager;

  public AbstractModel() {
    App.getAppComponent().inject(this);
  }
}
