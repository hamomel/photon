package com.hamom.photon.ui.screens.search

import com.hamom.photon.data.storage.dto.FiltersDto
import com.hamom.photon.data.storage.dto.SearchDto
import com.hamom.photon.data.storage.realm.Tag
import com.hamom.photon.ui.screens.base.AbstractModel
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.functions.BiFunction
import io.realm.RealmResults

/**
 * Created by hamom on 12.06.17.
 */

class SearchModel : AbstractModel() {

  fun getTagObs(): Observable<String> {
    val tags: RealmResults<Tag> = mRealmManager.tags

    if (!tags.isEmpty()) return Observable.mergeDelayError(createTagsObs(tags),
        mDataManager.downloadTagsObs().map { "" }).filter { !it.isEmpty() }
    else return mDataManager.downloadTagsObs().flatMap { createTagsObs(mRealmManager.tags) }
  }

  private fun createTagsObs(tags: RealmResults<Tag>): Observable<String> {
    return Observable.create<String> { e ->
      if (!e.isDisposed) {
        emitTags(tags, e)
        tags.addChangeListener { t, _ -> emitTags(t, e) }
      }
    }
  }

  private fun emitTags(tags: RealmResults<Tag>,
      e: ObservableEmitter<String>) {
    for (tag in tags) {
      e.onNext(tag.tag)
    }
  }

  fun getSearchPhrases(): Observable<String> = mRealmManager.searchPhrases

  fun saveSearchPhrase(phrase: String) {
    mRealmManager.saveSearchPhrase(phrase)
  }

  fun saveCurrentFilters(filtersDto: FiltersDto) {
    mDataManager.saveCurrentFilters(filtersDto)
  }

  fun saveCurrentSearch(searchDto: SearchDto) {
    mDataManager.saveCurrentSearch(searchDto)
  }
}
