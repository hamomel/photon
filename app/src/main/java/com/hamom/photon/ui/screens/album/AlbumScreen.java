package com.hamom.photon.ui.screens.album;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.hamom.photon.R;
import com.hamom.photon.data.network.requests.AddAlbumReq;
import com.hamom.photon.data.storage.realm.Album;
import com.hamom.photon.di.DaggerComponentFactory;
import com.hamom.photon.di.scopes.DaggerScope;
import com.hamom.photon.ui.screens.base.AbstractScreen;
import com.hamom.photon.flow.Screen;
import com.hamom.photon.ui.activities.root.RootActivity;
import com.hamom.photon.ui.screens.base.AbstractPresenter;
import com.hamom.photon.ui.screens.detail_photo.PhotoCardDetailScreen;
import com.hamom.photon.ui.screens.upload_empty.EmptyUploadScreen;
import com.hamom.photon.utils.App;
import com.hamom.photon.utils.ConstantManager;
import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;

/**
 * Created by hamom on 06.06.17.
 */
@Screen(R.layout.screen_album)
public class AlbumScreen extends AbstractScreen<RootActivity.Component> {
  private static String TAG = ConstantManager.TAG_PREFIX + "AlbumScreen: ";
  private AbstractScreen mParentScreen;
  private String mAlbumId;
  private String mUserId;
  private Parcelable mViewState;

  public AlbumScreen(String userId, String albumId, AbstractScreen parentScreen) {
    mParentScreen = parentScreen;
    mAlbumId = albumId;
    mUserId = userId;
  }

  private AlbumScreen(Parcel source) {
    String[] arr = new String[2];
    source.readStringArray(arr);
    mUserId = arr[0];
    mAlbumId = arr[1];
    mViewState = source.readParcelable(Parcelable.class.getClassLoader());
    mParentScreen = source.readParcelable(AbstractScreen.class.getClassLoader());
  }

  @Override
  public Object createScreenComponent(RootActivity.Component parentComponent) {
    return DaggerComponentFactory.createComponent(parentComponent, Component.class, new Module());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;

    return false;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + mAlbumId.hashCode();
    result = 31 * result + mParentScreen.hashCode();
    return result;
  }

  //region===================== DI ==========================
  @DaggerScope(AlbumScreen.class)
  @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
  public interface Component {
    void inject(Presenter presenter);

    void inject(AlbumView view);
  }

  @dagger.Module
  public class Module {
    @Provides
    @DaggerScope(AlbumScreen.class)
    Presenter providePresenter() {
      return new Presenter();
    }

    @Provides
    AlbumModel provideAlbumModel() {
      return new AlbumModel();
    }
  }
  //endregion

  //region===================== Parcelable ==========================
  public static final Creator<AlbumScreen> CREATOR = new Creator<AlbumScreen>() {
    @Override
    public AlbumScreen createFromParcel(Parcel source) {
      return new AlbumScreen(source);
    }

    @Override
    public AlbumScreen[] newArray(int size) {
      return new AlbumScreen[size];
    }
  };


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(mParentScreen, flags);
    dest.writeStringArray(new String[]{mUserId, mAlbumId});
    dest.writeParcelable(mViewState, flags);
  }
  //endregion

  //region===================== Presenter ==========================

  public class Presenter extends AbstractPresenter<AlbumView, AlbumModel> {

    private Album mAlbum;

    @Override
    protected void initDagger(MortarScope scope) {
      ((Component) scope.getService(ConstantManager.DAGGER_SERVICE_NAME)).inject(this);
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
      super.onLoad(savedInstanceState);
      getView().restoreLayoutManagerState(mViewState);
      subscribeOnAlbumObs();
    }

    void saveViewState(Parcelable state) {
      mViewState = state;
    }

    @Override
    protected void initToolbar() {
      if (hasView()) {
        getView().setViewMode(!mUserId.equals(mModel.getCurrentUserId()));
        getView().initToolbar(mRootPresenter.toolbarBuilder());
      }
    }

    private void subscribeOnAlbumObs() {
      mCompositeDisposable.add(mModel.getAlbumObs(mUserId, mAlbumId)
          .subscribe(album -> {
            mAlbum = album;
        if (hasView()) {
          getView().initView(album);
        }
      }, throwable -> getRootView().showError(throwable)));
    }

    void onDeleteClick() {
      if (mAlbum.isFavorite()){
        getRootView().showMessage(App.getAppContext().getString(R.string.cant_delete_favorite_album));
      } else {
        mModel.deleteAlbum(mAlbumId);
        //noinspection CheckResult
        Flow.get(getView()).goBack();
      }
    }

    void onAddClick() {
      Flow.get(getView()).set(new EmptyUploadScreen(((AbstractScreen) AlbumScreen.this), mAlbumId));
    }

    void onEditClick(AddAlbumReq req) {
      mModel.editAlbum(req, mAlbumId);
    }

    void onItemClick(String itemId) {
      Flow.get(getView()).set(new PhotoCardDetailScreen(itemId, AlbumScreen.this));
    }

  }

  //endregion
}
