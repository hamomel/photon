package com.hamom.photon.ui.screens.author

import android.content.Context
import android.os.Parcelable
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.GridLayoutManager.SpanSizeLookup
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import butterknife.BindView
import com.hamom.photon.R
import com.hamom.photon.data.storage.realm.User
import com.hamom.photon.ui.activities.root.RootPresenter
import com.hamom.photon.ui.screens.base.AbstractView
import com.hamom.photon.utils.ConstantManager
import flow.Flow
import mortar.MortarScope

/**
 * Created by hamom on 10.07.17.
 */
class AuthorView(context: Context, attrs: AttributeSet) : AbstractView<AuthorScreen.Presenter>(
    context, attrs) {

  @BindView(R.id.author_recycler) lateinit var authorRecycler: RecyclerView
  private val layoutManager: GridLayoutManager = GridLayoutManager(context, 2)

  override fun onBackPressed(): Boolean = Flow.get(context).goBack()

  override fun initDagger() {
    MortarScope.getScope(context).getService<AuthorScreen.Component>(
        ConstantManager.DAGGER_SERVICE_NAME).inject(this)
  }

  fun initToolbar(builder: RootPresenter.ToolbarBuilder) {
    builder.setTitle(context.getString(R.string.author)).setShowBack(true).build()
  }

  fun initView(user: User) {
    layoutManager.spanSizeLookup = object : SpanSizeLookup() {
      override fun getSpanSize(position: Int): Int = if (position == 0) 2 else 1
    }
    val adapter: AuthorAdapter = AuthorAdapter(user, { mPresenter.onAlbumClick(it) })
    authorRecycler.layoutManager = layoutManager
    authorRecycler.adapter = adapter
  }

  fun restoreLayoutManagerState(state: Parcelable?){
    layoutManager.onRestoreInstanceState(state)
  }

  override fun onSaveInstanceState(): Parcelable {
    mPresenter.saveViewState(layoutManager.onSaveInstanceState())
    return super.onSaveInstanceState()
  }
}