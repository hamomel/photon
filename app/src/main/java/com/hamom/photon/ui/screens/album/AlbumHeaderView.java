package com.hamom.photon.ui.screens.album;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.realm.Album;

/**
 * Created by hamom on 07.06.17.
 */

public class AlbumHeaderView extends ConstraintLayout {
  @BindView(R.id.album_title_tv)
  TextView albumTitleTv;
  @BindView(R.id.cards_count_tv)
  TextView cardsCountTv;
  @BindView(R.id.description_album_tv)
  TextView descriptionAlbumTv;

  public AlbumHeaderView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    ButterKnife.bind(this);
  }

  public void setAlbum(Album album){
    albumTitleTv.setText(album.getTitle());
    cardsCountTv.setText(String.valueOf(album.getPhotocards().size()));
    descriptionAlbumTv.setText(album.getDescription());
  }
}
