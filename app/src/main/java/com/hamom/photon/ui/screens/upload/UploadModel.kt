package com.hamom.photon.ui.screens.upload

import android.util.Log
import com.hamom.photon.data.network.requests.AddAlbumReq
import com.hamom.photon.data.network.requests.CreatePhotoReq
import com.hamom.photon.data.storage.realm.Album
import com.hamom.photon.data.storage.realm.Tag
import com.hamom.photon.data.storage.realm.User
import com.hamom.photon.jobs.jobs.AddAlbumJob
import com.hamom.photon.jobs.jobs.UploadPhotoJob
import com.hamom.photon.ui.screens.base.AbstractModel
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.realm.RealmResults

/**
 * Created by hamom on 06.07.17.
 */
class UploadModel : AbstractModel() {
  val TAG = ConstantManager.TAG_PREFIX + "UploadModel: "

  fun getUserObs(): Observable<User> {
    val userId: String = mDataManager.currentUserId
    val user: User? = mRealmManager.getUserById(userId)

    if (user != null) return Observable.mergeDelayError(createUserObs(user), createUserDownLoadObs(userId))
        .flatMap { if (it is User) Observable.just(it) else Observable.empty() }
    else return createUserDownLoadObs(userId).flatMap { createUserObs(mRealmManager.getUserById(userId)) }
  }

  private fun createUserDownLoadObs(userId: String) = mDataManager.downloadUserObs(userId)

  private fun createUserObs(user: User?): Observable<User> = Observable.create { e ->
    if (!e.isDisposed) {
      if (user != null) e.onNext(user)
      user?.addChangeListener { t: User, _ -> e.onNext(t) }
    }
  }

  fun getTagObs(): Observable<String> {
    val tags: RealmResults<Tag> = mRealmManager.tags

    if (!tags.isEmpty()) return Observable.mergeDelayError(createTagsObs(tags),
        mDataManager.downloadTagsObs().map { "" }).filter { !it.isEmpty() }
    else return mDataManager.downloadTagsObs().flatMap { createTagsObs(mRealmManager.tags) }
  }

  private fun createTagsObs(tags: RealmResults<Tag>): Observable<String> {
    return Observable.create<String> { e ->
      if (!e.isDisposed) {
        emitTags(tags, e)
        tags.addChangeListener { t, _ -> emitTags(t, e) }
      }
    }
  }

  private fun emitTags(tags: RealmResults<Tag>,
      e: ObservableEmitter<String>) {
    for (tag in tags) {
      e.onNext(tag.tag)
    }
  }


  fun getAlbumObs(albumId: String): Observable<Album> {
    val album: Album? = mRealmManager.getAlbumById(albumId)

    if (album != null) {
      return Observable.mergeDelayError(createAlbumObs(album), getDownloadAlbumObs(albumId))
          .flatMap { if (it is Album) Observable.just(it) else Observable.empty() }
          .doOnComplete { if (AppConfig.DEBUG) Log.d(TAG, "getAlbumObs onCompleted: ") }
    } else {
      return getDownloadAlbumObs(albumId).flatMap { createAlbumObs(mRealmManager.getAlbumById(albumId)) }
    }
  }

  private fun getDownloadAlbumObs(
      albumId: String) = mDataManager.downloadAlbumByIdObs(mDataManager.currentUserId, albumId)


  private fun createAlbumObs(album: Album): Observable<Album> {
    return Observable.create { e ->
      if (!e.isDisposed) e.onNext(album); album.addChangeListener { a: Album, _ -> e.onNext(a) }
    }
  }

  fun uploadPhoto(request: CreatePhotoReq) {
    mJobManager.addJobInBackground(UploadPhotoJob(request))
  }

  fun addAlbum(req: AddAlbumReq) {
    mJobManager.addJobInBackground(AddAlbumJob(req))
  }

}