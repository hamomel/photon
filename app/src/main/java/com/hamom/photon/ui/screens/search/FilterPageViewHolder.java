package com.hamom.photon.ui.screens.search;

import android.view.View;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hamom.photon.R;
import com.hamom.photon.data.storage.dto.FiltersDto;
import com.hamom.photon.ui.custom.FiltersView;
import com.hamom.photon.ui.screens.home.HomeScreen;

/**
 * Created by hamom on 20.06.17.
 */

class FilterPageViewHolder {

  @BindView(R.id.filters_view)
  FiltersView filtersView;
  @BindView(R.id.action_find_btn)
  Button actionBtn;

  private FilterCallback mCallback;
  private View mView;

  FilterPageViewHolder(View view, FilterCallback callback) {
    mCallback = callback;
    mView = view;
    ButterKnife.bind(this, view);
    applyExistingFilters();
  }

  private void applyExistingFilters(){
    FiltersDto filters = HomeScreen.getFilters();
    if (filters.isEmpty()){
      setFindAction();
    } else {
      setClearAction();
      filtersView.showCurrentFilters(filters);
    }
  }

  private void setFindAction() {
    actionBtn.setText(mView.getContext().getString(R.string.find));
    actionBtn.setOnClickListener(v -> mCallback.apply(filtersView.getDto()));
  }

  private void setClearAction() {
    actionBtn.setText(mView.getContext().getString(R.string.clear_filters));
    actionBtn.setOnClickListener(v -> mCallback.apply(new FiltersDto()));
  }

  interface FilterCallback{
    void apply(FiltersDto dto);
  }
}
