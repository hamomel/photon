package com.hamom.photon.ui.screens.upload

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.hamom.photon.R
import com.hamom.photon.data.network.requests.AddAlbumReq
import com.hamom.photon.data.network.requests.CreatePhotoReq
import com.hamom.photon.data.storage.realm.Album
import com.hamom.photon.data.storage.realm.User
import com.hamom.photon.di.DaggerComponentFactory
import com.hamom.photon.di.scopes.DaggerScope
import com.hamom.photon.eventBus.EventBus
import com.hamom.photon.eventBus.events.ErrorEvent
import com.hamom.photon.flow.Screen
import com.hamom.photon.ui.activities.root.RootActivity
import com.hamom.photon.ui.screens.auth.AuthScreen
import com.hamom.photon.ui.screens.author.AuthorScreen
import com.hamom.photon.ui.screens.base.AbstractPresenter
import com.hamom.photon.ui.screens.base.AbstractScreen
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager
import dagger.Provides
import flow.Flow
import io.reactivex.Observable
import mortar.MortarScope

/**
 * Created by hamom on 06.07.17.
 */
@Screen(R.layout.screen_upload)
class UploadScreen(var albumId: String = "", var photoUri: String = "") : AbstractScreen<RootActivity.Component>() {
val TAG = ConstantManager.TAG_PREFIX + "UploadScreen: "

  //region====================== Parcelable ============================
  override fun describeContents(): Int = 0

  override fun writeToParcel(dest: Parcel?, flags: Int) {
    dest?.writeStringArray(arrayOf(albumId, photoUri))
  }

  companion object CREATOR : Parcelable.Creator<UploadScreen> {
    override fun createFromParcel(parcel: Parcel): UploadScreen {
      val arr: Array<String> = parcel.createStringArray()
      return UploadScreen(arr[0], arr[1])
    }

    override fun newArray(size: Int): Array<UploadScreen?> {
      return Array(size, { null })
    }
  }
  //endregion

  //region====================== DI ============================
  override fun createScreenComponent(parentComponent: RootActivity.Component?): Any =
      DaggerComponentFactory.createComponent(parentComponent, Component::class.java, Module(this))

  @dagger.Module
  class Module(val screen: UploadScreen) {

    @Provides
    @DaggerScope(UploadScreen::class)
    fun providePresenter(): Presenter = screen.Presenter()

    @Provides
    @DaggerScope(UploadScreen::class)
    fun provideModel(): UploadModel = UploadModel()
  }

  @dagger.Component(modules = arrayOf(Module::class),
      dependencies = arrayOf(RootActivity.Component::class))
  @DaggerScope(UploadScreen::class)
  interface Component {
    fun inject(model: UploadView)
    fun inject(presenter: Presenter)
  }
  //endregion

  //region====================== Presenter ============================
  inner class Presenter : AbstractPresenter<UploadView, UploadModel>() {
    val TAG = ConstantManager.TAG_PREFIX + "UploadPresent: "
    override fun initDagger(scope: MortarScope) {
      scope.getService<Component>(ConstantManager.DAGGER_SERVICE_NAME).inject(this)
    }


    override fun initToolbar() {
      view.initToolbar(mRootPresenter.toolbarBuilder())
    }

    fun getAlbumId() = albumId

    override fun onLoad(savedInstanceState: Bundle?) {
      super.onLoad(savedInstanceState)
      if (AppConfig.DEBUG) Log.d(TAG, "onLoad: " + albumId)
      subscribeOnTagsObs()
      if (albumId.isEmpty()) subscribeOnUserObs()
      else subscribeOnAlbumObs()
    }

    private fun subscribeOnTagsObs() {
      mCompositeDisposable.add(mModel.getTagObs()
          .subscribe({ if (hasView()) view.addTag(it) }, { EventBus.publish(ErrorEvent(it)) }))
    }

    private fun subscribeOnUserObs() {
      mCompositeDisposable.add(mModel.getUserObs()
          .flatMap { Observable.fromIterable(it.albums) }
          .filter { it.isActive }
          .filter { !it.isFavorite }
          .subscribe({if (hasView()) view.addAlbum(it) }, { EventBus.publish(ErrorEvent(it)) }))
    }

    private fun subscribeOnAlbumObs() {
      if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnAlbumObs: ")
      mCompositeDisposable.add(mModel.getAlbumObs(albumId)
          .subscribe { if (hasView()) view.addAlbum(it)})
    }

    fun onCancelClick() {
      Flow.get(view).goBack()
    }

    fun onSaveClick(request: CreatePhotoReq) {
      request.photo = photoUri
      mModel.uploadPhoto(request)
      photoUri = ""
      Flow.get(view).goBack()
    }

    fun addAlbum(req: AddAlbumReq){
      mModel.addAlbum(req)
    }
  }
  //endregion

}