package com.hamom.photon.ui.screens.profile

import com.hamom.photon.data.network.requests.AddAlbumReq
import com.hamom.photon.data.storage.dto.EditUserDto
import com.hamom.photon.data.storage.realm.User
import com.hamom.photon.jobs.jobs.AddAlbumJob
import com.hamom.photon.jobs.jobs.UpdateUserInfoJob
import com.hamom.photon.ui.screens.base.AbstractModel
import io.reactivex.Observable

/**
 * Created by hamom on 05.06.17.
 */

internal class ProfileModel : AbstractModel() {

  fun getUserObs(userId: String): Observable<User> {
    val user: User? = mRealmManager.getUserById(userId)

    if (user != null) return Observable.mergeDelayError(createUserObs(user), createUserDownLoadObs(userId))
        .flatMap { if (it is User) Observable.just(it) else Observable.empty() }
    else return createUserDownLoadObs(userId).flatMap { createUserObs(mRealmManager.getUserById(userId)) }
  }

  private fun createUserDownLoadObs(userId: String) = mDataManager.downloadUserObs(userId)

  private fun createUserObs(user: User): Observable<User> = Observable.create { e ->
    if (!e.isDisposed) {
      e.onNext(user)
      user.addChangeListener { t: User, _ -> e.onNext(t) }
    }
  }

  fun isAuthUser() = !mDataManager.currentUserToken.isEmpty()

  val currentUserId: String
    get() = mDataManager.currentUserId

  fun createAlbum(req: AddAlbumReq) {
    mJobManager.addJobInBackground(AddAlbumJob(req))
  }

  fun signOut() {
    mDataManager.signOutCurrentUser()
  }

  fun updateUser(dto: EditUserDto) {
    mJobManager.addJobInBackground(UpdateUserInfoJob(dto))
  }
}
