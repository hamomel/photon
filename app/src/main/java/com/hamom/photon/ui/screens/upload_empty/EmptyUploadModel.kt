package com.hamom.photon.ui.screens.upload_empty

import com.hamom.photon.ui.screens.base.AbstractModel

/**
 * Created by hamom on 27.07.17.
 */
class EmptyUploadModel : AbstractModel() {
  fun isAuthUser(): Boolean = !mDataManager.currentUserToken.isEmpty()
}