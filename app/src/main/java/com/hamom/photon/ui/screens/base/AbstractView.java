package com.hamom.photon.ui.screens.base;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.Log;
import butterknife.ButterKnife;
import com.hamom.photon.ui.activities.root.RootPresenter;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import javax.inject.Inject;

/**
 * Created by hamom on 01.06.17.
 */

public abstract class AbstractView<P extends AbstractPresenter> extends ConstraintLayout implements IBackView {
  private static String TAG = ConstantManager.TAG_PREFIX + "AbstractView: ";
  @Inject
  protected P mPresenter;

  public AbstractView(Context context, AttributeSet attrs) {
    super(context, attrs);
    if (AppConfig.DEBUG) Log.d(TAG, "AbstractView: " + isInEditMode());

    if (!isInEditMode()){
      initDagger();
    }
  }

  protected abstract void initDagger();

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    if (!isInEditMode()){
      ButterKnife.bind(this);
    }
  }

  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (!isInEditMode()) mPresenter.takeView(this);
  }

  @Override
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    if (!isInEditMode()) mPresenter.dropView(this);
  }

}
