package com.hamom.photon.ui.custom;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import com.hamom.photon.R;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;

/**
 * Created by hamom on 19.06.17.
 */

public class FilterRadioButton extends android.support.v7.widget.AppCompatRadioButton {
  public static final String SUPER_STATE = "superState";
  public static final String IS_CHECKED = "isChecked";
  private static String TAG = ConstantManager.TAG_PREFIX + "FilterRButton: ";
  private @ColorRes int checkedColor = R.color.color_accent;
  private @ColorRes int uncheckedColor = R.color.color_text_light;

  public FilterRadioButton(Context context, AttributeSet attrs) {
    super(context, attrs);

    if (!isInEditMode()){
      setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.transparent));
      setButtonDrawable(new StateListDrawable());
      setGravity(Gravity.CENTER_HORIZONTAL);
      initCheckedListener();
      setUncheckedColor();
    }
  }

  private void initCheckedListener() {
    setOnCheckedChangeListener((buttonView, isChecked) -> {
      if (isChecked){
        setCheckedColor();
      } else {
        setUncheckedColor();
      }
    });
  }

  private void setCheckedColor() {
    setTextColor(ContextCompat.getColor(getContext(), checkedColor));
    setDrawablesColor(checkedColor);
  }

  private void setUncheckedColor() {
    setTextColor(ContextCompat.getColor(getContext(), uncheckedColor));
    setDrawablesColor(uncheckedColor);
  }

  private void setDrawablesColor(@ColorRes int colorRes) {
    @ColorInt int color = ContextCompat.getColor(getContext(), colorRes);
    Drawable[] drawables = getCompoundDrawables();
    Drawable[] tinted = new Drawable[4];
    for (int i = 0; i < drawables.length; i++) {
      Drawable icon = drawables[i];
      if (icon != null){
        Drawable wrappedIcon = DrawableCompat.wrap(icon.mutate());
        DrawableCompat.setTint(wrappedIcon, color);
      }
      tinted[i] = icon;
    }
    setCompoundDrawables(tinted[0], tinted[1], tinted[2], tinted[3]);
  }

  @Override
  public Parcelable onSaveInstanceState() {
    if (AppConfig.DEBUG) Log.d(TAG, "onSaveInstanceState: " + getId());

    Bundle bundle = new Bundle();
    bundle.putParcelable(SUPER_STATE, super.onSaveInstanceState());
    bundle.putBoolean(IS_CHECKED, isChecked());
    return bundle;
  }

  @Override
  public void onRestoreInstanceState(Parcelable state) {
    if (AppConfig.DEBUG) Log.d(TAG, "onRestoreInstanceState: " + getId());

    if (!(state instanceof Bundle)){
      super.onRestoreInstanceState(state);
    }
    
    Bundle bundle = ((Bundle) state);
    setChecked(bundle.getBoolean(IS_CHECKED));
    super.onRestoreInstanceState(bundle.getParcelable(SUPER_STATE));
  }
}
