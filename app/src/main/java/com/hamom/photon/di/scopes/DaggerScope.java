package com.hamom.photon.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.inject.Scope;

/**
 * Created by hamom on 01.06.17.
 */
@Scope
@Retention(RetentionPolicy.SOURCE)
public @interface DaggerScope {
  Class<?> value();
}
