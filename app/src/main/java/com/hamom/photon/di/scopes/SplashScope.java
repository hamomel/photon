package com.hamom.photon.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.inject.Scope;

/**
 * Created by hamom on 29.05.17.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface SplashScope {
}
