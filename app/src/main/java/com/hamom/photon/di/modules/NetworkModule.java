package com.hamom.photon.di.modules;

import android.support.annotation.NonNull;
import com.hamom.photon.data.network.RestService;
import com.hamom.photon.data.network.adapters.AlbumAdapter;
import com.hamom.photon.data.network.adapters.PhotoCardAdapter;
import com.hamom.photon.data.network.adapters.UserAdapter;
import com.hamom.photon.utils.AppConfig;
import com.squareup.moshi.Moshi;
import dagger.Module;
import dagger.Provides;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Created by hamom on 29.05.17.
 */
@Module
public class NetworkModule {
  @Provides
  @Singleton
  RestService provideRestService(Retrofit retrofit) {
    return retrofit.create(RestService.class);
  }

  @Provides
  @Singleton
  Retrofit provideRetrofit(OkHttpClient client) {
    return new Retrofit.Builder()
        .client(client)
        .baseUrl(AppConfig.BASE_URL)
        .addConverterFactory(createMoshiConverter())
        .addCallAdapterFactory(createRxAdapterFactory())
        .build();
  }

  @NonNull
  private RxJava2CallAdapterFactory createRxAdapterFactory() {
    return RxJava2CallAdapterFactory.create();
  }

  @NonNull
  private MoshiConverterFactory createMoshiConverter() {
    return MoshiConverterFactory.create(new Moshi.Builder()
        .add(new PhotoCardAdapter())
        .add(new UserAdapter())
        .add(new AlbumAdapter())
        .build());
  }

  @Provides
  OkHttpClient provideOkHttpClient() {
    return new OkHttpClient.Builder()
        .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .connectTimeout(AppConfig.CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
        .readTimeout(AppConfig.READ_TIMEOUT, TimeUnit.MILLISECONDS)
        .writeTimeout(AppConfig.WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
        .build();
  }
}
