package com.hamom.photon.di.modules;

import android.content.Context;
import android.util.Log;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.birbit.android.jobqueue.log.CustomLogger;
import com.facebook.stetho.Stetho;
import com.hamom.photon.data.managers.RealmManager;
import com.hamom.photon.jobs.JobInjector;
import com.hamom.photon.utils.App;
import com.hamom.photon.utils.AppConfig;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;
import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import javax.inject.Singleton;

/**
 * Created by hamom on 29.05.17.
 */
@Module
public class LocalModule {

  @Provides
  @Singleton
  RealmManager provideRealmManager(Context context) {
    Stetho.initialize(Stetho.newInitializerBuilder(context)
        .enableDumpapp(Stetho.defaultDumperPluginsProvider(context))
        .enableWebKitInspector(RealmInspectorModulesProvider.builder(context).build())
        .build());
    RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
    Realm.setDefaultConfiguration(realmConfiguration);
    return new RealmManager();
  }

  @Provides
  @Singleton
  JobManager provideJobManager(Context context) {
    return new JobManager(configureJobManager(context));
  }

  private Configuration configureJobManager(Context context) {
    return new Configuration.Builder(context)
        .minConsumerCount(1)
        .maxConsumerCount(3)
        .consumerKeepAlive(120)
        .loadFactor(3)
        .injector(new JobInjector())
        .customLogger(new CustomLogger() {
          static final String TAG = "JOBS ";
          @Override
          public boolean isDebugEnabled() {
            return AppConfig.DEBUG;
          }

          @Override
          public void d(String text, Object... args) {
            Log.d(TAG, String.format(text, args));
          }

          @Override
          public void e(Throwable t, String text, Object... args) {
            Log.e(TAG, String.format(text, args), t);

          }

          @Override
          public void e(String text, Object... args) {
            Log.e(TAG, String.format(text, args));

          }

          @Override
          public void v(String text, Object... args) {
            Log.v(TAG, String.format(text, args));

          }
        })
        .build();
  }
}
