package com.hamom.photon.di;

import android.util.Log;
import com.hamom.photon.utils.ConstantManager;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by hamom on 01.06.17.
 */

public class DaggerComponentFactory {
  private static String TAG = ConstantManager.TAG_PREFIX + "ComponFactory: ";

  public static <T> T createComponent(Object parentComponent, Class<T> componentClass,
      Object... modules) {
    T component = null;

    try {
      List<Object> dependencies = new ArrayList<>();
          dependencies.addAll(Arrays.asList(modules));
      if (parentComponent != null) {
        dependencies.add(0, parentComponent);
      }
      String daggerComponentName = nameParser(componentClass);

      Class<?> daggerComponent = Class.forName(daggerComponentName);
      Object builder = daggerComponent.getMethod("builder").invoke(null);

      for (Method method : builder.getClass().getDeclaredMethods()) {
        Class<?>[] params = method.getParameterTypes();
        if (params.length == 1) {
          Class<?> dependencyClass = params[0];
          for (Object dependency : dependencies) {
            if (dependencyClass.isAssignableFrom(dependency.getClass())) {
              method.invoke(builder, dependency);
              break;
            }
          }
        }
      }
      Log.d(TAG, "createComponent: " + builder.getClass().getName());

      component = (T) builder.getClass().getMethod("build").invoke(builder);
    } catch (InvocationTargetException e) {
      e.getCause();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    return component;
  }

  private static String nameParser(Class componentClass) {
    String fqn = componentClass.getName();
    String packageName = componentClass.getPackage().getName();
    String simpleName = fqn.substring(packageName.length() + 1);

    return (packageName + ".Dagger" + simpleName).replace('$', '_');
  }

}
