package com.hamom.photon.di.components;

import com.birbit.android.jobqueue.JobManager;
import com.hamom.photon.data.managers.DataManager;
import com.hamom.photon.data.managers.RealmManager;
import com.hamom.photon.data.network.RestService;
import com.hamom.photon.di.modules.AppModule;
import com.hamom.photon.di.modules.LocalModule;
import com.hamom.photon.di.modules.NetworkModule;
import com.hamom.photon.jobs.AbstractJob;
import com.hamom.photon.ui.screens.base.AbstractModel;
import dagger.Component;
import javax.inject.Singleton;

/**
 * Created by hamom on 29.05.17.
 */
@Singleton
@Component(modules = { AppModule.class, LocalModule.class, NetworkModule.class})
public interface AppComponent {
  RestService getRestService();
  DataManager getDataManager();
  RealmManager getRealmManager();
  JobManager getJobManager();

  void inject(AbstractModel abstractModel);
  void inject(AbstractJob job);
}
