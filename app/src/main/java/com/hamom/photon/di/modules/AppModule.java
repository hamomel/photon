package com.hamom.photon.di.modules;

import android.content.Context;
import dagger.Module;
import dagger.Provides;

/**
 * Created by hamom on 29.05.17.
 */
@Module
public class AppModule {
  private Context mContext;

  public AppModule(Context context) {
    mContext = context;
  }

  @Provides
  Context provideContext() {
    return mContext;
  }
}
