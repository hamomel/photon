package com.hamom.photon.eventBus.events

/**
 * Created by hamom on 16.07.17.
 */
data class ToastEvent(val message: String) : Event