package com.hamom.photon.eventBus

import android.util.Log
import com.hamom.photon.eventBus.events.Event
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * Created by hamom on 16.07.17.
 */
object EventBus {
  val TAG = ConstantManager.TAG_PREFIX + "EventBus: "

  private val publisher = PublishSubject.create<Event>()

  fun publish(event: Event) {
    if (AppConfig.DEBUG) Log.d(TAG, "publish: $event")
    publisher.onNext(event)
  }

  fun <T> listen(eventType: Class<T>): Observable<T> = publisher.ofType(eventType)
}