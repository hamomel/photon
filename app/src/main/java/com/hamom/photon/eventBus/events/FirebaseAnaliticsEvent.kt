package com.hamom.photon.eventBus.events

import android.os.Bundle

/**
 * Created by hamom on 26.07.17.
 */
data class FirebaseAnaliticsEvent(val eventType: String, val bundle: Bundle) : Event