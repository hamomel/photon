package com.hamom.photon.mortar;

import android.util.Log;

import com.hamom.photon.ui.screens.base.AbstractScreen;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import mortar.MortarScope;

/**
 * Created by hamom on 26.11.16.
 */

public class ScreenScoper {
  private static String TAG = ConstantManager.TAG_PREFIX + "ScreenScoper: ";

  private static Map<String, MortarScope> sScopeMap = new HashMap<>();

  public static MortarScope getScreenScope(AbstractScreen screen) {
    if (!sScopeMap.containsKey(screen.getScopeName())) {
      Log.d(TAG, "getScreenScope: create new scope");
      return createScreenScope(screen);
    } else {
      Log.d(TAG, "getScreenScope: return has scope");
      return sScopeMap.get(screen.getScopeName());
    }
  }

  public static void registerScope(MortarScope scope) {
    sScopeMap.put(scope.getName(), scope);
  }

  private static void cleanScopeMap() {
    Iterator<Map.Entry<String, MortarScope>> iterator = sScopeMap.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry<String, MortarScope> entry = iterator.next();
      if (entry.getValue().isDestroyed()) {
        iterator.remove();
      }
    }
  }

  public static void destroyScreenScope(String scopeName) {
    MortarScope mortarScope = sScopeMap.get(scopeName);
    mortarScope.destroy();
    cleanScopeMap();
  }

  private static MortarScope createScreenScope(AbstractScreen screen) {
    Log.d(TAG, "createScreenScope: with name " + screen.getScopeName());
    String parentScopeName = getParentScopeName(screen);
    MortarScope parentScope = sScopeMap.get(parentScopeName);
    Object screenComponent =
        screen.createScreenComponent(parentScope.getService(ConstantManager.DAGGER_SERVICE_NAME));
    MortarScope newScope = parentScope.buildChild()
        .withService(ConstantManager.DAGGER_SERVICE_NAME, screenComponent)
        .build(screen.getScopeName());
    registerScope(newScope);
    return newScope;
  }

  private static String getParentScopeName(AbstractScreen screen) {
    try {
      String genericName = ((Class) ((ParameterizedType) screen.getClass()
          .getGenericSuperclass()).getActualTypeArguments()[0]).getName();
      String parentScopeName = genericName;

      if (parentScopeName.contains("$")) {
        parentScopeName = parentScopeName.substring(0, genericName.indexOf("$"));
      }
      if (AppConfig.DEBUG) Log.d(TAG, "getParentScopeName: " + parentScopeName);

      return parentScopeName;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}

