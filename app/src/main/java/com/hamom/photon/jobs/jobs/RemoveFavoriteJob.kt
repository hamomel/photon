package com.hamom.photon.jobs.jobs

import android.util.Log
import com.birbit.android.jobqueue.Params
import com.hamom.photon.data.network.RestCallTransformer
import com.hamom.photon.data.storage.realm.Album
import com.hamom.photon.data.storage.realm.PhotoCard
import com.hamom.photon.data.storage.realm.User
import com.hamom.photon.eventBus.EventBus
import com.hamom.photon.eventBus.events.ErrorEvent
import com.hamom.photon.jobs.AbstractJob
import com.hamom.photon.jobs.MED
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

/**
 * Created by hamom on 11.07.17.
 */
class RemoveFavoriteJob(val photoId: String) : AbstractJob(
    Params(MED).persist().requireNetwork().groupBy("favorite" + photoId)) {
override val TAG = ConstantManager.TAG_PREFIX + "RemoveFavoriteJob: "

  override fun onAdded() {
    val realm: Realm = Realm.getDefaultInstance()
    val photoCard: PhotoCard = realm.where(PhotoCard::class.java).equalTo("id", photoId).findFirst()
    val user: User = realm.where(User::class.java).equalTo("id", dataManager.currentUserId).findFirst()
    val album: Album = user.albums.filter { it.isFavorite }[0]
    realm.executeTransaction { album.removePhotocard(photoCard) }
    realm.close()
  }

  @Throws(Throwable::class)
  override fun onRun() {
    var error: Throwable? = null
    restService.removeFromFavorite(dataManager.currentUserId, photoId, dataManager.currentUserToken)
        .compose(RestCallTransformer(dataManager, ""))
        .flatMap {
          val userId: String = dataManager.currentUserId
          val realm: Realm = Realm.getDefaultInstance()
          val user: User = realm.where(User::class.java).equalTo("id", userId).findFirst()
          val album: Album = user.albums.filter { album -> album.isFavorite }[0]
          val albumId: String = album.id
          realm.close()
          dataManager.downloadAlbumByIdObs(userId, albumId) }
        .subscribe({}, { error = it })

    error?.let { throw it }
  }

  override fun onCancel(cancelReason: Int, throwable: Throwable?) {
    super.onCancel(cancelReason, throwable)
    val albumId: String
    val realm: Realm = Realm.getDefaultInstance()
    val user: User = realm.where(User::class.java).equalTo("id", dataManager.currentUserId).findFirst()
    val album: Album = user.albums.filter { it.isFavorite }[0]
    albumId = album.id
    realm.close()
    val suffix: String = ConstantManager.ALBUM_SUFFIX + albumId
    dataManager.downloadAlbumByIdObs(dataManager.currentUserId, albumId,
        ConstantManager.DEFAULT_IF_MODIFIED_SINCE, suffix).subscribe()
  }
}