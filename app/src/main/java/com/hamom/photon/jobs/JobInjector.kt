package com.hamom.photon.jobs

import com.birbit.android.jobqueue.Job
import com.birbit.android.jobqueue.di.DependencyInjector
import com.hamom.photon.utils.App

/**
 * Created by hamom on 28.06.17.
 */
class JobInjector : DependencyInjector {
  override fun inject(job: Job?) {
    if(job is Injectable) job.inject(App.getAppComponent())
  }
}