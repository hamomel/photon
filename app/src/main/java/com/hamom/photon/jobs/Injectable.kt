package com.hamom.photon.jobs

import com.hamom.photon.di.components.AppComponent

/**
 * Created by hamom on 28.06.17.
 */
interface Injectable {
  fun inject(component: AppComponent)
}