package com.hamom.photon.jobs.jobs

import com.birbit.android.jobqueue.Params
import com.hamom.photon.data.network.RestCallTransformer
import com.hamom.photon.data.network.requests.AddAlbumReq
import com.hamom.photon.data.storage.realm.Album
import com.hamom.photon.eventBus.EventBus
import com.hamom.photon.eventBus.events.ErrorEvent
import com.hamom.photon.jobs.AbstractJob
import com.hamom.photon.jobs.MED
import com.hamom.photon.utils.ConstantManager
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

/**
 * Created by hamom on 14.07.17.
 */
class EditAlbumJob(val req: AddAlbumReq, val albumId: String) : AbstractJob(
    Params(MED).persist().requireNetwork()) {

  override fun onAdded() {
    val realm: Realm = Realm.getDefaultInstance()
    val album: Album = realm.where(Album::class.java).equalTo("id", albumId).findFirst()
    realm.beginTransaction()
    album.title = req.title
    album.description = req.description
    realm.commitTransaction()
    realm.close()
  }

  override fun onRun() {
    var error: Throwable? = null
    restService.editAlbum(dataManager.currentUserId, albumId, dataManager.currentUserToken, req)
        .subscribeOn(Schedulers.io())
        .compose(RestCallTransformer(dataManager, ""))
        .subscribe({ updateInRealm(it) }, { error = it })

    error?.let { throw it }
  }

  private fun updateInRealm(album: Album) {
    val realm: Realm = Realm.getDefaultInstance()
    realm.executeTransaction { it.insertOrUpdate(album) }
    realm.close()
  }


  override fun onCancel(cancelReason: Int, throwable: Throwable?) {
    super.onCancel(cancelReason, throwable)
    val suffix: String = ConstantManager.ALBUM_SUFFIX + albumId
    dataManager.downloadAlbumByIdObs(dataManager.currentUserId, albumId,
        ConstantManager.DEFAULT_IF_MODIFIED_SINCE, suffix).subscribe()
  }
}