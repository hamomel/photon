package com.hamom.photon.jobs.jobs

import com.birbit.android.jobqueue.Params
import com.hamom.photon.data.network.RestCallTransformer
import com.hamom.photon.data.storage.realm.Album
import com.hamom.photon.data.storage.realm.PhotoCard
import com.hamom.photon.data.storage.realm.User
import com.hamom.photon.eventBus.EventBus
import com.hamom.photon.eventBus.events.ErrorEvent
import com.hamom.photon.jobs.AbstractJob
import com.hamom.photon.jobs.MED
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

/**
 * Created by hamom on 14.07.17.
 */
class AddFavoriteJob(val photoId: String) : AbstractJob(
    Params(MED).requireNetwork().persist().groupBy("favorite" + photoId)) {

  override fun onAdded() {
    val realm: Realm = Realm.getDefaultInstance()
    val photo: PhotoCard = realm.where(PhotoCard::class.java).equalTo("id", photoId).findFirst()
    val user: User = realm.where(User::class.java).equalTo("id", dataManager.currentUserId).findFirst()
    val album: Album = user.albums.filter { it.isFavorite }[0]
    realm.executeTransaction { photo.addFavorits(); album.addPhotocard(photo) }
    realm.close()
  }

  @Throws(Throwable::class)
  override fun onRun() {
    var error: Throwable? = null

    restService.addFavorite(dataManager.currentUserId, photoId, dataManager.currentUserToken)
        .compose(RestCallTransformer(dataManager, ""))
        .flatMap {
          val realm: Realm = Realm.getDefaultInstance()
          val user: User = realm.where(User::class.java).equalTo("id", dataManager.currentUserId).findFirst()
          val album: Album = user.albums.filter { it.isFavorite }[0]
          val albumId = album.id
          realm.close()
          dataManager.downloadAlbumByIdObs(dataManager.currentUserId, albumId)
        }
        .subscribe({}, { error = it })

    error?.let { throw it }
  }

  override fun onCancel(cancelReason: Int, throwable: Throwable?) {
    super.onCancel(cancelReason, throwable)
    val realm: Realm = Realm.getDefaultInstance()
    val photo: PhotoCard = realm.where(PhotoCard::class.java).equalTo("id", photoId).findFirst()
    val user: User = realm.where(User::class.java).equalTo("id", dataManager.currentUserId).findFirst()
    val album: Album = user.albums.filter { it.isFavorite }[0]
    realm.executeTransaction { photo.removeFavorits(); album.removePhotocard(photo) }
    realm.close()
  }
}