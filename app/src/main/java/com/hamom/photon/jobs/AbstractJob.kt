package com.hamom.photon.jobs

import android.util.Log
import com.birbit.android.jobqueue.Job
import com.birbit.android.jobqueue.JobManager
import com.birbit.android.jobqueue.Params
import com.birbit.android.jobqueue.RetryConstraint
import com.hamom.photon.data.managers.DataManager
import com.hamom.photon.data.network.RestService
import com.hamom.photon.di.components.AppComponent
import com.hamom.photon.eventBus.EventBus
import com.hamom.photon.eventBus.events.ErrorEvent
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager
import javax.inject.Inject

/**
 * Created by hamom on 28.06.17.
 */
abstract class AbstractJob(params: Params?) : Job(params), Injectable {
open val TAG = ConstantManager.TAG_PREFIX + "AbstractJob: "
  @Inject @Transient lateinit var dataManager: DataManager
  @Inject @Transient lateinit var restService: RestService
  @Inject @Transient lateinit var jobManager: JobManager

  override fun inject(component: AppComponent) {
    component.inject(this)
  }

  override fun shouldReRunOnThrowable(throwable: Throwable, runCount: Int,
      maxRunCount: Int): RetryConstraint {
    if (AppConfig.DEBUG) Log.d(TAG, "shouldReRunOnThrowable: ")
    return RetryConstraint.createExponentialBackoff(
        ConstantManager.JOB_RETRY_COUNT, ConstantManager.JOB_INITIAL_RETRY_DELAY)
  }

  override fun onCancel(cancelReason: Int, throwable: Throwable?) {
    throwable?.let { EventBus.publish(ErrorEvent(Throwable(this::class.java.name + " onCancel", it))) }
  }
}
