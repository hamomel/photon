package com.hamom.photon.jobs.jobs

import android.util.Log
import com.birbit.android.jobqueue.Params
import com.hamom.photon.data.network.RestCallTransformer
import com.hamom.photon.data.network.requests.AddAlbumReq
import com.hamom.photon.data.storage.realm.Album
import com.hamom.photon.data.storage.realm.PhotoCard
import com.hamom.photon.eventBus.EventBus
import com.hamom.photon.eventBus.events.ErrorEvent
import com.hamom.photon.jobs.AbstractJob
import com.hamom.photon.jobs.MED
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager
import io.realm.Realm
import io.realm.RealmList
import java.util.*

/**
 * Created by hamom on 30.06.17.
 */
class AddAlbumJob(var req: AddAlbumReq) : AbstractJob(Params(MED).requireNetwork().persist()) {
override val TAG = ConstantManager.TAG_PREFIX + "AddAlbumJob: "

  override fun onAdded() {

  }

  override fun onRun() {
    if (AppConfig.DEBUG) Log.d(TAG, "onRun: " + req.title)
    var error: Throwable? = null
    restService.createAlbum(dataManager.currentUserId, dataManager.currentUserToken, req)
        .compose(RestCallTransformer<Album>(dataManager, ""))
        .flatMap { dataManager.downloadUserObs(dataManager.currentUserId)}
        .subscribe({}, { error = it })
    error?.let { throw it }

  }
}