package com.hamom.photon.jobs.jobs

import android.util.Log
import com.birbit.android.jobqueue.Params
import com.hamom.photon.data.network.RestCallTransformer
import com.hamom.photon.data.network.requests.EditUserReq
import com.hamom.photon.data.storage.dto.EditUserDto
import com.hamom.photon.data.storage.realm.User
import com.hamom.photon.eventBus.EventBus
import com.hamom.photon.eventBus.events.ErrorEvent
import com.hamom.photon.jobs.AbstractJob
import com.hamom.photon.jobs.MED
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

/**
 * Created by hamom on 04.07.17.
 */
class UpdateUserInfoJob(val dto: EditUserDto) : AbstractJob(
    Params(MED).requireNetwork().persist().groupBy("updateUser")) {

  override val TAG = ConstantManager.TAG_PREFIX + "UpUserInfoJob: "

  override fun onAdded() {
    val realm = Realm.getDefaultInstance()
    val user = realm.where(User::class.java).equalTo("id", dataManager.currentUserId).findFirst()
    if (AppConfig.DEBUG) Log.d(TAG, "onAdded: " + user)
    realm.beginTransaction()
    user.avatar = dto.avatar
    user.name = dto.name
    user.login = dto.login
    realm.commitTransaction()
    realm.close()
  }

  @Throws(Throwable::class)
  override fun onRun() {
    var error: Throwable? = null
    if (dto.avatar.startsWith("file:")) {
      dataManager.uploadImage(dto.avatar)
          .flatMap { (image) ->
            val req = EditUserReq(dto.name, dto.login, image)
            restService.updateUser(dataManager.currentUserId, dataManager.currentUserToken, req)
                .subscribeOn(Schedulers.io())
          }
          .compose(RestCallTransformer(dataManager,
              ConstantManager.USER_SUFFIX + dataManager.currentUserId))
          .subscribe({ user: User -> updateUserInRealm(user) }, { error = it })
    } else {
      val req = EditUserReq(dto.name, dto.login, dto.avatar)
      restService.updateUser(dataManager.currentUserId, dataManager.currentUserToken, req)
          .subscribeOn(Schedulers.io())
          .compose(RestCallTransformer(dataManager,
              ConstantManager.USER_SUFFIX + dataManager.currentUserId))
          .subscribe({ user: User -> updateUserInRealm(user) }, { error = it })
    }

    error?.let { throw it }
  }

  private fun updateUserInRealm(user: User) {
    val realm = Realm.getDefaultInstance()
    realm.executeTransaction { realm1 -> realm1.insertOrUpdate(user) }
    realm.close()
  }

  override fun onCancel(cancelReason: Int, throwable: Throwable?) {
    super.onCancel(cancelReason, throwable)
    val suffix: String = ConstantManager.USER_SUFFIX + dataManager.currentUserId
    dataManager.downloadUserObs(dataManager.currentUserId,
        ConstantManager.DEFAULT_IF_MODIFIED_SINCE, suffix).subscribe()
  }
}