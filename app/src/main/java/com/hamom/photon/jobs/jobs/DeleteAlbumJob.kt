package com.hamom.photon.jobs.jobs

import com.birbit.android.jobqueue.Params
import com.hamom.photon.data.network.RestCallTransformer
import com.hamom.photon.data.storage.realm.Album
import com.hamom.photon.eventBus.EventBus
import com.hamom.photon.eventBus.events.ErrorEvent
import com.hamom.photon.jobs.AbstractJob
import com.hamom.photon.jobs.MED
import com.hamom.photon.utils.ConstantManager
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

/**
 * Created by hamom on 09.07.17.
 */
class DeleteAlbumJob(val albumId: String) : AbstractJob(Params(MED).requireNetwork().persist()) {

  override fun onRun() {
    val realm: Realm = Realm.getDefaultInstance()
    val album: Album = realm.where(Album::class.java).equalTo("id", albumId).findFirst()
    deletePhotoCards(album)
    realm.executeTransaction { album.deleteFromRealm() }
    realm.close()
  }

  private fun deletePhotoCards(album: Album) {
    for (photocard in album.photocards) {
      jobManager.addJobInBackground(DeletePhotoCardJob(photocard.id))
    }
  }

  override fun onAdded() {
    var error: Throwable? = null
    restService.deleteAlbum(dataManager.currentUserId, albumId, dataManager.currentUserToken)
        .subscribeOn(Schedulers.io())
        .compose(RestCallTransformer(dataManager, ""))
        .subscribe({}, { error = it })

    error?.let { throw it }
  }

  override fun onCancel(cancelReason: Int, throwable: Throwable?) {
    super.onCancel(cancelReason, throwable)
    val suffix: String = ConstantManager.ALBUM_SUFFIX + albumId
    dataManager.downloadAlbumByIdObs(dataManager.currentUserId, albumId,
        ConstantManager.DEFAULT_IF_MODIFIED_SINCE, suffix).subscribe()
  }
}