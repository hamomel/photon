package com.hamom.photon.jobs.jobs

import com.birbit.android.jobqueue.Params
import com.hamom.photon.data.network.RestCallTransformer
import com.hamom.photon.data.storage.realm.PhotoCard
import com.hamom.photon.eventBus.EventBus
import com.hamom.photon.eventBus.events.ErrorEvent
import com.hamom.photon.jobs.AbstractJob
import com.hamom.photon.jobs.MED
import com.hamom.photon.utils.ConstantManager
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

/**
 * Created by hamom on 11.07.17.
 */
class DeletePhotoCardJob(val photoId: String) : AbstractJob(
    Params(MED).requireNetwork().persist()) {

  override fun onAdded() {
    val realm: Realm = Realm.getDefaultInstance()
    val photo: PhotoCard = realm.where(PhotoCard::class.java).equalTo("id", photoId).findFirst()
    realm.executeTransaction { photo.deleteFromRealm() }
    realm.close()
  }

  override fun onRun() {
    var error: Throwable? = null
    restService.deletePhotoCard(dataManager.currentUserId, photoId, dataManager.currentUserToken)
        .subscribeOn(Schedulers.io())
        .compose(RestCallTransformer(dataManager, ""))
        .subscribe({}, { error = it })

    error?.let { throw it }
  }

  override fun onCancel(cancelReason: Int, throwable: Throwable?) {
    super.onCancel(cancelReason, throwable)
    val suffix: String = ConstantManager.PHOTOCARD_SUFFIX + photoId
    dataManager.downloadPhotoCardById(dataManager.currentUserId, photoId,
        ConstantManager.DEFAULT_IF_MODIFIED_SINCE, suffix).subscribe()
  }
}