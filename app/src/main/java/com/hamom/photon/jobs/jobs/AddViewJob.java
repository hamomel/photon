package com.hamom.photon.jobs.jobs;

import android.support.annotation.Nullable;
import android.util.Log;
import com.birbit.android.jobqueue.Params;
import com.hamom.photon.data.network.RestCallTransformer;
import com.hamom.photon.data.network.requests.AddViewReq;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.eventBus.EventBus;
import com.hamom.photon.eventBus.events.ErrorEvent;
import com.hamom.photon.jobs.AbstractJob;
import com.hamom.photon.jobs.Priority;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

/**
 * Created by hamom on 27.06.17.
 */

public class AddViewJob extends AbstractJob {
  private static String TAG = ConstantManager.TAG_PREFIX + "AddViewJob ";

  private String mPhotoCardId;

  public AddViewJob(String photoCardId) {
    super(new Params(Priority.MED).requireNetwork().persist().groupBy("AddView" + photoCardId));
    this.mPhotoCardId = photoCardId;
  }

  @Override
  public void onAdded() {
    Realm realm = Realm.getDefaultInstance();
    PhotoCard photoCard = realm.where(PhotoCard.class).equalTo("id", mPhotoCardId).findFirst();
    realm.executeTransaction(realm1 -> photoCard.addView());
    realm.close();

  }

  @Override
  public void onRun() throws Throwable {
    final Throwable[] error = { null };
      restService.addPhotoCardView(mPhotoCardId, new AddViewReq(mPhotoCardId))
          .compose(new RestCallTransformer<>(dataManager, ""))
          .subscribe(modifyPhotocardRes -> {
            if (AppConfig.DEBUG) {
              Log.d(TAG, "onRun: is success: " + modifyPhotocardRes.isSuccess());
            }
          }, throwable -> error[0] = throwable);

    if (error[0] != null) throw error[0];
  }

  @Override
  protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
    super.onCancel(cancelReason, throwable);
    Realm realm = Realm.getDefaultInstance();
    PhotoCard photoCard = realm.where(PhotoCard.class).equalTo("id", mPhotoCardId).findFirst();
    realm.executeTransaction(realm1 -> photoCard.removeView());
    realm.close();
  }

}
