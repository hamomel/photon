package com.hamom.photon.jobs.jobs

import android.util.Log
import com.birbit.android.jobqueue.Params
import com.hamom.photon.R
import com.hamom.photon.data.network.RestCallTransformer
import com.hamom.photon.data.network.requests.CreatePhotoReq
import com.hamom.photon.eventBus.EventBus
import com.hamom.photon.eventBus.events.ToastEvent
import com.hamom.photon.jobs.AbstractJob
import com.hamom.photon.jobs.MED
import com.hamom.photon.utils.App
import com.hamom.photon.utils.AppConfig
import com.hamom.photon.utils.ConstantManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by hamom on 08.07.17.
 */
class UploadPhotoJob(var request: CreatePhotoReq) : AbstractJob(
    Params(MED).persist().requireNetwork()) {
  override val TAG = ConstantManager.TAG_PREFIX + "UploadPhotoJob: "

  override fun onAdded() {

  }

  override fun onRun() {
    if (AppConfig.DEBUG) Log.d(TAG, "onRun: " + request.title)
    var error: Throwable? = null
    dataManager.uploadImage(request.photo)
        .flatMap { (image) ->
          request.photo = image
          Observable.just(request).subscribeOn(Schedulers.io())
        }
        .flatMap { r: CreatePhotoReq ->
          restService.createPhoto(dataManager.currentUserId,
              dataManager.currentUserToken, r)
        }
        .compose(RestCallTransformer(dataManager, ""))
        .flatMap { dataManager.downloadPhotoCardById(dataManager.currentUserId, it.id) }
        .subscribeOn(AndroidSchedulers.mainThread())
        .subscribe( { EventBus.publish(ToastEvent(App.getAppContext().getString(R.string.photo_successfuly_published))) }, { error = it })

    error?.let { throw it }
  }
}