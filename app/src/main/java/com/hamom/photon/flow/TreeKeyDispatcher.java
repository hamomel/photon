package com.hamom.photon.flow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import com.hamom.photon.R;
import com.hamom.photon.mortar.ScreenScoper;
import com.hamom.photon.ui.screens.base.AbstractScreen;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import flow.Direction;
import flow.Dispatcher;
import flow.History;
import flow.KeyChanger;
import flow.State;
import flow.Traversal;
import flow.TraversalCallback;
import flow.TreeKey;
import java.util.Collections;
import java.util.Map;

/**
 * Created by hamom on 26.11.16.
 */

public class TreeKeyDispatcher implements KeyChanger, Dispatcher {
  private Activity mActivity;
  private Object inKey;
  @Nullable
  private Object outKey;
  private FrameLayout mRootFrame;

  public TreeKeyDispatcher(Activity activity) {
    mActivity = activity;
  }

  private static String TAG = ConstantManager.TAG_PREFIX + "TreeKeyDispat: ";
  @Override
  public void dispatch(Traversal traversal, TraversalCallback callback) {
    Map<Object, Context> contexts;
    State inState = traversal.getState(traversal.destination.top());
    inKey = inState.getKey();
    State outState = traversal.origin == null ? null : traversal.getState(traversal.origin.top());
    outKey = outState == null ? null : outState.getKey();

    mRootFrame = (FrameLayout) mActivity.findViewById(R.id.main_frame);

    if (inKey.equals(outKey)) {
      if (AppConfig.DEBUG) Log.d(TAG, "dispatch: keys are equals");

      callback.onTraversalCompleted();
      return;
    }

    Context flowContext = traversal.createContext(inKey, mActivity);
    Context mortarContext = ScreenScoper.getScreenScope((AbstractScreen) inKey).createContext(flowContext);
    contexts = Collections.singletonMap(inKey, mortarContext);

    changeKey(outState, inState, traversal.direction, contexts, callback);
  }

  @Override
  public void changeKey(@Nullable State outgoingState, State incomingState, Direction direction,
      Map<Object, Context> incomingContexts, TraversalCallback callback) {

    Context context = incomingContexts.get(inKey);

    if (outgoingState != null) {
      outgoingState.save(mRootFrame.getChildAt(0));
    }

    View newView = createNewView(context);
    incomingState.restore(newView);

    if (outKey != null && !(inKey instanceof TreeKey)) {
      ((AbstractScreen) outKey).unregisterScope();
    }

    View oldView = mRootFrame.getChildAt(0);

    if (direction == Direction.REPLACE){
      if (AppConfig.DEBUG) Log.d(TAG, "changeKey: REPLACE");
      if (mRootFrame.getChildAt(0) != null) {
        mRootFrame.removeView(oldView);
      }
      mRootFrame.addView(newView);
      callback.onTraversalCompleted();
    } else {
      if (AppConfig.DEBUG) Log.d(TAG, "changeKey: BACKWARD-FORWARD");
      mRootFrame.addView(newView);

      // дожидаемся когда станут известны размеры view которая придет в контейнер
      ViewHelper.waitForMeasure(newView, new ViewHelper.OnMeasureCallback() {
        @Override
        public void onMeasure(View view, int width, int height) {
          runAnimation(mRootFrame, oldView, newView, direction, new TraversalCallback() {
            @Override
            public void onTraversalCompleted() {
              callback.onTraversalCompleted();
            }
          });
        }
      });
    }

  }

  private View createNewView(Context context) {
    Screen screen = inKey.getClass().getAnnotation(Screen.class);
    if (screen == null) {
      throw new IllegalStateException(
          "@Screen annotation is missing on screen " + ((AbstractScreen) inKey).getScopeName());
    }

    int layout = screen.value();
    LayoutInflater inflater = LayoutInflater.from(context);
    View newView = inflater.inflate(layout, mRootFrame, false);
    return newView;
  }

  private void runAnimation(FrameLayout container, View from, View to, Direction direction,
      TraversalCallback traversalCallback) {
    if (AppConfig.DEBUG) Log.d(TAG, "runAnimation: ");

    Animator animator = createAnimation(from, to, direction);
    animator.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
        if (from != null) {
          container.removeView(from);
        }
        traversalCallback.onTraversalCompleted();
      }
    });
    animator.setInterpolator(new FastOutSlowInInterpolator());
    animator.start();
  }

  @NonNull
  private Animator createAnimation(View from, View to, Direction direction) {
    boolean backward = direction == Direction.BACKWARD;
    AnimatorSet set = new AnimatorSet();

    int fromTranslation;
    if (from != null) {
      fromTranslation = backward ? from.getWidth() : -from.getWidth();
      final ObjectAnimator outAnimation =
          ObjectAnimator.ofFloat(from, "translationX", fromTranslation);
      set.play(outAnimation);
    }

    int toTranslation = backward ? -to.getWidth() : to.getWidth();
    final ObjectAnimator toAnimation = ObjectAnimator.ofFloat(to, "translationX", toTranslation, 0);
    set.play(toAnimation);

    return set;
  }
}
