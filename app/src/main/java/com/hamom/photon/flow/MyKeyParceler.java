package com.hamom.photon.flow;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import flow.KeyParceler;

/**
 * Created by hamom on 13.06.17.
 */

public class MyKeyParceler implements KeyParceler {
  @NonNull
  @Override
  public Parcelable toParcelable(@NonNull Object key) {
    return ((Parcelable) key);
  }

  @NonNull
  @Override
  public Object toKey(@NonNull Parcelable parcelable) {
    return parcelable;
  }
}
