package com.hamom.photon.data.network.requests

import com.hamom.photon.data.storage.realm.Filters
import java.io.Serializable

data class CreatePhotoReq (
	val album: String? = null,
	var photo: String? = null,
	val filters: Filters? = null,
	val title: String? = null,
	val tags: List<String?>? = null
) : Serializable
