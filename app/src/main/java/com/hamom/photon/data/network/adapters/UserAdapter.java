package com.hamom.photon.data.network.adapters;

import com.hamom.photon.data.network.responces.UserRes;
import com.hamom.photon.data.storage.realm.Album;
import com.hamom.photon.data.storage.realm.User;
import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;
import io.realm.RealmList;

/**
 * Created by hamom on 03.06.17.
 */

public class UserAdapter {

  @ToJson
  String toJson(User user){
    return "";
  }

  @FromJson
  User fromJson(UserRes userRes){

    return new User(getAlbums(userRes),
        userRes.getAlbumCount(),
        userRes.getMail(),
        userRes.getPhotocardCount(),
        userRes.getName(),
        userRes.getId(),
        userRes.getAvatar(),
        userRes.getLogin(),
        userRes.getToken());
  }

  private RealmList<Album> getAlbums(UserRes userRes) {
    RealmList<Album> albums = new RealmList<>();
    for (Album album : userRes.getAlbums()) {
      albums.add(album);
    }
    return albums;
  }
}
