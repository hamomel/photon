package com.hamom.photon.data.network.errors

/**
 * Created by hamom on 19.07.17.
 */
class SignInError : Throwable("Логин и пароль не совпадают")