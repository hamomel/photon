package com.hamom.photon.data.storage.realm;

import com.hamom.photon.utils.ConstantManager;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PhotoCard extends RealmObject{
	@PrimaryKey
	private String id;
	private String owner;
	private String photo;
	private int favorits;
	private Filters filters;
	private String title;
	private int views;
	private String tags;
  private boolean active;

  public PhotoCard() {
  }

  public PhotoCard(String id, String owner, String photo, int favorits, Filters filters,
      String title, int views, String tags, boolean active) {
    this.id = id;
    this.owner = owner;
    this.photo = photo;
    this.favorits = favorits;
    this.filters = filters;
    this.title = title;
    this.views = views;
    this.tags = tags;
    this.active = active;
  }

  public boolean isActive(){
    return active;
  }

  public void setOwner(String owner){
		this.owner = owner;
	}

	public String getOwner(){
		return owner;
	}

	public void setPhoto(String photo){
		this.photo = photo;
	}

	public String getPhoto(){
		return photo;
	}

	public void setId(String id){
		this.id = id;
	}
	public String getId(){
		return id;
	}

	public void addFavorits(){
		favorits++;
	}

	public void removeFavorits() {
    favorits--;
	}

	public int getFavorits(){
		return favorits;
	}

	public void setFilters(Filters filters){
		this.filters = filters;
	}

	public Filters getFilters(){
		return filters;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void addView() {
		views++;
	}

	public void removeView() { views--;}

	public int getViews(){
		return views;
	}

	public List<String> getTags() {
		return new ArrayList<String>(Arrays.asList(tags.split(
        ConstantManager.PHOTO_TAGS_SEPARATOR)));
	}

	public void addTag(String tag) {
		this.tags = tags.concat(tag + ConstantManager.PHOTO_TAGS_SEPARATOR);
	}

	@Override
 	public String toString(){
		return 
			"PhotoCard{" +
			"owner = '" + owner + '\'' + 
			",photo = '" + photo + '\'' + 
			",id = '" + id + '\'' + 
			",favorits = '" + favorits + '\'' + 
			",filters = '" + filters + '\'' + 
			",title = '" + title + '\'' + 
			",views = '" + views + '\'' +
					",tags = '" + tags + '\'' +
			"}";
		}

}