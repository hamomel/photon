package com.hamom.photon.data.managers;

import android.net.Uri;
import android.util.Log;
import com.hamom.photon.data.data_utils.RetryFunction;
import com.hamom.photon.data.network.RestCallTransformer;
import com.hamom.photon.data.network.RestService;
import com.hamom.photon.data.network.SignInCallTransformer;
import com.hamom.photon.data.network.SignUpCallTransformer;
import com.hamom.photon.data.network.requests.SignInReq;
import com.hamom.photon.data.network.requests.SignUpReq;
import com.hamom.photon.data.network.responces.ModifyPhotoCardRes;
import com.hamom.photon.data.network.responces.UploadImageRes;
import com.hamom.photon.data.storage.dto.FiltersDto;
import com.hamom.photon.data.storage.dto.SearchDto;
import com.hamom.photon.data.storage.realm.Album;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.RealmResults;
import java.io.File;
import java.util.Iterator;
import javax.inject.Inject;
import javax.inject.Singleton;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.jetbrains.annotations.NotNull;

/**
 * Created by hamom on 28.05.17.
 */
@Singleton
public class DataManager {
  private static String TAG = ConstantManager.TAG_PREFIX + "DataManager: ";
  private RealmManager mRealmManager;
  private RestService mRestService;
  private AppPreferencesManager mAppPreferencesManager;

  @Inject
  public DataManager(RealmManager realmManager, RestService restService,
      AppPreferencesManager appPreferencesManager) {
    mRealmManager = realmManager;
    mRestService = restService;
    mAppPreferencesManager = appPreferencesManager;
  }

  //region===================== AppPreferences ==========================
  public AppPreferencesManager getAppPreferencesManager() {
    return mAppPreferencesManager;
  }

  public String getCurrentUserId() {
    return mAppPreferencesManager.getCurrentUserId();
  }

  public String getCurrentUserToken() {
    return mAppPreferencesManager.getCurrentUserToken();
  }

  public void signOutCurrentUser() {
    mAppPreferencesManager.deleteCurrentUserId();
    mAppPreferencesManager.deleteCurrentUserToken();
  }

  public void saveCurrentSearch(SearchDto searchDto) {
    mAppPreferencesManager.saveCurrentFilters(new FiltersDto());
    mAppPreferencesManager.saveCurrentSearch(searchDto);
  }

  public SearchDto getCurrentSearch() {
    return mAppPreferencesManager.getCurrentSerach();
  }

  public void saveCurrentFilters(FiltersDto filtersDto) {
    mAppPreferencesManager.saveCurrentSearch(new SearchDto());
    mAppPreferencesManager.saveCurrentFilters(filtersDto);
  }

  public FiltersDto getCurrentFilters() {
    return mAppPreferencesManager.getCurrentFilters();
  }
  //endregion

  public Observable<UploadImageRes> uploadImage(String fileUri) {
    File imageFile = new File(Uri.parse(fileUri).getPath());
    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);

    MultipartBody.Part body =
        MultipartBody.Part.createFormData("image", imageFile.getName(), requestBody);

    return mRestService.uploadImage(getCurrentUserId(), getCurrentUserToken(), body)
        .compose(new RestCallTransformer<>(this, ""))
        .retryWhen(new RetryFunction())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Observable<PhotoCard> downloadListOfPhotoToRealm(int pageNumber) {
    int limit = AppConfig.PHOTO_PAGE_SIZE;
    int offset = pageNumber * limit;
    String suffix = ConstantManager.PAGE_SUFFIX + pageNumber;
    String lastModified = mAppPreferencesManager.getLastModified(suffix);

    return mRestService.getPhotosList(limit, offset, lastModified)
        .compose(new RestCallTransformer<>(this, suffix))
        .retryWhen(new RetryFunction())
        .flatMap(Observable::fromIterable)
        .doOnNext(photoCard -> {
          if (!photoCard.isActive()) {
            mRealmManager.deletePhotoCard(photoCard);
          }
        })
        .filter(PhotoCard::isActive)
        .doOnNext(photo -> mRealmManager.savePhoto(photo));
  }

  public Observable<PhotoCard> downloadAllPhotoToRealm() {
    String suffix = ConstantManager.PAGE_SUFFIX + "all";
    String lastModified = ConstantManager.DEFAULT_IF_MODIFIED_SINCE;

    return mRestService.getAllPhotosList(lastModified)
        .compose(new RestCallTransformer<>(this, ""))
        .retryWhen(new RetryFunction())
        .flatMap(Observable::fromIterable)
        .doOnNext(photoCard -> {
          if (!photoCard.isActive()) {
            mRealmManager.deletePhotoCard(photoCard);
          }
        })
        .filter(PhotoCard::isActive)
        .doOnNext(photo -> mRealmManager.savePhoto(photo));
  }

  public Observable<Boolean> downloadUserObs(String userId){
    String suffix = ConstantManager.USER_SUFFIX + userId;
    String lastModified = mAppPreferencesManager.getLastModified(suffix);
    return downloadUserObs(userId, lastModified, suffix);
  }

  public Observable<Boolean> downloadUserObs(String userId, String lastModified, String lastModSuffix) {

    return mRestService.getUser(userId, lastModified)
        .compose(new RestCallTransformer<>(this, lastModSuffix))
        .retryWhen(new RetryFunction())
        .doOnNext(user -> {
          Iterator<Album> iter = user.getAlbums().iterator();
          Album album;
          if (AppConfig.DEBUG) Log.d(TAG, "downloadUserObs: " + user);
          while (iter.hasNext()){
            album = iter.next();
            if (AppConfig.DEBUG) Log.d(TAG, "downloadUserObs: " + album.getTitle() + " " + album.isActive());
            if (!album.isActive()){

              mRealmManager.deleteAlbum(album.getId());
              iter.remove();
            }
          }
        })
        .doOnNext(user -> mRealmManager.saveUser(user))
        .isEmpty()
        .toObservable()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Observable<User> signIn(SignInReq req) {
    return mRestService.signInUser(req)
        .compose(new SignInCallTransformer<>())
        .doOnNext(user -> mRealmManager.saveUser(user))
        .doOnNext(user -> {
          mAppPreferencesManager.saveCurrentUserId(user.getId());
          mAppPreferencesManager.saveCurrentUserToken(user.getToken());
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Observable<User> signUp(SignUpReq req) {
    return mRestService.signUpUser(req)
        .compose(new SignUpCallTransformer<>())
        .doOnNext(user -> mRealmManager.saveUser(user))
        .doOnNext(user -> {
          mAppPreferencesManager.saveCurrentUserId(user.getId());
          mAppPreferencesManager.saveCurrentUserToken(user.getToken());
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Observable<Boolean> downloadAlbumByIdObs(String userId, String albumId){
    String suffix = ConstantManager.ALBUM_SUFFIX + albumId;
    String lastModified = mAppPreferencesManager.getLastModified(suffix);
    return downloadAlbumByIdObs(userId, albumId, lastModified, suffix);
  }

  public Observable<Boolean> downloadAlbumByIdObs(String userId, String albumId, String lastModified, String lastModSuffix) {
    if (AppConfig.DEBUG) Log.d(TAG, "downloadAlbumByIdObs: " + albumId);

    return mRestService.getAlbum(userId, albumId, lastModified)
        .compose(new RestCallTransformer<>(this, lastModSuffix))
        .retryWhen(new RetryFunction())
        .doOnNext(album -> {
          if (!album.isActive()){
            mRealmManager.deleteAlbum(album.getId());
          }
        })
        .filter(Album::isActive)
        .doOnNext(RealmManager::saveAlbum)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .isEmpty()
        .toObservable();
  }

  public Observable<Boolean> downloadTagsObs() {
    String suffix = ConstantManager.TAGS_SUFFIX;
    String lastModified = mAppPreferencesManager.getLastModified(suffix);

    return mRestService.getTags(lastModified)
        .compose(new RestCallTransformer<>(this, suffix))
        .retryWhen(new RetryFunction())
        .subscribeOn(Schedulers.io())
        .doOnNext(s -> {
          if (AppConfig.DEBUG) Log.d(TAG, "downloadTagsObs: " + s);
        })
        .doOnNext(s -> mRealmManager.saveTag(s))
        .observeOn(AndroidSchedulers.mainThread())
        .isEmpty()
        .toObservable();
  }

  @NotNull
  public Observable<Boolean> downloadPhotoCardById(@NotNull String userId, @NotNull String photoId){
    String suffix = ConstantManager.PHOTOCARD_SUFFIX;
    String lastModified = mAppPreferencesManager.getLastModified(suffix);
    return downloadPhotoCardById(userId, photoId, lastModified, suffix);
  }

  @NotNull
  public Observable<Boolean> downloadPhotoCardById(@NotNull String userId, @NotNull String photoId, String lastModified, String lastModSuffix) {

    return mRestService.getPhotoCard(userId, photoId, lastModified)
        .compose(new RestCallTransformer<>(this, lastModSuffix))
        .retryWhen(new RetryFunction())
        .doOnNext(photoCard -> mRealmManager.savePhoto(photoCard))
        .isEmpty()
        .toObservable()
        .subscribeOn(Schedulers.io());
  }
}
