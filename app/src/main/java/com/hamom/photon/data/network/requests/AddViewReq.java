package com.hamom.photon.data.network.requests;

/**
 * Created by hamom on 14.06.17.
 */

public class AddViewReq {
  String id;

  public AddViewReq(String id) {
    this.id = id;
  }
}
