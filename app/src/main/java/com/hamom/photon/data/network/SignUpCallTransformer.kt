package com.hamom.photon.data.network

import com.hamom.photon.data.network.errors.NetworkAvailableError
import com.hamom.photon.data.network.errors.SignUpError
import com.hamom.photon.utils.NetworkStatusChecker
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import retrofit2.Response

/**
 * Created by hamom on 19.07.17.
 */
class SignUpCallTransformer<R> : ObservableTransformer<Response<R>, R> {

  override fun apply(upstream: Observable<Response<R>>): ObservableSource<R> {
    return NetworkStatusChecker.isInternetAvailable()
        .flatMap { if (it) upstream else Observable.error(NetworkAvailableError())}
        .flatMap {
          when(it.code()) {
            201 -> Observable.just(it.body())
            500 -> Observable.error { SignUpError() }
            else -> Observable.error { Throwable(it.code().toString()) }
          }
        }
  }
}