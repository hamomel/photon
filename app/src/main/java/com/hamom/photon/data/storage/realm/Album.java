package com.hamom.photon.data.storage.realm;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import java.util.List;
import org.jetbrains.annotations.NotNull;

public class Album extends RealmObject{
  @PrimaryKey
  private String id;
  private String owner;
  private String description;
	private boolean active;
	private int favorits;
	private String title;
	private int views;
	private boolean isFavorite;
	private RealmList<PhotoCard> photocards;

	public Album() {
	}

	public Album(String owner, String description, String id, int favorits,
			String title, int views, boolean isFavorite, boolean isActive, RealmList<PhotoCard> photocards) {
		this.owner = owner;
		this.description = description;
		this.id = id;
		this.favorits = favorits;
		this.title = title;
		this.views = views;
		this.photocards = photocards;
    this.active = isActive;
		this.isFavorite = isFavorite;
	}

	public void setOwner(String owner){
		this.owner = owner;
	}

	public String getOwner(){
		return owner;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setFavorits(int favorits){
		this.favorits = favorits;
	}

	public int getFavorits(){
		return favorits;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setViews(int views){
		this.views = views;
	}

	public int getViews(){
		return views;
	}

	public void addPhotocard(PhotoCard photocard){
		this.photocards.add(photocard);
	}

	public void removePhotocard(@NotNull PhotoCard photoCard) {
    this.photocards.remove(photoCard);
	}

	public List<PhotoCard> getPhotocards(){
		return photocards;
	}

	public boolean isActive() {
		return active;
	}

	public boolean isFavorite(){return isFavorite;}

	@Override
 	public String toString(){
		return 
			"Album{" + 
			"owner = '" + owner + '\'' +
			",description = '" + description + '\'' + 
			",id = '" + id + '\'' + 
			",favorits = '" + favorits + '\'' + 
			",title = '" + title + '\'' + 
			",views = '" + views + '\'' + 
			",photocards = '" + photocards + '\'' + 
			"}";
		}


}