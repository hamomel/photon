package com.hamom.photon.data.storage.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by hamom on 20.06.17.
 */
data class SearchDto(val searchPhrase: String = "",
    val tags: List<String> = ArrayList()) : Parcelable {

  constructor(parcel: Parcel) : this(
      parcel.readString(),
      parcel.createStringArrayList())

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(searchPhrase)
    parcel.writeStringList(tags)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<SearchDto> {
    override fun createFromParcel(parcel: Parcel): SearchDto {
      return SearchDto(parcel)
    }

    override fun newArray(size: Int): Array<SearchDto?> {
      return arrayOfNulls(size)
    }
  }

  fun isEmpty(): Boolean = searchPhrase.isEmpty() && tags.isEmpty()
}