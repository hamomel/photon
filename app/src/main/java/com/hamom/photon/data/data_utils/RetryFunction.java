package com.hamom.photon.data.data_utils;

import android.util.Log;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import java.util.concurrent.TimeUnit;

/**
 * Created by hamom on 17.06.17.
 */

public class RetryFunction implements Function<Observable<Throwable>, ObservableSource<?>> {
  private static String TAG = ConstantManager.TAG_PREFIX + "RetryFunction: ";

  @Override
  public ObservableSource<?> apply(@NonNull Observable<Throwable> throwableObservable)
      throws Exception {

    return throwableObservable.doOnNext(Throwable::printStackTrace)
        .doOnNext(throwable -> {
          if (AppConfig.DEBUG) {
            Log.d(TAG,
                "apply throwable: " + throwable.getClass().getSimpleName() + " " + throwable
                    .getMessage());
          }
        })
        .zipWith(Observable.range(1, AppConfig.RETRY_REQUEST_COUNT),
            (throwable, retryCount) -> retryCount)
        .doOnNext(retryCount -> {
          if (AppConfig.DEBUG) Log.d(TAG, "apply retryCount: " + retryCount);
        })
        .map(retryCount -> ((long) (AppConfig.RETRY_REQUEST_BASE_DELAY * Math.pow(Math.E,
            retryCount))))
        .flatMap(delay -> Observable.timer(delay, TimeUnit.MILLISECONDS));
  }
}
