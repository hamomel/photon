package com.hamom.photon.data.network.requests

data class EditAlbumReq(
	val preview: String? = null,
	val description: String? = null,
	val title: String? = null
)
