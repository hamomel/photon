package com.hamom.photon.data.network.responces

import com.hamom.photon.data.storage.realm.PhotoCard

data class AlbumRes(
	val owner: String = "",
	val description: String = "",
	val active: Boolean = false,
	val id: String = "",
	val favorits: Int = 0,
	val title: String = "",
	val views: Int = 0,
	val photocards: List<PhotoCard> = ArrayList(),
	val isFavorite: Boolean = false
)
