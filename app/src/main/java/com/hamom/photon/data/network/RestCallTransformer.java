package com.hamom.photon.data.network;

import android.util.Log;
import com.hamom.photon.data.managers.DataManager;
import com.hamom.photon.data.network.errors.NetworkAvailableError;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import com.hamom.photon.utils.NetworkStatusChecker;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.annotations.NonNull;
import retrofit2.Response;

/**
 * Created by hamom on 17.12.16.
 */

public class RestCallTransformer<R> implements ObservableTransformer<Response<R>, R> {
  private static String TAG = ConstantManager.TAG_PREFIX + "RestCallTrans: ";
  private DataManager mDataManager;
  private String mKeySuffix;

  public RestCallTransformer(DataManager dataManager, String keySuffix) {
    mDataManager = dataManager;
    mKeySuffix = keySuffix;
  }

  @Override
  public ObservableSource<R> apply(@NonNull final Observable<Response<R>> upstream) {
    return NetworkStatusChecker.isInternetAvailable()
        .flatMap(aBoolean -> aBoolean ? upstream : Observable.error(new NetworkAvailableError()))
        .flatMap(rResponse -> {
          if (AppConfig.DEBUG) Log.d(TAG, "apply: " + rResponse.code());
          switch (rResponse.code()){
            case 200:
              String lastModified = rResponse.headers().get(ConstantManager.LAST_MODIFIED_HEADER);

              if (lastModified != null) {
                mDataManager.getAppPreferencesManager().saveLastModified(lastModified, mKeySuffix);
              }
              return Observable.just(rResponse.body());
            case 201:
              return Observable.just(rResponse.body());
            case 202:
              return Observable.just(rResponse.body());
            case 204:
              return Observable.empty();
            case 304:
              return Observable.empty();
            default:
              return Observable.error(new Throwable(String.valueOf(rResponse.code())));
          }
        });
  }
}
