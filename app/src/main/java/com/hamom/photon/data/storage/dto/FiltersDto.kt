package com.hamom.photon.data.storage.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by hamom on 20.06.17.
 */
data class FiltersDto(
    val dish: String = "",
    val nuances: List<String> = ArrayList(),
    val decor: String = "",
    val temperature: String = "",
    val light: String = "",
    val lightDirection : String = "",
    val lightSource: String = "") : Parcelable {

  constructor(parcel: Parcel) : this(
      parcel.readString(),
      parcel.createStringArrayList(),
      parcel.readString(),
      parcel.readString(),
      parcel.readString(),
      parcel.readString(),
      parcel.readString())

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(dish)
    parcel.writeStringList(nuances)
    parcel.writeString(decor)
    parcel.writeString(temperature)
    parcel.writeString(light)
    parcel.writeString(lightDirection)
    parcel.writeString(lightSource)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<FiltersDto> {
    override fun createFromParcel(parcel: Parcel): FiltersDto {
      return FiltersDto(parcel)
    }

    override fun newArray(size: Int): Array<FiltersDto?> {
      return arrayOfNulls(size)
    }
  }

  fun isEmpty(): Boolean = dish.isEmpty()
      && nuances.isEmpty()
      && decor.isEmpty()
      && temperature.isEmpty()
      && light.isEmpty()
      && lightDirection.isEmpty()
      && lightSource.isEmpty()

}