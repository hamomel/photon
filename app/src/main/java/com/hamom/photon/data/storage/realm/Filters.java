package com.hamom.photon.data.storage.realm;

import io.realm.RealmObject;
import java.io.Serializable;

public class Filters extends RealmObject implements Serializable{
	private String nuances;
	private String lightDirection;
	private String lightSource;
	private String dish;
	private String decor;
	private String light;
	private String temperature;

	public Filters() {
	}

	public Filters(String nuances, String lightDirection, String lightSource, String dish,
			String decor, String light, String temperature) {
		this.nuances = nuances;
		this.lightDirection = lightDirection;
		this.lightSource = lightSource;
		this.dish = dish;
		this.decor = decor;
		this.light = light;
		this.temperature = temperature;
	}

	public void setNuances(String nuances){
		this.nuances = nuances;
	}

	public String getNuances(){
		return nuances;
	}

	public void setLightDirection(String lightDirection){
		this.lightDirection = lightDirection;
	}

	public String getLightDirection(){
		return lightDirection;
	}

	public void setLightSource(String lightSource){
		this.lightSource = lightSource;
	}

	public String getLightSource(){
		return lightSource;
	}

	public void setDish(String dish){
		this.dish = dish;
	}

	public String getDish(){
		return dish;
	}

	public void setDecor(String decor){
		this.decor = decor;
	}

	public String getDecor(){
		return decor;
	}

	public void setLight(String light){
		this.light = light;
	}

	public String getLight(){
		return light;
	}

	public void setTemperature(String temperature){
		this.temperature = temperature;
	}

	public String getTemperature(){
		return temperature;
	}

	@Override
 	public String toString(){
		return 
			"Filters{" + 
			"nuances = '" + nuances + '\'' + 
			",lightDirection = '" + lightDirection + '\'' + 
			",lightSource = '" + lightSource + '\'' + 
			",dish = '" + dish + '\'' + 
			",decor = '" + decor + '\'' + 
			",light = '" + light + '\'' + 
			",temperature = '" + temperature + '\'' + 
			"}";
		}
}
