package com.hamom.photon.data.network.requests;

public class SignUpReq {
	private String password;
	private String name;
	private String login;
	private String email;

	public SignUpReq(String password, String name, String login, String email) {
		this.password = password;
		this.name = name;
		this.login = login;
		this.email = email;
	}
}
