package com.hamom.photon.data.network.responces;

import com.hamom.photon.data.storage.realm.Album;
import java.util.List;

public class UserRes{
	private List<Album> albums;
	private int albumCount;
	private String mail;
	private int photocardCount;
	private String name;
	private String id;
	private String avatar;
	private String login;
	private String token;


	public List<Album> getAlbums(){
		return albums;
	}


	public int getAlbumCount(){
		return albumCount;
	}


	public String getMail(){
		return mail;
	}


	public int getPhotocardCount(){
		return photocardCount;
	}


	public String getName(){
		return name;
	}


	public String getId(){
		return id;
	}


	public String getAvatar(){
		return avatar;
	}


	public String getLogin(){
		return login;
	}


	public String getToken(){
		return token;
	}

}