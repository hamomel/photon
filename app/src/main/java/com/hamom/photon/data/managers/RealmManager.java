package com.hamom.photon.data.managers;

import android.util.Log;
import com.hamom.photon.data.storage.realm.Album;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.data.storage.realm.SearchPhrase;
import com.hamom.photon.data.storage.realm.Tag;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.jetbrains.annotations.NotNull;

/**
 * Created by hamom on 29.05.17.
 */
@Singleton
public class RealmManager {
  private static String TAG = ConstantManager.TAG_PREFIX + "RealmManager: ";
  private Realm mRealmInstance;

  @Inject
  public RealmManager() {
  }

  private Realm getRealmInstance() {
    if (mRealmInstance == null) {
      mRealmInstance = Realm.getDefaultInstance();
    }
    return mRealmInstance;
  }

  void savePhoto(final PhotoCard photoCard) {
    Realm realm = Realm.getDefaultInstance();
    realm.executeTransaction(realm1 -> realm1.insertOrUpdate(photoCard));
    realm.close();
  }

  public RealmResults<PhotoCard> getAllPhotoCard() {
    return getRealmInstance().where(PhotoCard.class)
        .equalTo("active", true)
        .findAllSorted("views", Sort.DESCENDING);
  }

  public PhotoCard getPhotoCardById(String id) {
    return getRealmInstance().where(PhotoCard.class).equalTo("id", id).findFirst();
  }

   public User getUserById(String userId) {
    return getRealmInstance().where(User.class).equalTo("id", userId).findFirst();
  }

  public void saveUser(User user) {
    Realm realm = Realm.getDefaultInstance();
    if (AppConfig.DEBUG) Log.d(TAG, "saveUser: " + user);

    realm.executeTransaction(realm1 -> realm1.insertOrUpdate(user));
    realm.close();
  }

  public static void saveAlbum(Album album) {
    if (AppConfig.DEBUG) Log.d(TAG, "saveAlbum: " + album.getId());

    Realm realm = Realm.getDefaultInstance();
    realm.executeTransactionAsync(realm1 -> realm1.insertOrUpdate(album));
    realm.close();
  }

   public void deletePhotoCard(PhotoCard photoCard) {
    Realm realm = Realm.getDefaultInstance();
    PhotoCard oldPhoto = realm.where(PhotoCard.class).equalTo("id", photoCard.getId()).findFirst();
    if (oldPhoto != null) realm.executeTransaction(realm1 -> oldPhoto.deleteFromRealm());
    realm.close();
  }

  public void saveTag(List<String> tags) {
    Realm realm = Realm.getDefaultInstance();
    for (String tag : tags) {
      realm.executeTransaction(realm1 -> realm1.insertOrUpdate(new Tag(tag)));
    }
    realm.close();
  }

  public RealmResults<Tag> getTags() {
    return getRealmInstance().where(Tag.class).findAll();
  }

  public void saveSearchPhrase(String phrase) {
    if (AppConfig.DEBUG) Log.d(TAG, "saveSearchPhrase: " + phrase);

    Realm realm = Realm.getDefaultInstance();
    realm.executeTransactionAsync(realm1 -> realm1.insertOrUpdate(new SearchPhrase(phrase)));
    realm.close();
  }

  public Observable<String> getSearchPhrases() {
    RealmResults<SearchPhrase> results =
        getRealmInstance().where(SearchPhrase.class).findAll();

    return Observable.create(e -> {
      if (!e.isDisposed()) {
        emitSearchPhrases(e, results);
        results.addChangeListener((searchPhrases, changeSet) -> {
          if (searchPhrases.isValid()) {
            emitSearchPhrases(e, searchPhrases);
          }
        });
      } else {
        results.removeAllChangeListeners();
      }
    });
  }

  private void emitSearchPhrases(ObservableEmitter<String> e,
      RealmResults<SearchPhrase> searchPhrases) {
    for (SearchPhrase searchPhrase : searchPhrases) {
      e.onNext(searchPhrase.getPhrase());
    }
  }

  public void deleteAlbum(String id) {
    Realm realm = Realm.getDefaultInstance();
    Album album = realm.where(Album.class).equalTo("id", id).findFirst();
    if (album != null) {
      realm.executeTransaction(realm1 -> album.deleteFromRealm());
    }
    realm.close();
  }

  @NotNull
  public Album getAlbumById(@NotNull String albumId) {
    return getRealmInstance().where(Album.class).equalTo("id", albumId).findFirst();
  }
}
