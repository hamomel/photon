package com.hamom.photon.data.network.requests;

public class SignInReq {
	private String password;
	private String email;

	public SignInReq(String password, String email) {
		this.password = password;
		this.email = email;
	}
}
