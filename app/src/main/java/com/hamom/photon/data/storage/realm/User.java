package com.hamom.photon.data.storage.realm;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import java.util.List;

public class User extends RealmObject{
	@PrimaryKey
	private String id;
	private RealmList<Album> albums;
	private int albumCount;
	private String mail;
	private int photocardCount;
	private String name;
	private String avatar;
	private String login;
	private String token;

	public User() {
	}

	public User(RealmList<Album> albums, int albumCount, String mail, int photocardCount, String name,
			String id, String avatar, String login, String token) {
		this.albums = albums;
		this.albumCount = albumCount;
		this.mail = mail;
		this.photocardCount = photocardCount;
		this.name = name;
		this.id = id;
		this.avatar = avatar;
		this.login = login;
		this.token = token;
	}

	public void addAlbum(Album album){
		this.albums.add(album);
	}

	public List<Album> getAlbums(){
		return albums;
	}

	public void setAlbumCount(int albumCount){
		this.albumCount = albumCount;
	}

	public int getAlbumCount(){
		return albumCount;
	}

	public void setMail(String mail){
		this.mail = mail;
	}

	public String getMail(){
		return mail;
	}

	public void setPhotocardCount(int photocardCount){
		this.photocardCount = photocardCount;
	}

	public int getPhotocardCount(){
		return photocardCount;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setAvatar(String avatar){
		this.avatar = avatar;
	}

	public String getAvatar(){
		return avatar;
	}

	public void setLogin(String login){
		this.login = login;
	}

	public String getLogin(){
		return login;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	public void deleteAlbum(Album album) {
		albums.remove(album);
	}

	@Override
 	public String toString(){
		return 
			"User{" + 
			"albums = '" + albums + '\'' + 
			",albumCount = '" + albumCount + '\'' + 
			",mail = '" + mail + '\'' + 
			",photocardCount = '" + photocardCount + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",avatar = '" + avatar + '\'' + 
			",login = '" + login + '\'' + 
			",token = '" + token + '\'' + 
			"}";
		}

}