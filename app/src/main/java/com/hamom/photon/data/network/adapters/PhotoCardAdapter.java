package com.hamom.photon.data.network.adapters;

import com.hamom.photon.data.network.responces.PhotoCardRes;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

/**
 * Created by hamom on 30.05.17.
 */

public class PhotoCardAdapter {

  @ToJson
  String toJson(PhotoCard photoCard) {
    return "";
  }

  @FromJson
  PhotoCard fromJson(PhotoCardRes res) {
    StringBuilder tags = new StringBuilder();
    for (String tag : res.getTags()) {
      tags.append(tag).append(";");
    }
    return new PhotoCard(res.getId(),
        res.getOwner(),
        res.getPhoto(),
        res.getFavorits(),
        res.getFilters(),
        res.getTitle(),
        res.getViews(),
        tags.toString(),
        res.isActive());
  }
}
