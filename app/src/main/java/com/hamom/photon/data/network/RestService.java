package com.hamom.photon.data.network;

import com.hamom.photon.data.network.requests.AddAlbumReq;
import com.hamom.photon.data.network.requests.AddViewReq;
import com.hamom.photon.data.network.requests.CreatePhotoReq;
import com.hamom.photon.data.network.requests.EditUserReq;
import com.hamom.photon.data.network.requests.SignInReq;
import com.hamom.photon.data.network.requests.SignUpReq;
import com.hamom.photon.data.network.responces.CreatePhotoRes;
import com.hamom.photon.data.network.responces.ModifyPhotoCardRes;
import com.hamom.photon.data.network.responces.UploadImageRes;
import com.hamom.photon.data.storage.realm.Album;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.hamom.photon.data.storage.realm.User;
import com.hamom.photon.utils.ConstantManager;
import io.reactivex.Observable;
import java.util.List;
import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by hamom on 29.05.17.
 */

public interface RestService {

  @GET("photocard/list")
  Observable<Response<List<PhotoCard>>> getPhotosList(@Query("limit") int limit,
      @Query("offset") int offset, @Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastModified);

  @GET("photocard/list")
  Observable<Response<List<PhotoCard>>> getAllPhotosList(@Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastModified);

  @GET("user/{userId}")
  Observable<Response<User>> getUser(@Path("userId") String userId,
      @Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastModified);

  @GET("user/{userId}/album/{albumId}")
  Observable<Response<Album>> getAlbum(@Path("userId") String userId,
      @Path("albumId") String albumId, @Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastModified);

  @POST("user/signUp")
  Observable<Response<User>> signUpUser(@Body() SignUpReq req);

  @POST("user/signin")
  Observable<Response<User>> signInUser(@Body() SignInReq req);

  @POST("photocard/{photocardId}/view")
  Observable<Response<ModifyPhotoCardRes>> addPhotoCardView(@Path("photocardId") String id,
      @Body() AddViewReq req);

  @POST("user/{userId}/favorite/{photocardId}")
  Observable<Response<ModifyPhotoCardRes>> addFavorite(@Path("userId") String userId,
      @Path("photocardId") String photocardId, @Header(ConstantManager.AUTHORIZATION_HEADER) String token);

  @GET("photocard/tags")
  Observable<Response<List<String>>> getTags(@Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastModified);

  @POST("user/{userId}/album")
  Observable<Response<Album>> createAlbum(@Path("userId") String userId,
      @Header(ConstantManager.AUTHORIZATION_HEADER) String token, @Body() AddAlbumReq req);

  @PUT("user/{userId}/album/{albumId}")
  Observable<Response<Album>> editAlbum(@Path("userId") String userId, @Path("albumId") String albumId,
      @Header(ConstantManager.AUTHORIZATION_HEADER) String token, @Body() AddAlbumReq req);

  @Multipart
  @POST("user/{userId}/image/upload")
  Observable<Response<UploadImageRes>> uploadImage(@Path("userId") String userId,
      @Header(ConstantManager.AUTHORIZATION_HEADER) String token, @Part MultipartBody.Part file);

  @PUT("user/{userId}")
  Observable<Response<User>> updateUser(@Path("userId") String userId,
      @Header(ConstantManager.AUTHORIZATION_HEADER) String token, @Body EditUserReq req);

  @POST("user/{userId}/photocard")
  Observable<Response<CreatePhotoRes>> createPhoto(@Path("userId") String userId,
      @Header(ConstantManager.AUTHORIZATION_HEADER) String token, @Body CreatePhotoReq req);

  @DELETE("user/{userId}/album/{albumId}")
  Observable<Response<Void>> deleteAlbum(@Path("userId") String userId,
      @Path("albumId") String albumId, @Header(ConstantManager.AUTHORIZATION_HEADER) String token);

  @DELETE("user/{userId}/favorite/{photocardId}")
  Observable<Response<Void>> removeFromFavorite(@Path("userId") String userId,
      @Path("photocardId") String photocardId, @Header(ConstantManager.AUTHORIZATION_HEADER) String token);

  @DELETE("user/{userId}/photocard/{photocardId}")
  Observable<Response<Void>> deletePhotoCard(@Path("userId") String userId,
      @Path("photocardId") String photocardId, @Header(ConstantManager.AUTHORIZATION_HEADER) String token);

  @GET("user/{userId}/photocard/{photoCardId}")
  Observable<Response<PhotoCard>> getPhotoCard(@Path("userId") String userId,
      @Path("photoCardId") String photocardId, @Header(ConstantManager.AUTHORIZATION_HEADER) String token);
}
