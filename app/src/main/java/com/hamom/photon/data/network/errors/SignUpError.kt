package com.hamom.photon.data.network.errors

/**
 * Created by hamom on 19.07.17.
 */
class SignUpError : Throwable("Пользователь с таким логином или e-mail уже зарегистрирован")