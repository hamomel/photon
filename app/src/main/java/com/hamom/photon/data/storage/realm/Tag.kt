package com.hamom.photon.data.storage.realm

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by hamom on 21.06.17.
 */
@RealmClass open class Tag(@field:PrimaryKey var tag: String = ""): RealmModel