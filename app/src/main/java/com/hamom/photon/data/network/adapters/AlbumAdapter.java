package com.hamom.photon.data.network.adapters;

import com.hamom.photon.data.network.responces.AlbumRes;
import com.hamom.photon.data.storage.realm.Album;
import com.hamom.photon.data.storage.realm.PhotoCard;
import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;
import io.realm.RealmList;

/**
 * Created by hamom on 03.06.17.
 */

public class AlbumAdapter {

  @ToJson
  String toJson(Album album){
    return "";
  }

  @FromJson
  Album fromJson(AlbumRes albumRes){

    return new Album(albumRes.getOwner(),
        albumRes.getDescription(),
        albumRes.getId(),
        albumRes.getFavorits(),
        albumRes.getTitle(),
        albumRes.getViews(),
        albumRes.isFavorite(),
        albumRes.getActive(),
        getPhotoCards(albumRes));
  }

  private RealmList<PhotoCard> getPhotoCards(AlbumRes albumRes) {
    RealmList<PhotoCard> photoCards = new RealmList<>();
    for (PhotoCard photoCard : albumRes.getPhotocards()) {
      photoCards.add(photoCard);
    }
    return photoCards;
  }

}
