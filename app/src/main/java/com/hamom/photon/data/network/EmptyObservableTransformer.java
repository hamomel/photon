package com.hamom.photon.data.network;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.annotations.NonNull;

/**
 * Created by hamom on 06.06.17.
 */

public class EmptyObservableTransformer<R> implements ObservableTransformer<R, R> {

  Observable<R> mDatabaseObs;

  public EmptyObservableTransformer(Observable<R> databaseObs) {
    mDatabaseObs = databaseObs;
  }

  @Override
  public ObservableSource<R> apply(@NonNull Observable<R> upstream) {
    return upstream.isEmpty().toObservable().flatMap(aBoolean -> {
      if (aBoolean){
        return mDatabaseObs;
      } else {
        return Observable.empty();
      }
    });
  }
}
