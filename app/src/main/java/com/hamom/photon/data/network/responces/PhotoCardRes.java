package com.hamom.photon.data.network.responces;

import com.hamom.photon.data.storage.realm.Filters;
import java.util.List;

public class PhotoCardRes{
	private String owner;
	private String photo;
	private String id;
	private int favorits;
	private Filters filters;
	private String title;
	private int views;
	private List<String> tags;
	private boolean active;

	public PhotoCardRes(String owner, String photo, String id, int favorits, Filters filters,
			String title, int views, List<String> tags, boolean active) {
		this.owner = owner;
		this.photo = photo;
		this.id = id;
		this.favorits = favorits;
		this.filters = filters;
		this.title = title;
		this.views = views;
		this.tags = tags;
		this.active = active;
	}

	public boolean isActive(){
		return active;
	}

	public String getOwner(){
		return owner;
	}

	public String getPhoto(){
		return photo;
	}

	public String getId(){
		return id;
	}

	public int getFavorits(){
		return favorits;
	}

	public Filters getFilters(){
		return filters;
	}

	public String getTitle(){
		return title;
	}

	public int getViews(){
		return views;
	}

	public List<String> getTags(){
		return tags;
	}
}