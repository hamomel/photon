package com.hamom.photon.data.storage.dto

import java.io.Serializable

/**
 * Created by hamom on 04.07.17.
 */
data class EditUserDto(val name: String, val login: String, val avatar: String) : Serializable