package com.hamom.photon.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.StringRes;
import android.util.Log;
import com.hamom.photon.data.storage.dto.FiltersDto;
import com.hamom.photon.data.storage.dto.SearchDto;
import com.hamom.photon.utils.AppConfig;
import com.hamom.photon.utils.ConstantManager;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import java.io.IOException;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by hamom on 29.05.17.
 */
@Singleton
public class AppPreferencesManager {
  private static String TAG = ConstantManager.TAG_PREFIX + "AppPrefMan: ";

  private static final String CURRENT_SEARCH_KEY = "CURRENT_SEARCH_KEY";
  private static final String CURRENT_FILTERS_KEY = "CURRENT_FILTERS_KEY";
  private static final String LAST_MODIFIED_KEY = "LAST_MODIFIED_KEY";
  private static final String USER_ID_KEY = "USER_ID_KEY";
  private static final String USER_TOKEN_KEY = "USER_TOKEN_KEY";

  private SharedPreferences mSharedPreferences;

  @Inject
  public AppPreferencesManager(Context context) {
    mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
  }

  public void saveLastModified(String lastModified, String suffix){
    if (AppConfig.DEBUG) Log.d(TAG, "saveLastModified: " + suffix + " " + lastModified);

    String key = LAST_MODIFIED_KEY + suffix;
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(key, lastModified);
    editor.apply();
  }

  String getLastModified(String suffix){
    String key = LAST_MODIFIED_KEY + suffix;
    return mSharedPreferences.getString(key, ConstantManager.DEFAULT_IF_MODIFIED_SINCE);
  }

  void saveCurrentUserId(String id){
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(USER_ID_KEY, id);
    editor.apply();
  }

  String getCurrentUserId(){
    return mSharedPreferences.getString(USER_ID_KEY, "");
  }

  void deleteCurrentUserId(){
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.remove(USER_ID_KEY);
    editor.apply();
  }

  void saveCurrentUserToken(String token){
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(USER_TOKEN_KEY, token);
    editor.apply();
  }

  String getCurrentUserToken(){
    return mSharedPreferences.getString(USER_TOKEN_KEY, "");
  }

  void deleteCurrentUserToken(){
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.remove(USER_TOKEN_KEY);
    editor.apply();
  }

  void saveCurrentSearch(SearchDto search){
    JsonAdapter<SearchDto> adapter = getSearchDtoJsonAdapter();
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(CURRENT_SEARCH_KEY, adapter.toJson(search));
    editor.apply();
  }

  SearchDto getCurrentSerach(){
    JsonAdapter<SearchDto> adapter = getSearchDtoJsonAdapter();
    String json = mSharedPreferences.getString(CURRENT_SEARCH_KEY, "");
    SearchDto dto = new SearchDto();
    try {
      dto = json.isEmpty() ? dto : adapter.fromJson(json);
    } catch (IOException e){
      e.printStackTrace();
    }
    return dto;
  }

  private JsonAdapter<SearchDto> getSearchDtoJsonAdapter() {
    Moshi moshi = new Moshi.Builder().build();
    return moshi.adapter(SearchDto.class);
  }

  void saveCurrentFilters(FiltersDto search){
    JsonAdapter<FiltersDto> adapter = getFiltersDtoJsonAdapter();
    SharedPreferences.Editor editor = mSharedPreferences.edit();
    editor.putString(CURRENT_FILTERS_KEY, adapter.toJson(search));
    editor.apply();
  }

  FiltersDto getCurrentFilters(){
    JsonAdapter<FiltersDto> adapter = getFiltersDtoJsonAdapter();
    String json = mSharedPreferences.getString(CURRENT_FILTERS_KEY, "");
    FiltersDto dto = new FiltersDto();
    try {
      dto = json.isEmpty() ? dto : adapter.fromJson(json);
    } catch (IOException e){
      e.printStackTrace();
    }
    return dto;
  }

  private JsonAdapter<FiltersDto> getFiltersDtoJsonAdapter() {
    Moshi moshi = new Moshi.Builder().build();
    return moshi.adapter(FiltersDto.class);
  }
}
