package com.hamom.photon.data.network.requests

import java.io.Serializable

data class AddAlbumReq(
    val description: String,
    val title: String
) : Serializable
