package com.hamom.photon.data.network.responces;

/**
 * Created by hamom on 14.06.17.
 */

public class ModifyPhotoCardRes {
  boolean success;

  public ModifyPhotoCardRes(boolean success) {
    this.success = success;
  }

  public boolean isSuccess() {
    return success;
  }

}
