package com.hamom.photon.data.network.requests

/**
 * Created by hamom on 01.07.17.
 */
data class EditUserReq(val name: String, val login: String, val avatar: String)