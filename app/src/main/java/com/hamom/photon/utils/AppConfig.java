package com.hamom.photon.utils;

/**
 * Created by hamom on 29.05.17.
 */

public class AppConfig {
  public static final boolean DEBUG = true;
  //network
  public static final String BASE_URL = "http://207.154.248.163:5000/";
  public static final long CONNECT_TIMEOUT = 5000L;
  public static final long READ_TIMEOUT = 5000L;
  public static final long WRITE_TIMEOUT = 5000L;
  public static final int RETRY_REQUEST_COUNT = 5;
  public static final int RETRY_REQUEST_BASE_DELAY = 500;

  public static final int PHOTO_PAGE_SIZE = 60;
}
