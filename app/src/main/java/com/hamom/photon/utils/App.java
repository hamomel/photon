package com.hamom.photon.utils;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;
import com.hamom.photon.data.network.errors.SignInError;
import com.hamom.photon.data.network.errors.SignUpError;
import com.hamom.photon.di.components.AppComponent;
import com.hamom.photon.di.components.DaggerAppComponent;
import com.hamom.photon.di.modules.AppModule;
import com.hamom.photon.di.modules.LocalModule;
import com.hamom.photon.di.modules.NetworkModule;
import com.hamom.photon.eventBus.EventBus;
import com.hamom.photon.eventBus.events.ErrorEvent;
import com.hamom.photon.eventBus.events.FirebaseAnaliticsEvent;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import mortar.MortarScope;

/**
 * Created by hamom on 28.05.17.
 */

public class App extends Application {
  private static String TAG = ConstantManager.TAG_PREFIX + "App: ";
  public static AppComponent sAppComponent;
  private static Context sContext;
  private MortarScope mRootScope;
  private FirebaseAnalytics mFirebaseAnalytics;

  @Override
  public Object getSystemService(String name) {
    if (mRootScope == null) {
      return super.getSystemService(name);
    } else {
      return mRootScope.hasService(name) ? mRootScope.getService(name)
          : super.getSystemService(name);
    }
  }

  private MortarScope createRootScope() {
    return MortarScope.buildRootScope()
        .withService(ConstantManager.DAGGER_SERVICE_NAME, sAppComponent)
        .build("root");
  }

  @Override
  public void onCreate() {
    super.onCreate();
    sContext = getApplicationContext();
    sAppComponent = createAppComponent();
    mRootScope = createRootScope();
    Realm.init(sContext);
    mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    subscribeOnFirebaseEvents();
    subscribeOnFirebaseCrush();
  }

  private void subscribeOnFirebaseCrush() {
    EventBus.INSTANCE.listen(ErrorEvent.class)
        .subscribeOn(Schedulers.io())
        .doOnNext(event -> {
          if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnFirebaseEvents: " + event);
        })
        .filter(errorEvent -> !(errorEvent.getThrowable() instanceof SignUpError))
        .filter(errorEvent -> !(errorEvent.getThrowable() instanceof SignInError))
        .subscribe(errorEvent -> FirebaseCrash.report(errorEvent.getThrowable()));
  }

  private void subscribeOnFirebaseEvents() {
    EventBus.INSTANCE.listen(FirebaseAnaliticsEvent.class)
        .subscribeOn(Schedulers.io())
        .doOnNext(event -> {
          if (AppConfig.DEBUG) Log.d(TAG, "subscribeOnFirebaseEvents: " + event);
        })
        .subscribe(event -> mFirebaseAnalytics.logEvent(event.getEventType(), event.getBundle()));
  }

  @Override
  public void onTerminate() {
    super.onTerminate();
  }

  private AppComponent createAppComponent() {
    return DaggerAppComponent.builder()
        .localModule(new LocalModule())
        .networkModule(new NetworkModule())
        .appModule(new AppModule(sContext))
        .build();
  }

  public static AppComponent getAppComponent() {
    return sAppComponent;
  }

  public static Context getAppContext() {
    return sContext;
  }
}
