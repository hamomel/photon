package com.hamom.photon.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import io.reactivex.Observable;

/**
 * Created by hamom on 17.12.16.
 */

public class NetworkStatusChecker {
  private static String TAG = ConstantManager.TAG_PREFIX + "NetStatCheck: ";
  private static Boolean isNetworkAvailable(){
    ConnectivityManager cm = (ConnectivityManager) App.getAppContext().getSystemService(
        Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = cm.getActiveNetworkInfo();
    if (AppConfig.DEBUG) Log.d(TAG, "isNetworkAvailable: " + (networkInfo != null && networkInfo.isConnectedOrConnecting()));

    return networkInfo != null && networkInfo.isConnectedOrConnecting();
  }

  public static Observable<Boolean> isInternetAvailable(){
    return Observable.create(e -> {
      if (!e.isDisposed()){
        e.onNext(isNetworkAvailable());
      }
    });
  }
}
