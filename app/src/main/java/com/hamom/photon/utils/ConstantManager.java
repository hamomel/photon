package com.hamom.photon.utils;

import com.hamom.photon.BuildConfig;

/**
 * Created by hamom on 29.05.17.
 */

public class ConstantManager {
  public static final String TAG_PREFIX = "Photon: ";

  public static final String FILE_PROVIDER_AUTHORITY = BuildConfig.APPLICATION_ID + ".fileprovider";

  public static final String LAST_MODIFIED_HEADER = "Last-Modified";
  public static final String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";
  public static final String AUTHORIZATION_HEADER = "Authorization";

  public static final String PHOTO_TAGS_SEPARATOR = ";";

  public static final String DAGGER_SERVICE_NAME = "DaggerService";

  public static final String DEFAULT_IF_MODIFIED_SINCE = "Wed, 15 Nov 1995 04:58:08 GMT";
  public static final String PAGE_SUFFIX = "_PAGE_";
  public static final String USER_SUFFIX = "_USER_";
  public static final String ALBUM_SUFFIX = "_ALBUM_";
  public static final String TAGS_SUFFIX = "_TAGS";
  public static final String PHOTOCARD_SUFFIX = "_PHOTOCARD_";

  public static final int SHARE_PERMISSIONS_REQUEST = 1001;
  public static final int DOWNLOAD_PERMISSIONS_REQUEST = 1002;
  public static final int READ_PERMISSIONS_REQUEST = 1003;
  public static final int REQUEST_PHOTO_FROM_GALLERY = 2001;

  public static final int JOB_RETRY_COUNT = 5;

  public static final long JOB_INITIAL_RETRY_DELAY = 500;
  public static final String LOGIN_REGEXP = "^[a-zA-Z0-9_]{3,12}$";
  public static final String NAME_REGEXP = ".{3,24}$";
  public static final String PASSWORD_REGEXP = "^[a-zA-Z0-9]{8,24}$";
}
